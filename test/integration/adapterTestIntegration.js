/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const mocha = require('mocha');
const path = require('path');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const util = require('util');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-gitlab',
      type: 'Gitlab',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Gitlab = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] GitLab Adapter Test', () => {
  describe('Gitlab Class Tests', () => {
    const a = new Gitlab(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    // describe('#getV4ApplicationSettings - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ApplicationSettings((data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.after_sign_out_path);
    //           assert.equal('string', data.response.after_sign_up_text);
    //           assert.equal('string', data.response.container_registry_token_expire_delay);
    //           assert.equal('string', data.response.created_at);
    //           assert.equal('string', data.response.default_branch_protection);
    //           assert.equal('string', data.response.default_group_visibility);
    //           assert.equal('string', data.response.default_project_visibility);
    //           assert.equal('string', data.response.default_projects_limit);
    //           assert.equal('string', data.response.default_snippet_visibility);
    //           assert.equal('string', data.response.domain_blacklist);
    //           assert.equal('string', data.response.domain_blacklist_enabled);
    //           assert.equal('string', data.response.domain_whitelist);
    //           assert.equal('string', data.response.gravatar_enabled);
    //           assert.equal('string', data.response.home_page_url);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.koding_enabled);
    //           assert.equal('string', data.response.koding_url);
    //           assert.equal('string', data.response.max_attachment_size);
    //           assert.equal('string', data.response.plantuml_enabled);
    //           assert.equal('string', data.response.plantuml_url);
    //           assert.equal('string', data.response.repository_storage);
    //           assert.equal('string', data.response.repository_storages);
    //           assert.equal('string', data.response.restricted_visibility_levels);
    //           assert.equal('string', data.response.session_expire_delay);
    //           assert.equal('string', data.response.sign_in_text);
    //           assert.equal('string', data.response.signin_enabled);
    //           assert.equal('string', data.response.signup_enabled);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('string', data.response.user_oauth_applications);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Application', 'getV4ApplicationSettings', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4ApplicationSettings - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ApplicationSettings({}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.after_sign_out_path);
    //           assert.equal('string', data.response.after_sign_up_text);
    //           assert.equal('string', data.response.container_registry_token_expire_delay);
    //           assert.equal('string', data.response.created_at);
    //           assert.equal('string', data.response.default_branch_protection);
    //           assert.equal('string', data.response.default_group_visibility);
    //           assert.equal('string', data.response.default_project_visibility);
    //           assert.equal('string', data.response.default_projects_limit);
    //           assert.equal('string', data.response.default_snippet_visibility);
    //           assert.equal('string', data.response.domain_blacklist);
    //           assert.equal('string', data.response.domain_blacklist_enabled);
    //           assert.equal('string', data.response.domain_whitelist);
    //           assert.equal('string', data.response.gravatar_enabled);
    //           assert.equal('string', data.response.home_page_url);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.koding_enabled);
    //           assert.equal('string', data.response.koding_url);
    //           assert.equal('string', data.response.max_attachment_size);
    //           assert.equal('string', data.response.plantuml_enabled);
    //           assert.equal('string', data.response.plantuml_url);
    //           assert.equal('string', data.response.repository_storage);
    //           assert.equal('string', data.response.repository_storages);
    //           assert.equal('string', data.response.restricted_visibility_levels);
    //           assert.equal('string', data.response.session_expire_delay);
    //           assert.equal('string', data.response.sign_in_text);
    //           assert.equal('string', data.response.signin_enabled);
    //           assert.equal('string', data.response.signup_enabled);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('string', data.response.user_oauth_applications);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Application', 'putV4ApplicationSettings', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4DeployKeys - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4DeployKeys((data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response['0']);
    //           assert.equal('object', typeof data.response['1']);
    //           assert.equal('object', typeof data.response['2']);
    //           assert.equal('object', typeof data.response['3']);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('DeployKeys', 'getV4DeployKeys', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    let groupId;

    describe('#getV4Groups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4Groups(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response['0']);
                assert.equal('object', typeof data.response['1']);
                assert.equal('object', typeof data.response['2']);
              } else {
                runCommonAsserts(data, error);
              }

              groupId = data.response[0].id;
              saveMockData('Groups', 'getV4Groups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#postV4Groups - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4Groups({}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.avatar_url);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.lfs_enabled);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.path);
    //           assert.equal('string', data.response.request_access_enabled);
    //           assert.equal('string', data.response.statistics);
    //           assert.equal('string', data.response.visibility_level);
    //           assert.equal('string', data.response.web_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'postV4Groups', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4GroupsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4GroupsId(groupId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.avatar_url);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.lfs_enabled);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.path);
                assert.equal('object', typeof data.response.projects);
                assert.equal('string', data.response.request_access_enabled);
                assert.equal('object', typeof data.response.shared_projects);
                assert.equal('string', data.response.statistics);
                assert.equal('string', data.response.visibility_level);
                assert.equal('string', data.response.web_url);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Groups', 'getV4GroupsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putV4GroupsId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4GroupsId('fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.avatar_url);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.lfs_enabled);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.path);
    //           assert.equal('string', data.response.request_access_enabled);
    //           assert.equal('string', data.response.statistics);
    //           assert.equal('string', data.response.visibility_level);
    //           assert.equal('string', data.response.web_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'putV4GroupsId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4GroupsId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4GroupsId('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'deleteV4GroupsId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4GroupsIdAccessRequests - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4GroupsIdAccessRequests('fakedata', 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.avatar_url);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.requested_at);
    //           assert.equal('string', data.response.state);
    //           assert.equal('string', data.response.username);
    //           assert.equal('string', data.response.web_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'getV4GroupsIdAccessRequests', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4GroupsIdAccessRequests - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4GroupsIdAccessRequests('fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.avatar_url);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.requested_at);
    //           assert.equal('string', data.response.state);
    //           assert.equal('string', data.response.username);
    //           assert.equal('string', data.response.web_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'postV4GroupsIdAccessRequests', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4GroupsIdAccessRequestsUserId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4GroupsIdAccessRequestsUserId('fakedata', 12, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'deleteV4GroupsIdAccessRequestsUserId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4GroupsIdAccessRequestsUserIdApprove - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4GroupsIdAccessRequestsUserIdApprove('fakedata', 12, {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.access_level);
    //           assert.equal('string', data.response.avatar_url);
    //           assert.equal('string', data.response.expires_at);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.state);
    //           assert.equal('string', data.response.username);
    //           assert.equal('string', data.response.web_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'putV4GroupsIdAccessRequestsUserIdApprove', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4GroupsIdIssues - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4GroupsIdIssues(groupId, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.assignee);
                assert.equal('object', typeof data.response.author);
                assert.equal('string', data.response.confidential);
                assert.equal('string', data.response.created_at);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.downvotes);
                assert.equal('string', data.response.due_date);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.iid);
                assert.equal('string', data.response.labels);
                assert.equal('object', typeof data.response.milestone);
                assert.equal('string', data.response.projectId);
                assert.equal('string', data.response.state);
                assert.equal('string', data.response.subscribed);
                assert.equal('string', data.response.title);
                assert.equal('string', data.response.updated_at);
                assert.equal('string', data.response.upvotes);
                assert.equal('string', data.response.user_notes_count);
                assert.equal('string', data.response.web_url);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Groups', 'getV4GroupsIdIssues', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let userId;

    describe('#getV4GroupsIdMembers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4GroupsIdMembers(groupId, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.access_level);
                assert.equal('string', data.response.avatar_url);
                assert.equal('string', data.response.expires_at);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.state);
                assert.equal('string', data.response.username);
                assert.equal('string', data.response.web_url);
                userId = data.response.id;
              } else {
                runCommonAsserts(data, error);
                userId = data.response[0].id;
              }

              saveMockData('Groups', 'getV4GroupsIdMembers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#postV4GroupsIdMembers - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4GroupsIdMembers('fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.access_level);
    //           assert.equal('string', data.response.avatar_url);
    //           assert.equal('string', data.response.expires_at);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.state);
    //           assert.equal('string', data.response.username);
    //           assert.equal('string', data.response.web_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'postV4GroupsIdMembers', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4GroupsIdMembersUserId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4GroupsIdMembersUserId('fakedata', 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.access_level);
    //           assert.equal('string', data.response.avatar_url);
    //           assert.equal('string', data.response.expires_at);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.state);
    //           assert.equal('string', data.response.username);
    //           assert.equal('string', data.response.web_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'getV4GroupsIdMembersUserId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4GroupsIdMembersUserId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4GroupsIdMembersUserId('fakedata', 12, {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.access_level);
    //           assert.equal('string', data.response.avatar_url);
    //           assert.equal('string', data.response.expires_at);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.state);
    //           assert.equal('string', data.response.username);
    //           assert.equal('string', data.response.web_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'putV4GroupsIdMembersUserId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4GroupsIdMembersUserId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4GroupsIdMembersUserId('fakedata', 12, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'deleteV4GroupsIdMembersUserId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4GroupsIdNotificationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4GroupsIdNotificationSettings(groupId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.events);
                assert.equal('string', data.response.level);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Groups', 'getV4GroupsIdNotificationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putV4GroupsIdNotificationSettings - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4GroupsIdNotificationSettings('fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.events);
    //           assert.equal('string', data.response.level);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'putV4GroupsIdNotificationSettings', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4GroupsIdProjects - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4GroupsIdProjects(groupId, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.archived);
                assert.equal('string', data.response.avatar_url);
                assert.equal('string', data.response.builds_enabled);
                assert.equal('string', data.response.container_registry_enabled);
                assert.equal('string', data.response.created_at);
                assert.equal('string', data.response.creator_id);
                assert.equal('string', data.response.default_branch);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.forked_from_project);
                assert.equal('string', data.response.forks_count);
                assert.equal('string', data.response.http_url_to_repo);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.issues_enabled);
                assert.equal('string', data.response.last_activity_at);
                assert.equal('string', data.response.lfs_enabled);
                assert.equal('string', data.response.merge_requests_enabled);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.name_with_namespace);
                assert.equal('object', typeof data.response.namespace);
                assert.equal('string', data.response.only_allow_merge_if_all_discussions_are_resolved);
                assert.equal('string', data.response.only_allow_merge_if_build_succeeds);
                assert.equal('string', data.response.open_issues_count);
                assert.equal('object', typeof data.response.owner);
                assert.equal('string', data.response.path);
                assert.equal('string', data.response.path_with_namespace);
                assert.equal('string', data.response.public);
                assert.equal('string', data.response.public_builds);
                assert.equal('string', data.response.request_access_enabled);
                assert.equal('string', data.response.runners_token);
                assert.equal('string', data.response.shared_runners_enabled);
                assert.equal('string', data.response.shared_with_groups);
                assert.equal('string', data.response.snippets_enabled);
                assert.equal('string', data.response.ssh_url_to_repo);
                assert.equal('string', data.response.star_count);
                assert.equal('object', typeof data.response.statistics);
                assert.equal('string', data.response.tag_list);
                assert.equal('string', data.response.visibility_level);
                assert.equal('string', data.response.web_url);
                assert.equal('string', data.response.wiki_enabled);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Groups', 'getV4GroupsIdProjects', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#postV4GroupsIdProjectsProjectId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4GroupsIdProjectsProjectId('fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.avatar_url);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.lfs_enabled);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.path);
    //           assert.equal('object', typeof data.response.projects);
    //           assert.equal('string', data.response.request_access_enabled);
    //           assert.equal('object', typeof data.response.shared_projects);
    //           assert.equal('string', data.response.statistics);
    //           assert.equal('string', data.response.visibility_level);
    //           assert.equal('string', data.response.web_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'postV4GroupsIdProjectsProjectId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4GroupsIdCustomAttributes - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4GroupsIdCustomAttributes(12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response['0']);
    //           assert.equal('object', typeof data.response['1']);
    //           assert.equal('object', typeof data.response['2']);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'getV4GroupsIdCustomAttributes', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4GroupsIdCustomAttributesKey - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4GroupsIdCustomAttributesKey('fakedata', 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.key);
    //           assert.equal('string', data.response.value);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'getV4GroupsIdCustomAttributesKey', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4GroupsIdCustomAttributes - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4GroupsIdCustomAttributes('fakedata', 12, {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.key);
    //           assert.equal('string', data.response.value);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'putV4GroupsIdCustomAttributes', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4GroupsIdCustomAttributes - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4GroupsIdCustomAttributes(12, 'fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'deleteV4GroupsIdCustomAttributes', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4GroupsIdBadges - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4GroupsIdBadges(groupId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(10, data.response.id);
                assert.equal('string', data.response.linkUrl);
                assert.equal('string', data.response.imageUrl);
                assert.equal('string', data.response.rendered_link_url);
                assert.equal('string', data.response.rendered_image_url);
                assert.equal('string', data.response.kind);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Groups', 'getV4GroupsIdBadges', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#postV4GroupsIdBadges - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4GroupsIdBadges('fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(5, data.response.id);
    //           assert.equal('string', data.response.linkUrl);
    //           assert.equal('string', data.response.imageUrl);
    //           assert.equal('string', data.response.rendered_link_url);
    //           assert.equal('string', data.response.rendered_image_url);
    //           assert.equal('string', data.response.kind);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'postV4GroupsIdBadges', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4GroupsIdBadgesBadgeId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4GroupsIdBadgesBadgeId('fakedata', 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(5, data.response.id);
    //           assert.equal('string', data.response.linkUrl);
    //           assert.equal('string', data.response.imageUrl);
    //           assert.equal('string', data.response.rendered_link_url);
    //           assert.equal('string', data.response.rendered_image_url);
    //           assert.equal('string', data.response.kind);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'getV4GroupsIdBadgesBadgeId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4GroupsIdBadgesBadgedId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4GroupsIdBadgesBadgedId('fakedata', 12, {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(1, data.response.id);
    //           assert.equal('string', data.response.linkUrl);
    //           assert.equal('string', data.response.imageUrl);
    //           assert.equal('string', data.response.rendered_link_url);
    //           assert.equal('string', data.response.rendered_image_url);
    //           assert.equal('string', data.response.kind);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'putV4GroupsIdBadgesBadgedId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4GroupsIdBadgesBadgesId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4GroupsIdBadgesBadgesId('fakedata', 12, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'deleteV4GroupsIdBadgesBadgesId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4GroupsIdBadgesRender - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4GroupsIdBadgesRender('fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.linkUrl);
    //           assert.equal('string', data.response.imageUrl);
    //           assert.equal('string', data.response.rendered_link_url);
    //           assert.equal('string', data.response.rendered_image_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'getV4GroupsIdBadgesRender', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4GroupsIdBoards - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4GroupsIdBoards(groupId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response['0']);
                assert.equal('object', typeof data.response['1']);
                assert.equal('object', typeof data.response['2']);
                assert.equal('object', typeof data.response['3']);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Groups', 'getV4GroupsIdBoards', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#postV4GroupsIdBoards - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4GroupsIdBoards('fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(10, data.response.id);
    //           assert.equal('string', data.response['name:']);
    //           assert.equal('object', typeof data.response.group);
    //           assert.equal('object', typeof data.response.milestone);
    //           assert.equal(true, Array.isArray(data.response.lists));
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'postV4GroupsIdBoards', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4GroupsIdBoardsBoardId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4GroupsIdBoardsBoardId(12, 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(4, data.response.id);
    //           assert.equal('string', data.response['name:']);
    //           assert.equal('object', typeof data.response.group);
    //           assert.equal('object', typeof data.response.milestone);
    //           assert.equal(true, Array.isArray(data.response.lists));
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'getV4GroupsIdBoardsBoardId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4GroupsIdBoardsBoardId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4GroupsIdBoardsBoardId(12, 'fakedata', 12, 12, 12, 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(8, data.response.id);
    //           assert.equal('string', data.response['name:']);
    //           assert.equal('object', typeof data.response.group);
    //           assert.equal('object', typeof data.response.milestone);
    //           assert.equal(true, Array.isArray(data.response.lists));
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'putV4GroupsIdBoardsBoardId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4GroupsIdBoardsBoardId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4GroupsIdBoardsBoardId('fakedata', 12, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'deleteV4GroupsIdBoardsBoardId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4GroupsIdBoardsBoardIdLists - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4GroupsIdBoardsBoardIdLists(12, 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response['0']);
    //           assert.equal('object', typeof data.response['1']);
    //           assert.equal('object', typeof data.response['2']);
    //           assert.equal('object', typeof data.response['3']);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'getV4GroupsIdBoardsBoardIdLists', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4GroupsIdBoardsBoardIdLists - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4GroupsIdBoardsBoardIdLists(12, 'fakedata', 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response['0']);
    //           assert.equal('object', typeof data.response['1']);
    //           assert.equal('object', typeof data.response['2']);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'postV4GroupsIdBoardsBoardIdLists', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4GroupsIdBoardsBoardIdListsListId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4GroupsIdBoardsBoardIdListsListId('fakedata', 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response['0']);
    //           assert.equal('object', typeof data.response['1']);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'getV4GroupsIdBoardsBoardIdListsListId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4GroupsIdBoardsBoardsIdListsListId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4GroupsIdBoardsBoardsIdListsListId(12, 'fakedata', 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response['0']);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'putV4GroupsIdBoardsBoardsIdListsListId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4GroupsIdBoardsBoardIdListsListId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4GroupsIdBoardsBoardIdListsListId('fakedata', 12, 12, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'deleteV4GroupsIdBoardsBoardIdListsListId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4GroupsIdLabels - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4GroupsIdLabels(groupId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response['0']);
                assert.equal('object', typeof data.response['1']);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Groups', 'getV4GroupsIdLabels', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putV4GroupsIdLabels - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4GroupsIdLabels('fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(6, data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.color);
    //           assert.equal('string', data.response.text_color);
    //           assert.equal('object', typeof data.response.description);
    //           assert.equal(5, data.response.open_issues_count);
    //           assert.equal(10, data.response.closed_issues_count);
    //           assert.equal(1, data.response.open_merge_requests_count);
    //           assert.equal(true, data.response.subscribed);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'putV4GroupsIdLabels', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4GroupsIdLabels - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4GroupsIdLabels('fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(6, data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.color);
    //           assert.equal('string', data.response.text_color);
    //           assert.equal('object', typeof data.response.description);
    //           assert.equal(7, data.response.open_issues_count);
    //           assert.equal(4, data.response.closed_issues_count);
    //           assert.equal(8, data.response.open_merge_requests_count);
    //           assert.equal(true, data.response.subscribed);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'postV4GroupsIdLabels', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4GroupsIdLabels - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4GroupsIdLabels('fakedata', 'fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'deleteV4GroupsIdLabels', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4GroupsIdLabelsLabelIdSubscribe - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4GroupsIdLabelsLabelIdSubscribe('fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(7, data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.color);
    //           assert.equal('string', data.response.text_color);
    //           assert.equal('object', typeof data.response.description);
    //           assert.equal(4, data.response.open_issues_count);
    //           assert.equal(8, data.response.closed_issues_count);
    //           assert.equal(2, data.response.open_merge_requests_count);
    //           assert.equal(false, data.response.subscribed);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'postV4GroupsIdLabelsLabelIdSubscribe', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4GroupsIdLabelsLabelIdUnsubscribe - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4GroupsIdLabelsLabelIdUnsubscribe('fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(4, data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.color);
    //           assert.equal('string', data.response.text_color);
    //           assert.equal('object', typeof data.response.description);
    //           assert.equal(8, data.response.open_issues_count);
    //           assert.equal(5, data.response.closed_issues_count);
    //           assert.equal(6, data.response.open_merge_requests_count);
    //           assert.equal(true, data.response.subscribed);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'postV4GroupsIdLabelsLabelIdUnsubscribe', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4GroupsIdVariables - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4GroupsIdVariables('fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response['0']);
    //           assert.equal('object', typeof data.response['1']);
    //           assert.equal('object', typeof data.response['2']);
    //           assert.equal('object', typeof data.response['3']);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'getV4GroupsIdVariables', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4GroupsIdVariables - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4GroupsIdVariables('fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.key);
    //           assert.equal('string', data.response.value);
    //           assert.equal('string', data.response.variable_type);
    //           assert.equal(false, data.response.protected);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'postV4GroupsIdVariables', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4GroupsIdVariablesKey - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4GroupsIdVariablesKey('fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.key);
    //           assert.equal('string', data.response.value);
    //           assert.equal('string', data.response.variable_type);
    //           assert.equal(false, data.response.protected);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'putV4GroupsIdVariablesKey', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4GroupsIdVariableKey - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4GroupsIdVariableKey('fakedata', 'fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'deleteV4GroupsIdVariableKey', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4GroupsIdMilestones - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4GroupsIdMilestones(groupId, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response['0']);
                assert.equal('object', typeof data.response['1']);
                assert.equal('object', typeof data.response['2']);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Groups', 'getV4GroupsIdMilestones', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#postV4GroupsIdMilestones - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4GroupsIdMilestones('fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(7, data.response.id);
    //           assert.equal(1, data.response.iid);
    //           assert.equal(10, data.response.group_id);
    //           assert.equal('string', data.response.title);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.due_date);
    //           assert.equal('string', data.response.start_date);
    //           assert.equal('string', data.response.state);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('string', data.response.created_at);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'postV4GroupsIdMilestones', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4GroupsIdMilestonesMilestonesId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4GroupsIdMilestonesMilestonesId('fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(5, data.response.id);
    //           assert.equal(4, data.response.iid);
    //           assert.equal(5, data.response.group_id);
    //           assert.equal('string', data.response.title);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.due_date);
    //           assert.equal('string', data.response.start_date);
    //           assert.equal('string', data.response.state);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('string', data.response.created_at);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'getV4GroupsIdMilestonesMilestonesId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4GroupsIdMilestonesMilestonesId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4GroupsIdMilestonesMilestonesId('fakedata', 'fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(7, data.response.id);
    //           assert.equal(1, data.response.iid);
    //           assert.equal(1, data.response.group_id);
    //           assert.equal('string', data.response.title);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.due_date);
    //           assert.equal('string', data.response.start_date);
    //           assert.equal('string', data.response.state);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('string', data.response.created_at);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'putV4GroupsIdMilestonesMilestonesId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4GroupsIdMilestonesMilestonesId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4GroupsIdMilestonesMilestonesId('fakedata', 'fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'deleteV4GroupsIdMilestonesMilestonesId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4GroupsIdMilestonesMilestoneIdMergeRequests - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4GroupsIdMilestonesMilestoneIdMergeRequests('fakedata', 'fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'getV4GroupsIdMilestonesMilestoneIdMergeRequests', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4GroupsIdMilestonesMilestoneIdIssues - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4GroupsIdMilestonesMilestoneIdIssues('fakedata', 'fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'getV4GroupsIdMilestonesMilestoneIdIssues', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4GroupsIdMilestonesMilestoneIdBurndownEvents - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4GroupsIdMilestonesMilestoneIdBurndownEvents('fakedata', 'fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'getV4GroupsIdMilestonesMilestoneIdBurndownEvents', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4GroupsIdEpicsEpicsIdResourceLabelEvents - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4GroupsIdEpicsEpicsIdResourceLabelEvents('fakedata', 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response['0']);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'getV4GroupsIdEpicsEpicsIdResourceLabelEvents', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4GroupsIdEpicsEpicsIdResourceLabelEventsReourceLabelEventId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4GroupsIdEpicsEpicsIdResourceLabelEventsReourceLabelEventId('fakedata', 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(4, data.response.id);
    //           assert.equal('object', typeof data.response.user);
    //           assert.equal('string', data.response.created_at);
    //           assert.equal('string', data.response.resource_type);
    //           assert.equal(4, data.response.resource_id);
    //           assert.equal('object', typeof data.response.label);
    //           assert.equal('string', data.response.action);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'getV4GroupsIdEpicsEpicsIdResourceLabelEventsReourceLabelEventId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4GroupsIdSearch - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4GroupsIdSearch('fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response['0']);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'getV4GroupsIdSearch', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4GroupsIdEpicsIdDiscussions - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4GroupsIdEpicsIdDiscussions('fakedata', 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response['0']);
    //           assert.equal('object', typeof data.response['1']);
    //           assert.equal('object', typeof data.response['2']);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'getV4GroupsIdEpicsIdDiscussions', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4GroupsIdEpicsIdDiscussions - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4GroupsIdEpicsIdDiscussions('fakedata', 12, 'fakedata', {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'postV4GroupsIdEpicsIdDiscussions', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4GroupsIdEpicsIdDiscussionsId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4GroupsIdEpicsIdDiscussionsId('fakedata', 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.id);
    //           assert.equal(true, data.response.individual_note);
    //           assert.equal(true, Array.isArray(data.response.notes));
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'getV4GroupsIdEpicsIdDiscussionsId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4GroupsIdEpicsIdDiscussionsIdNotes - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4GroupsIdEpicsIdDiscussionsIdNotes('fakedata', 12, 12, {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'postV4GroupsIdEpicsIdDiscussionsIdNotes', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4GroupsIdEpicsIdDiscussionsIdNoteId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4GroupsIdEpicsIdDiscussionsIdNoteId('fakedata', 12, 12, 12, 'fakedata', {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'putV4GroupsIdEpicsIdDiscussionsIdNoteId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4GroupsIdEpicsIdDiscussionsIdNoteId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4GroupsIdEpicsIdDiscussionsIdNoteId('fakedata', 12, 12, 12, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Groups', 'deleteV4GroupsIdEpicsIdDiscussionsIdNoteId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4Hooks - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4Hooks((data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.created_at);
    //           assert.equal('string', data.response.enable_ssl_verification);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.push_events);
    //           assert.equal('string', data.response.tag_push_events);
    //           assert.equal('string', data.response.url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Hooks', 'getV4Hooks', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4Hooks - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4Hooks({}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.created_at);
    //           assert.equal('string', data.response.enable_ssl_verification);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.push_events);
    //           assert.equal('string', data.response.tag_push_events);
    //           assert.equal('string', data.response.url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Hooks', 'postV4Hooks', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4HooksId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4HooksId(12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.created_at);
    //           assert.equal('string', data.response.enable_ssl_verification);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.push_events);
    //           assert.equal('string', data.response.tag_push_events);
    //           assert.equal('string', data.response.url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Hooks', 'getV4HooksId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4HooksId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4HooksId(12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.created_at);
    //           assert.equal('string', data.response.enable_ssl_verification);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.push_events);
    //           assert.equal('string', data.response.tag_push_events);
    //           assert.equal('string', data.response.url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Hooks', 'deleteV4HooksId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4Issues - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4Issues(null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.assignee);
                assert.equal('object', typeof data.response.author);
                assert.equal('string', data.response.confidential);
                assert.equal('string', data.response.created_at);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.downvotes);
                assert.equal('string', data.response.due_date);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.iid);
                assert.equal('string', data.response.labels);
                assert.equal('object', typeof data.response.milestone);
                assert.equal('string', data.response.project_id);
                assert.equal('string', data.response.state);
                assert.equal('string', data.response.subscribed);
                assert.equal('string', data.response.title);
                assert.equal('string', data.response.updated_at);
                assert.equal('string', data.response.upvotes);
                assert.equal('string', data.response.user_notes_count);
                assert.equal('string', data.response.web_url);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Issues', 'getV4Issues', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#getV4KeysId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4KeysId(12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.can_push);
    //           assert.equal('string', data.response.created_at);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.key);
    //           assert.equal('string', data.response.title);
    //           assert.equal('object', typeof data.response.user);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Keys', 'getV4KeysId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4Namespaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4Namespaces((data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(2, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.path);
                assert.equal('string', data.response.kind);
                assert.equal('string', data.response.full_path);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Namespaces', 'getV4Namespaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4NotificationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4NotificationSettings((data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.events);
                assert.equal('string', data.response.level);
                assert.equal('string', data.response.notification_email);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('NotificationSettings', 'getV4NotificationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putV4NotificationSettings - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4NotificationSettings({}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.events);
    //           assert.equal('string', data.response.level);
    //           assert.equal('string', data.response.notification_email);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('NotificationSettings', 'putV4NotificationSettings', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    let projectId = 13154870;
    let projectName;
    let projectOwnerId;
    let userNameSpace;
    const projectPostBody = {
      name: 'TestProject101',
      description: 'This is a test project.',
      visibility: 'private',
      initialize_with_readme: true
    };

    describe('#postV4Projects - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV4Projects(projectPostBody, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.archived);
                assert.equal('string', data.response.avatar_url);
                assert.equal('string', data.response.builds_enabled);
                assert.equal('string', data.response.container_registry_enabled);
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.creator_id);
                assert.equal('string', data.response.default_branch);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.forked_from_project);
                assert.equal('string', data.response.forks_count);
                assert.equal('string', data.response.http_url_to_repo);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.issues_enabled);
                assert.equal('string', data.response.last_activity_at);
                assert.equal('string', data.response.lfs_enabled);
                assert.equal('string', data.response.merge_requests_enabled);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.name_with_namespace);
                assert.equal('object', typeof data.response.namespace);
                assert.equal('string', data.response.only_allow_merge_if_all_discussions_are_resolved);
                assert.equal('string', data.response.only_allow_merge_if_build_succeeds);
                assert.equal('string', data.response.open_issues_count);
                assert.equal('object', typeof data.response.owner);
                assert.equal('string', data.response.pathParam);
                assert.equal('string', data.response.path_with_namespace);
                assert.equal('string', data.response.public);
                assert.equal('string', data.response.public_builds);
                assert.equal('string', data.response.request_access_enabled);
                assert.equal('string', data.response.runners_token);
                assert.equal('string', data.response.shared_runners_enabled);
                assert.equal('string', data.response.shared_with_groups);
                assert.equal('string', data.response.snippets_enabled);
                assert.equal('string', data.response.ssh_url_to_repo);
                assert.equal('string', data.response.star_count);
                assert.equal('object', typeof data.response.statistics);
                assert.equal('string', data.response.tagList);
                assert.equal('string', data.response.visibility_level);
                assert.equal('string', data.response.web_url);
                assert.equal('string', data.response.wiki_enabled);
              } else {
                runCommonAsserts(data, error);
              }

              projectId = data.response.id;
              projectName = data.response.name;
              projectOwnerId = data.response.owner.id;
              userNameSpace = data.response.namespace.name;
              saveMockData('Projects', 'postV4Projects', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const owned = true;

    describe('#getV4Projects - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4Projects(null, null, null, null, null, null, owned, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(8, data.response.id);
                assert.equal('string', data.response.default_branch);
                assert.equal('string', data.response.visibility);
                assert.equal('string', data.response.ssh_url_to_repo);
                assert.equal('string', data.response.http_url_to_repo);
                assert.equal('string', data.response.web_url);
                assert.equal('string', data.response.readme_url);
                assert.equal('object', typeof data.response.owner);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.name_with_namespace);
                assert.equal('string', data.response.pathParam);
                assert.equal('string', data.response.path_with_namespace);
                assert.equal(false, data.response.issues_enabled);
                assert.equal(7, data.response.open_issues_count);
                assert.equal(false, data.response.merge_requests_enabled);
                assert.equal(true, data.response.jobs_enabled);
                assert.equal(false, data.response.wiki_enabled);
                assert.equal(false, data.response.snippets_enabled);
                assert.equal(true, data.response.resolve_outdated_diff_discussions);
                assert.equal(true, data.response.container_registry_enabled);
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.last_activity_at);
                assert.equal(2, data.response.creator_id);
                assert.equal('object', typeof data.response.namespace);
                assert.equal('string', data.response.import_status);
                assert.equal(false, data.response.archived);
                assert.equal('string', data.response.avatar_url);
                assert.equal(false, data.response.shared_runners_enabled);
                assert.equal(2, data.response.forks_count);
                assert.equal(2, data.response.star_count);
                assert.equal('string', data.response.runners_token);
                assert.equal(true, data.response.public_jobs);
                assert.equal(true, Array.isArray(data.response.shared_with_groups));
                assert.equal(true, data.response.only_allow_merge_if_pipeline_succeeds);
                assert.equal(true, data.response.only_allow_merge_if_all_discussions_are_resolved);
                assert.equal(false, data.response.request_access_enabled);
                assert.equal('string', data.response.merge_method);
                assert.equal(1, data.response.approvals_before_merge);
                assert.equal('object', typeof data.response.statistics);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4Projects', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#postV4ProjectsUserUserId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsUserUserId(12, {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.archived);
    //           assert.equal('string', data.response.avatar_url);
    //           assert.equal('string', data.response.builds_enabled);
    //           assert.equal('string', data.response.container_registry_enabled);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.creator_id);
    //           assert.equal('string', data.response.default_branch);
    //           assert.equal('string', data.response.description);
    //           assert.equal('object', typeof data.response.forked_from_project);
    //           assert.equal('string', data.response.forks_count);
    //           assert.equal('string', data.response.http_url_to_repo);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.issues_enabled);
    //           assert.equal('string', data.response.last_activity_at);
    //           assert.equal('string', data.response.lfs_enabled);
    //           assert.equal('string', data.response.merge_requests_enabled);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.name_with_namespace);
    //           assert.equal('object', typeof data.response.namespace);
    //           assert.equal('string', data.response.only_allow_merge_if_all_discussions_are_resolved);
    //           assert.equal('string', data.response.only_allow_merge_if_build_succeeds);
    //           assert.equal('string', data.response.open_issues_count);
    //           assert.equal('object', typeof data.response.owner);
    //           assert.equal('string', data.response.pathParam);
    //           assert.equal('string', data.response.path_with_namespace);
    //           assert.equal('string', data.response.public);
    //           assert.equal('string', data.response.public_builds);
    //           assert.equal('string', data.response.request_access_enabled);
    //           assert.equal('string', data.response.runners_token);
    //           assert.equal('string', data.response.shared_runners_enabled);
    //           assert.equal('string', data.response.shared_with_groups);
    //           assert.equal('string', data.response.snippets_enabled);
    //           assert.equal('string', data.response.ssh_url_to_repo);
    //           assert.equal('string', data.response.star_count);
    //           assert.equal('object', typeof data.response.statistics);
    //           assert.equal('string', data.response.tagList);
    //           assert.equal('string', data.response.visibility_level);
    //           assert.equal('string', data.response.web_url);
    //           assert.equal('string', data.response.wiki_enabled);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsUserUserId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsId(projectId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.archived);
                assert.equal('string', data.response.avatar_url);
                assert.equal('string', data.response.builds_enabled);
                assert.equal('string', data.response.container_registry_enabled);
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.creator_id);
                assert.equal('string', data.response.default_branch);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.forked_from_project);
                assert.equal('string', data.response.forks_count);
                assert.equal('string', data.response.http_url_to_repo);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.issues_enabled);
                assert.equal('string', data.response.last_activity_at);
                assert.equal('string', data.response.lfs_enabled);
                assert.equal('string', data.response.merge_requests_enabled);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.name_with_namespace);
                assert.equal('object', typeof data.response.namespace);
                assert.equal('string', data.response.only_allow_merge_if_all_discussions_are_resolved);
                assert.equal('string', data.response.only_allow_merge_if_build_succeeds);
                assert.equal('string', data.response.open_issues_count);
                assert.equal('object', typeof data.response.owner);
                assert.equal('string', data.response.pathParam);
                assert.equal('string', data.response.path_with_namespace);
                assert.equal('string', data.response.permissions);
                assert.equal('string', data.response.public);
                assert.equal('string', data.response.public_builds);
                assert.equal('string', data.response.request_access_enabled);
                assert.equal('string', data.response.runners_token);
                assert.equal('string', data.response.shared_runners_enabled);
                assert.equal('string', data.response.shared_with_groups);
                assert.equal('string', data.response.snippets_enabled);
                assert.equal('string', data.response.ssh_url_to_repo);
                assert.equal('string', data.response.star_count);
                assert.equal('object', typeof data.response.statistics);
                assert.equal('string', data.response.tagList);
                assert.equal('string', data.response.visibility_level);
                assert.equal('string', data.response.web_url);
                assert.equal('string', data.response.wiki_enabled);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#approveV4Project - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.approveV4Project(projectId, {}, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const projectPutBody = {
      description: 'This description changed.'
    };

    describe('#putV4ProjectsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putV4ProjectsId(projectId, projectPutBody, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.archived);
                assert.equal('string', data.response.avatar_url);
                assert.equal('string', data.response.builds_enabled);
                assert.equal('string', data.response.container_registry_enabled);
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.creator_id);
                assert.equal('string', data.response.default_branch);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.forked_from_project);
                assert.equal('string', data.response.forks_count);
                assert.equal('string', data.response.http_url_to_repo);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.issues_enabled);
                assert.equal('string', data.response.last_activity_at);
                assert.equal('string', data.response.lfs_enabled);
                assert.equal('string', data.response.merge_requests_enabled);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.name_with_namespace);
                assert.equal('object', typeof data.response.namespace);
                assert.equal('string', data.response.only_allow_merge_if_all_discussions_are_resolved);
                assert.equal('string', data.response.only_allow_merge_if_build_succeeds);
                assert.equal('string', data.response.open_issues_count);
                assert.equal('object', typeof data.response.owner);
                assert.equal('string', data.response.pathParam);
                assert.equal('string', data.response.path_with_namespace);
                assert.equal('string', data.response.public);
                assert.equal('string', data.response.public_builds);
                assert.equal('string', data.response.request_access_enabled);
                assert.equal('string', data.response.runners_token);
                assert.equal('string', data.response.shared_runners_enabled);
                assert.equal('string', data.response.shared_with_groups);
                assert.equal('string', data.response.snippets_enabled);
                assert.equal('string', data.response.ssh_url_to_repo);
                assert.equal('string', data.response.star_count);
                assert.equal('object', typeof data.response.statistics);
                assert.equal('string', data.response.tagList);
                assert.equal('string', data.response.visibility_level);
                assert.equal('string', data.response.web_url);
                assert.equal('string', data.response.wiki_enabled);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'putV4ProjectsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#deleteV4ProjectsId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsId('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdAccessRequests - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdAccessRequests('fakedata', 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.avatar_url);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.requested_at);
    //           assert.equal('string', data.response.state);
    //           assert.equal('string', data.response.username);
    //           assert.equal('string', data.response.web_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdAccessRequests', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdAccessRequests - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdAccessRequests('fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.avatar_url);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.requested_at);
    //           assert.equal('string', data.response.state);
    //           assert.equal('string', data.response.username);
    //           assert.equal('string', data.response.web_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdAccessRequests', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdAccessRequestsUserId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdAccessRequestsUserId('fakedata', 12, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdAccessRequestsUserId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4ProjectsIdAccessRequestsUserIdApprove - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdAccessRequestsUserIdApprove('fakedata', 12, {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.access_level);
    //           assert.equal('string', data.response.avatar_url);
    //           assert.equal('string', data.response.expires_at);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.state);
    //           assert.equal('string', data.response.username);
    //           assert.equal('string', data.response.web_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdAccessRequestsUserIdApprove', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#postV4ProjectsIdArchive - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV4ProjectsIdArchive(projectId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.archived);
                assert.equal('string', data.response.avatar_url);
                assert.equal('string', data.response.builds_enabled);
                assert.equal('string', data.response.container_registry_enabled);
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.creator_id);
                assert.equal('string', data.response.default_branch);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.forked_from_project);
                assert.equal('string', data.response.forks_count);
                assert.equal('string', data.response.http_url_to_repo);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.issues_enabled);
                assert.equal('string', data.response.last_activity_at);
                assert.equal('string', data.response.lfs_enabled);
                assert.equal('string', data.response.merge_requests_enabled);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.name_with_namespace);
                assert.equal('object', typeof data.response.namespace);
                assert.equal('string', data.response.only_allow_merge_if_all_discussions_are_resolved);
                assert.equal('string', data.response.only_allow_merge_if_build_succeeds);
                assert.equal('string', data.response.open_issues_count);
                assert.equal('object', typeof data.response.owner);
                assert.equal('string', data.response.pathParam);
                assert.equal('string', data.response.path_with_namespace);
                assert.equal('string', data.response.public);
                assert.equal('string', data.response.public_builds);
                assert.equal('string', data.response.request_access_enabled);
                assert.equal('string', data.response.runners_token);
                assert.equal('string', data.response.shared_runners_enabled);
                assert.equal('string', data.response.shared_with_groups);
                assert.equal('string', data.response.snippets_enabled);
                assert.equal('string', data.response.ssh_url_to_repo);
                assert.equal('string', data.response.star_count);
                assert.equal('object', typeof data.response.statistics);
                assert.equal('string', data.response.tagList);
                assert.equal('string', data.response.visibility_level);
                assert.equal('string', data.response.web_url);
                assert.equal('string', data.response.wiki_enabled);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'postV4ProjectsIdArchive', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdUnarchive - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV4ProjectsIdUnarchive(projectId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.archived);
                assert.equal('string', data.response.avatar_url);
                assert.equal('string', data.response.builds_enabled);
                assert.equal('string', data.response.container_registry_enabled);
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.creator_id);
                assert.equal('string', data.response.default_branch);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.forked_from_project);
                assert.equal('string', data.response.forks_count);
                assert.equal('string', data.response.http_url_to_repo);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.issues_enabled);
                assert.equal('string', data.response.last_activity_at);
                assert.equal('string', data.response.lfs_enabled);
                assert.equal('string', data.response.merge_requests_enabled);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.name_with_namespace);
                assert.equal('object', typeof data.response.namespace);
                assert.equal('string', data.response.only_allow_merge_if_all_discussions_are_resolved);
                assert.equal('string', data.response.only_allow_merge_if_build_succeeds);
                assert.equal('string', data.response.open_issues_count);
                assert.equal('object', typeof data.response.owner);
                assert.equal('string', data.response.pathParam);
                assert.equal('string', data.response.path_with_namespace);
                assert.equal('string', data.response.public);
                assert.equal('string', data.response.public_builds);
                assert.equal('string', data.response.request_access_enabled);
                assert.equal('string', data.response.runners_token);
                assert.equal('string', data.response.shared_runners_enabled);
                assert.equal('string', data.response.shared_with_groups);
                assert.equal('string', data.response.snippets_enabled);
                assert.equal('string', data.response.ssh_url_to_repo);
                assert.equal('string', data.response.star_count);
                assert.equal('object', typeof data.response.statistics);
                assert.equal('string', data.response.tagList);
                assert.equal('string', data.response.visibility_level);
                assert.equal('string', data.response.web_url);
                assert.equal('string', data.response.wiki_enabled);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'postV4ProjectsIdUnarchive', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdBoards - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdBoards(projectId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.lists);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdBoards', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#getV4ProjectsIdBoardsBoardIdLists - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdBoardsBoardIdLists('fakedata', 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.id);
    //           assert.equal('object', typeof data.response.label);
    //           assert.equal('string', data.response.position);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdBoardsBoardIdLists', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdBoardsBoardIdLists - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdBoardsBoardIdLists('fakedata', 12, {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.id);
    //           assert.equal('object', typeof data.response.label);
    //           assert.equal('string', data.response.position);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdBoardsBoardIdLists', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdBoardsBoardIdListsListId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdBoardsBoardIdListsListId('fakedata', 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.id);
    //           assert.equal('object', typeof data.response.label);
    //           assert.equal('string', data.response.position);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdBoardsBoardIdListsListId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4ProjectsIdBoardsBoardIdListsListId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdBoardsBoardIdListsListId('fakedata', 12, 12, {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.id);
    //           assert.equal('object', typeof data.response.label);
    //           assert.equal('string', data.response.position);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdBoardsBoardIdListsListId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdBoardsBoardIdListsListId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdBoardsBoardIdListsListId('fakedata', 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.id);
    //           assert.equal('object', typeof data.response.label);
    //           assert.equal('string', data.response.position);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdBoardsBoardIdListsListId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdDeployKeys - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdDeployKeys(projectId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.can_push);
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.key);
                assert.equal('string', data.response.title);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdDeployKeys', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#postV4ProjectsIdDeployKeys - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdDeployKeys('fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.can_push);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.key);
    //           assert.equal('string', data.response.title);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdDeployKeys', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdDeployKeysKeyId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdDeployKeysKeyId('fakedata', 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.can_push);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.key);
    //           assert.equal('string', data.response.title);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdDeployKeysKeyId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdDeployKeysKeyId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdDeployKeysKeyId('fakedata', 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.can_push);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.key);
    //           assert.equal('string', data.response.title);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdDeployKeysKeyId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdDeployKeysKeyIdEnable - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdDeployKeysKeyIdEnable('fakedata', 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.can_push);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.key);
    //           assert.equal('string', data.response.title);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdDeployKeysKeyIdEnable', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdDeployments - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdDeployments(projectId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.createdAt);
                assert.equal('object', typeof data.response.deployable);
                assert.equal('object', typeof data.response.environment);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.iid);
                assert.equal('string', data.response.ref);
                assert.equal('string', data.response.sha);
                assert.equal('object', typeof data.response.user);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdDeployments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#getV4ProjectsIdDeploymentsDeploymentId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdDeploymentsDeploymentId('fakedata', 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('object', typeof data.response.deployable);
    //           assert.equal('object', typeof data.response.environment);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.iid);
    //           assert.equal('string', data.response.ref);
    //           assert.equal('string', data.response.sha);
    //           assert.equal('object', typeof data.response.user);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdDeploymentsDeploymentId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    let projectEnvironmentId;
    const projectEnvironmentPostBody = {
      name: 'TestEnvironment101'
    };

    describe('#postV4ProjectsIdEnvironments - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV4ProjectsIdEnvironments(projectId, projectEnvironmentPostBody, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.external_url);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
              } else {
                runCommonAsserts(data, error);
              }

              projectEnvironmentId = data.response.id;
              saveMockData('Projects', 'postV4ProjectsIdEnvironments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdEnvironments - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdEnvironments(projectId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.external_url);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdEnvironments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsEnvironmentsEnvironmentId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsEnvironmentsEnvironmentId(projectId, projectEnvironmentId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.external_url);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsEnvironmentsEnvironmentId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const projectEnvironmentPutBody = {
      name: 'TestEnvironment101-changed'
    };

    describe('#putV4ProjectsIdEnvironmentsEnvironmentId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putV4ProjectsIdEnvironmentsEnvironmentId(projectId, projectEnvironmentId, projectEnvironmentPutBody, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.external_url);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'putV4ProjectsIdEnvironmentsEnvironmentId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsEnvironmentsEnvironmentIdStop - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV4ProjectsEnvironmentsEnvironmentIdStop(projectId, projectEnvironmentId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.external_url);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'postV4ProjectsEnvironmentsEnvironmentIdStop', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdEnvironmentsEnvironmentId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteV4ProjectsIdEnvironmentsEnvironmentId(projectId, projectEnvironmentId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'deleteV4ProjectsIdEnvironmentsEnvironmentId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdEvents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdEvents(projectId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.action_name);
                assert.equal('object', typeof data.response.author);
                assert.equal('string', data.response.author_id);
                assert.equal('string', data.response.author_username);
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.data);
                assert.equal('object', typeof data.response.note);
                assert.equal('string', data.response.project_id);
                assert.equal('string', data.response.target_id);
                assert.equal('string', data.response.target_title);
                assert.equal('string', data.response.target_type);
                assert.equal('string', data.response.title);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdEvents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const forkFromProjectId = 12751015;
    let newForkProjectId;
    const projectForkPostBody = {
      namespace: userNameSpace
    };

    describe('#postV4ProjectsIdFork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV4ProjectsIdFork(forkFromProjectId, projectForkPostBody, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(9, data.response.id);
                assert.equal('object', typeof data.response.description);
                assert.equal('string', data.response.default_branch);
                assert.equal('string', data.response.visibility);
                assert.equal('string', data.response.ssh_url_to_repo);
                assert.equal('string', data.response.http_url_to_repo);
                assert.equal('string', data.response.web_url);
                assert.equal('string', data.response.readme_url);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.name_with_namespace);
                assert.equal('string', data.response.pathParam);
                assert.equal('string', data.response.path_with_namespace);
                assert.equal(false, data.response.issues_enabled);
                assert.equal(6, data.response.open_issues_count);
                assert.equal(false, data.response.merge_requests_enabled);
                assert.equal(true, data.response.jobs_enabled);
                assert.equal(false, data.response.wiki_enabled);
                assert.equal(true, data.response.snippets_enabled);
                assert.equal(true, data.response.resolve_outdated_diff_discussions);
                assert.equal(true, data.response.container_registry_enabled);
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.last_activity_at);
                assert.equal(9, data.response.creator_id);
                assert.equal('object', typeof data.response.namespace);
                assert.equal('string', data.response.import_status);
                assert.equal(false, data.response.archived);
                assert.equal('string', data.response.avatar_url);
                assert.equal(false, data.response.shared_runners_enabled);
                assert.equal(1, data.response.forks_count);
                assert.equal(4, data.response.star_count);
                assert.equal(true, data.response.public_jobs);
                assert.equal(true, Array.isArray(data.response.shared_with_groups));
                assert.equal(false, data.response.only_allow_merge_if_pipeline_succeeds);
                assert.equal(true, data.response.only_allow_merge_if_all_discussions_are_resolved);
                assert.equal(true, data.response.request_access_enabled);
                assert.equal('string', data.response.merge_method);
              } else {
                runCommonAsserts(data, error);
              }

              newForkProjectId = data.response.id;
              saveMockData('Projects', 'postV4ProjectsIdFork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdForks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdForks(null, null, null, null, null, null, null, null, null, null, null, null, null, null, forkFromProjectId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(9, data.response.id);
                assert.equal('object', typeof data.response.description);
                assert.equal('string', data.response.default_branch);
                assert.equal('string', data.response.visibility);
                assert.equal('string', data.response.ssh_url_to_repo);
                assert.equal('string', data.response.http_url_to_repo);
                assert.equal('string', data.response.web_url);
                assert.equal('string', data.response.readme_url);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.name_with_namespace);
                assert.equal('string', data.response.pathParam);
                assert.equal('string', data.response.path_with_namespace);
                assert.equal(true, data.response.issues_enabled);
                assert.equal(2, data.response.open_issues_count);
                assert.equal(true, data.response.merge_requests_enabled);
                assert.equal(true, data.response.jobs_enabled);
                assert.equal(false, data.response.wiki_enabled);
                assert.equal(true, data.response.snippets_enabled);
                assert.equal(true, data.response.resolve_outdated_diff_discussions);
                assert.equal(true, data.response.container_registry_enabled);
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.last_activity_at);
                assert.equal(1, data.response.creator_id);
                assert.equal('object', typeof data.response.namespace);
                assert.equal('string', data.response.import_status);
                assert.equal(true, data.response.archived);
                assert.equal('string', data.response.avatar_url);
                assert.equal(false, data.response.shared_runners_enabled);
                assert.equal(2, data.response.forks_count);
                assert.equal(9, data.response.star_count);
                assert.equal(true, data.response.public_jobs);
                assert.equal(true, Array.isArray(data.response.shared_with_groups));
                assert.equal(false, data.response.only_allow_merge_if_pipeline_succeeds);
                assert.equal(true, data.response.only_allow_merge_if_all_discussions_are_resolved);
                assert.equal(true, data.response.request_access_enabled);
                assert.equal('string', data.response.merge_method);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdForks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#postV4ProjectsIdForkForkedFromId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdForkForkedFromId('fakedata', 'fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdForkForkedFromId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#deleteV4ProjectsIdFork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteV4ProjectsIdFork(newForkProjectId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'deleteV4ProjectsIdFork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdStar - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV4ProjectsIdStar(projectId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.archived);
                assert.equal('string', data.response.avatar_url);
                assert.equal('string', data.response.builds_enabled);
                assert.equal('string', data.response.container_registry_enabled);
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.creator_id);
                assert.equal('string', data.response.default_branch);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.forked_from_project);
                assert.equal('string', data.response.forks_count);
                assert.equal('string', data.response.http_url_to_repo);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.issues_enabled);
                assert.equal('string', data.response.last_activity_at);
                assert.equal('string', data.response.lfs_enabled);
                assert.equal('string', data.response.merge_requests_enabled);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.name_with_namespace);
                assert.equal('object', typeof data.response.namespace);
                assert.equal('string', data.response.only_allow_merge_if_all_discussions_are_resolved);
                assert.equal('string', data.response.only_allow_merge_if_build_succeeds);
                assert.equal('string', data.response.open_issues_count);
                assert.equal('object', typeof data.response.owner);
                assert.equal('string', data.response.pathParam);
                assert.equal('string', data.response.path_with_namespace);
                assert.equal('string', data.response.public);
                assert.equal('string', data.response.public_builds);
                assert.equal('string', data.response.request_access_enabled);
                assert.equal('string', data.response.runners_token);
                assert.equal('string', data.response.shared_runners_enabled);
                assert.equal('string', data.response.shared_with_groups);
                assert.equal('string', data.response.snippets_enabled);
                assert.equal('string', data.response.ssh_url_to_repo);
                assert.equal('string', data.response.star_count);
                assert.equal('object', typeof data.response.statistics);
                assert.equal('string', data.response.tagList);
                assert.equal('string', data.response.visibility_level);
                assert.equal('string', data.response.web_url);
                assert.equal('string', data.response.wiki_enabled);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'postV4ProjectsIdStar', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdUnstar - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV4ProjectsIdUnstar(projectId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.archived);
                assert.equal('string', data.response.avatar_url);
                assert.equal('string', data.response.builds_enabled);
                assert.equal('string', data.response.container_registry_enabled);
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.creator_id);
                assert.equal('string', data.response.default_branch);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.forked_from_project);
                assert.equal('string', data.response.forks_count);
                assert.equal('string', data.response.http_url_to_repo);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.issues_enabled);
                assert.equal('string', data.response.last_activity_at);
                assert.equal('string', data.response.lfs_enabled);
                assert.equal('string', data.response.merge_requests_enabled);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.name_with_namespace);
                assert.equal('object', typeof data.response.namespace);
                assert.equal('string', data.response.only_allow_merge_if_all_discussions_are_resolved);
                assert.equal('string', data.response.only_allow_merge_if_build_succeeds);
                assert.equal('string', data.response.open_issues_count);
                assert.equal('object', typeof data.response.owner);
                assert.equal('string', data.response.pathParam);
                assert.equal('string', data.response.path_with_namespace);
                assert.equal('string', data.response.public);
                assert.equal('string', data.response.public_builds);
                assert.equal('string', data.response.request_access_enabled);
                assert.equal('string', data.response.runners_token);
                assert.equal('string', data.response.shared_runners_enabled);
                assert.equal('string', data.response.shared_with_groups);
                assert.equal('string', data.response.snippets_enabled);
                assert.equal('string', data.response.ssh_url_to_repo);
                assert.equal('string', data.response.star_count);
                assert.equal('object', typeof data.response.statistics);
                assert.equal('string', data.response.tagList);
                assert.equal('string', data.response.visibility_level);
                assert.equal('string', data.response.web_url);
                assert.equal('string', data.response.wiki_enabled);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'postV4ProjectsIdUnstar', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdHooks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdHooks(projectId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.build_events);
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.enable_ssl_verification);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.issues_events);
                assert.equal('string', data.response.merge_requests_events);
                assert.equal('string', data.response.note_events);
                assert.equal('string', data.response.pipeline_events);
                assert.equal('string', data.response.project_id);
                assert.equal('string', data.response.push_events);
                assert.equal('string', data.response.tag_push_events);
                assert.equal('string', data.response.url);
                assert.equal('string', data.response.wiki_page_events);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdHooks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#postV4ProjectsIdHooks - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdHooks('fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.build_events);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.enable_ssl_verification);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.issues_events);
    //           assert.equal('string', data.response.merge_requests_events);
    //           assert.equal('string', data.response.note_events);
    //           assert.equal('string', data.response.pipeline_events);
    //           assert.equal('string', data.response.project_id);
    //           assert.equal('string', data.response.push_events);
    //           assert.equal('string', data.response.tag_push_events);
    //           assert.equal('string', data.response.url);
    //           assert.equal('string', data.response.wiki_page_events);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdHooks', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdHooksHookId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdHooksHookId('fakedata', 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.build_events);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.enable_ssl_verification);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.issues_events);
    //           assert.equal('string', data.response.merge_requests_events);
    //           assert.equal('string', data.response.note_events);
    //           assert.equal('string', data.response.pipeline_events);
    //           assert.equal('string', data.response.project_id);
    //           assert.equal('string', data.response.push_events);
    //           assert.equal('string', data.response.tag_push_events);
    //           assert.equal('string', data.response.url);
    //           assert.equal('string', data.response.wiki_page_events);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdHooksHookId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4ProjectsIdHooksHookId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdHooksHookId('fakedata', 12, {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.build_events);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.enable_ssl_verification);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.issues_events);
    //           assert.equal('string', data.response.merge_requests_events);
    //           assert.equal('string', data.response.note_events);
    //           assert.equal('string', data.response.pipeline_events);
    //           assert.equal('string', data.response.project_id);
    //           assert.equal('string', data.response.push_events);
    //           assert.equal('string', data.response.tag_push_events);
    //           assert.equal('string', data.response.url);
    //           assert.equal('string', data.response.wiki_page_events);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdHooksHookId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdHooksHookId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdHooksHookId('fakedata', 12, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdHooksHookId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    let projectIssueIid;
    const projectIssusesPostBody = {
      title: 'testIssue101'
    };

    describe('#postV4ProjectsIdIssues - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV4ProjectsIdIssues(projectId, projectIssusesPostBody, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.assignee);
                assert.equal('object', typeof data.response.author);
                assert.equal('string', data.response.confidential);
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.downvotes);
                assert.equal('string', data.response.due_date);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.iid);
                assert.equal('string', data.response.labels);
                assert.equal('object', typeof data.response.milestone);
                assert.equal('string', data.response.project_id);
                assert.equal('string', data.response.state);
                assert.equal('string', data.response.subscribed);
                assert.equal('string', data.response.title);
                assert.equal('string', data.response.updated_at);
                assert.equal('string', data.response.upvotes);
                assert.equal('string', data.response.user_notes_count);
                assert.equal('string', data.response.web_url);
              } else {
                runCommonAsserts(data, error);
              }

              projectIssueIid = data.response.iid;
              saveMockData('Projects', 'postV4ProjectsIdIssues', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdIssues - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdIssues(projectId, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.assignee);
                assert.equal('object', typeof data.response.author);
                assert.equal('string', data.response.confidential);
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.downvotes);
                assert.equal('string', data.response.due_date);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.iid);
                assert.equal('string', data.response.labels);
                assert.equal('object', typeof data.response.milestone);
                assert.equal('string', data.response.project_id);
                assert.equal('string', data.response.state);
                assert.equal('string', data.response.subscribed);
                assert.equal('string', data.response.title);
                assert.equal('string', data.response.updated_at);
                assert.equal('string', data.response.upvotes);
                assert.equal('string', data.response.user_notes_count);
                assert.equal('string', data.response.web_url);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdIssues', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdIssuesIssueId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdIssuesIssueId(projectId, projectIssueIid, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.assignee);
                assert.equal('object', typeof data.response.author);
                assert.equal('string', data.response.confidential);
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.downvotes);
                assert.equal('string', data.response.due_date);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.iid);
                assert.equal('string', data.response.labels);
                assert.equal('object', typeof data.response.milestone);
                assert.equal('string', data.response.project_id);
                assert.equal('string', data.response.state);
                assert.equal('string', data.response.subscribed);
                assert.equal('string', data.response.title);
                assert.equal('string', data.response.updated_at);
                assert.equal('string', data.response.upvotes);
                assert.equal('string', data.response.user_notes_count);
                assert.equal('string', data.response.web_url);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdIssuesIssueId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const projectIssuePutBody = {
      title: 'testIssue101-changed'
    };

    describe('#putV4ProjectsIdIssuesIssueId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putV4ProjectsIdIssuesIssueId(projectId, projectIssueIid, projectIssuePutBody, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.assignee);
                assert.equal('object', typeof data.response.author);
                assert.equal('string', data.response.confidential);
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.downvotes);
                assert.equal('string', data.response.due_date);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.iid);
                assert.equal('string', data.response.labels);
                assert.equal('object', typeof data.response.milestone);
                assert.equal('string', data.response.project_id);
                assert.equal('string', data.response.state);
                assert.equal('string', data.response.subscribed);
                assert.equal('string', data.response.title);
                assert.equal('string', data.response.updated_at);
                assert.equal('string', data.response.upvotes);
                assert.equal('string', data.response.user_notes_count);
                assert.equal('string', data.response.web_url);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'putV4ProjectsIdIssuesIssueId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let projectIssueEmojiId;
    const projectIssueEmojiPostBody = {
      name: 'blowfish'
    };

    describe('#postV4ProjectsIdIssuesIssueIdAwardEmoji - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV4ProjectsIdIssuesIssueIdAwardEmoji(projectId, projectIssueIid, projectIssueEmojiPostBody, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.awardable_id);
                assert.equal('string', data.response.awardable_type);
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.updated_at);
                assert.equal('object', typeof data.response.user);
              } else {
                runCommonAsserts(data, error);
              }

              projectIssueEmojiId = data.response.id;
              saveMockData('Projects', 'postV4ProjectsIdIssuesIssueIdAwardEmoji', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdIssuesIssueIdAwardEmoji - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdIssuesIssueIdAwardEmoji(projectId, projectIssueIid, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.awardable_id);
                assert.equal('string', data.response.awardable_type);
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.updated_at);
                assert.equal('object', typeof data.response.user);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdIssuesIssueIdAwardEmoji', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdIssuesIssueIdAwardEmojiAwardId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdIssuesIssueIdAwardEmojiAwardId(projectId, projectIssueIid, projectIssueEmojiId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.awardable_id);
                assert.equal('string', data.response.awardable_type);
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.updated_at);
                assert.equal('object', typeof data.response.user);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdIssuesIssueIdAwardEmojiAwardId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdIssuesIssueIdAwardEmojiAwardId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteV4ProjectsIdIssuesIssueIdAwardEmojiAwardId(projectId, projectIssueIid, projectIssueEmojiId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.awardable_id);
                assert.equal('string', data.response.awardable_type);
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.updated_at);
                assert.equal('object', typeof data.response.user);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'deleteV4ProjectsIdIssuesIssueIdAwardEmojiAwardId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const projectIssueAddSpentTimePostBody = {
      duration: '5h30m'
    };

    describe('#postV4ProjectsIdIssuesIssueIdAddSpentTime - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postV4ProjectsIdIssuesIssueIdAddSpentTime(projectId, projectIssueIid, projectIssueAddSpentTimePostBody, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'postV4ProjectsIdIssuesIssueIdAddSpentTime', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const projectIssueTimeEstimatePostBody = {
      duration: '5h30m'
    };

    describe('#postV4ProjectsIdIssuesIssueIdTimeEstimate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postV4ProjectsIdIssuesIssueIdTimeEstimate(projectId, projectIssueIid, projectIssueTimeEstimatePostBody, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'postV4ProjectsIdIssuesIssueIdTimeEstimate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdIssuesIssueIdTimeStats - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getV4ProjectsIdIssuesIssueIdTimeStats(projectId, projectIssueIid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdIssuesIssueIdTimeStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdIssuesIssueIdResetSpentTime - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postV4ProjectsIdIssuesIssueIdResetSpentTime(projectId, projectIssueIid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'postV4ProjectsIdIssuesIssueIdResetSpentTime', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdIssuesIssueIdResetTimeEstimate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postV4ProjectsIdIssuesIssueIdResetTimeEstimate(projectId, projectIssueIid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'postV4ProjectsIdIssuesIssueIdResetTimeEstimate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdIssuesIssueIdTodo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV4ProjectsIdIssuesIssueIdTodo(projectId, projectIssueIid, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.action_name);
                assert.equal('object', typeof data.response.author);
                assert.equal('string', data.response.bodyQuery);
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.project);
                assert.equal('string', data.response.state);
                assert.equal('string', data.response.target);
                assert.equal('string', data.response.target_type);
                assert.equal('string', data.response.target_url);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'postV4ProjectsIdIssuesIssueIdTodo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdLabels - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdLabels(projectId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.closed_issues_count);
                assert.equal('string', data.response.color);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.open_issues_count);
                assert.equal('string', data.response.open_merge_requests_count);
                assert.equal('string', data.response.priority);
                assert.equal('string', data.response.subscribed);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdLabels', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let projectLabelId;
    let projectLabelName;
    const projectLabelPostBody = {
      name: 'testLabel101',
      color: 'red'
    };

    describe('#postV4ProjectsIdLabels - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV4ProjectsIdLabels(projectId, projectLabelPostBody, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.closed_issues_count);
                assert.equal('string', data.response.color);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.open_issues_count);
                assert.equal('string', data.response.open_merge_requests_count);
                assert.equal('string', data.response.priority);
                assert.equal('string', data.response.subscribed);
              } else {
                runCommonAsserts(data, error);
              }

              projectLabelId = data.response.id;
              projectLabelName = data.response.name;
              saveMockData('Projects', 'postV4ProjectsIdLabels', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const projectLabelPutBody = {
      name: 'testLabel101',
      color: 'blue'
    };

    describe('#putV4ProjectsIdLabels - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putV4ProjectsIdLabels(projectId, projectLabelPutBody, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.closed_issues_count);
                assert.equal('string', data.response.color);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.open_issues_count);
                assert.equal('string', data.response.open_merge_requests_count);
                assert.equal('string', data.response.priority);
                assert.equal('string', data.response.subscribed);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'putV4ProjectsIdLabels', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#postV4ProjectsIdLabelsIdSubscribe - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdLabelsIdSubscribe('fakedata', 12, {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.closed_issues_count);
    //           assert.equal('string', data.response.color);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.open_issues_count);
    //           assert.equal('string', data.response.open_merge_requests_count);
    //           assert.equal('string', data.response.priority);
    //           assert.equal('string', data.response.subscribed);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdLabelsIdSubscribe', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdLabelsIdUnsubscribe - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdLabelsIdUnsubscribe('fakedata', 12, {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdLabelsIdUnsubscribe', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#deleteV4ProjectsIdLabels - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteV4ProjectsIdLabels(projectId, projectLabelName, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.closed_issues_count);
                assert.equal('string', data.response.color);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.open_issues_count);
                assert.equal('string', data.response.open_merge_requests_count);
                assert.equal('string', data.response.priority);
                assert.equal('string', data.response.subscribed);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'deleteV4ProjectsIdLabels', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let projectUserId;

    describe('#getV4ProjectsIdMembers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdMembers(projectId, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.access_level);
                assert.equal('string', data.response.avatar_url);
                assert.equal('string', data.response.expires_at);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.state);
                assert.equal('string', data.response.username);
                assert.equal('string', data.response.web_url);
                projectUserId = data.response.id;
              } else {
                runCommonAsserts(data, error);
                projectUserId = data.response[0].id;
              }

              saveMockData('Projects', 'getV4ProjectsIdMembers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#postV4ProjectsIdMembers - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdMembers('fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.access_level);
    //           assert.equal('string', data.response.avatar_url);
    //           assert.equal('string', data.response.expires_at);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.state);
    //           assert.equal('string', data.response.username);
    //           assert.equal('string', data.response.web_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdMembers', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdMembersUserId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdMembersUserId(projectId, projectUserId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.access_level);
                assert.equal('string', data.response.avatar_url);
                assert.equal('string', data.response.expires_at);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.state);
                assert.equal('string', data.response.username);
                assert.equal('string', data.response.web_url);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdMembersUserId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putV4ProjectsIdMembersUserId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdMembersUserId('fakedata', 12, {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.access_level);
    //           assert.equal('string', data.response.avatar_url);
    //           assert.equal('string', data.response.expires_at);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.state);
    //           assert.equal('string', data.response.username);
    //           assert.equal('string', data.response.web_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdMembersUserId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdMembersUserId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdMembersUserId('fakedata', 12, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdMembersUserId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdMergeRequests - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdMergeRequests(projectId, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.assignee);
                assert.equal('object', typeof data.response.author);
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.downvotes);
                assert.equal('string', data.response.force_remove_source_branch);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.iid);
                assert.equal('string', data.response.labels);
                assert.equal('string', data.response.merge_commit_sha);
                assert.equal('string', data.response.merge_status);
                assert.equal('string', data.response.merge_when_build_succeeds);
                assert.equal('object', typeof data.response.milestone);
                assert.equal('string', data.response.project_id);
                assert.equal('string', data.response.sha);
                assert.equal('string', data.response.should_remove_source_branch);
                assert.equal('string', data.response.source_branch);
                assert.equal('string', data.response.source_project_id);
                assert.equal('string', data.response.state);
                assert.equal('string', data.response.subscribed);
                assert.equal('string', data.response.target_branch);
                assert.equal('string', data.response.target_project_id);
                assert.equal('string', data.response.title);
                assert.equal('string', data.response.updated_at);
                assert.equal('string', data.response.upvotes);
                assert.equal('string', data.response.user_notes_count);
                assert.equal('string', data.response.web_url);
                assert.equal('string', data.response.work_in_progress);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdMergeRequests', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#postV4ProjectsIdMergeRequests - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdMergeRequests('fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response.assignee);
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.downvotes);
    //           assert.equal('string', data.response.force_remove_source_branch);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.iid);
    //           assert.equal('string', data.response.labels);
    //           assert.equal('string', data.response.merge_commit_sha);
    //           assert.equal('string', data.response.merge_status);
    //           assert.equal('string', data.response.merge_when_build_succeeds);
    //           assert.equal('object', typeof data.response.milestone);
    //           assert.equal('string', data.response.project_id);
    //           assert.equal('string', data.response.sha);
    //           assert.equal('string', data.response.should_remove_source_branch);
    //           assert.equal('string', data.response.source_branch);
    //           assert.equal('string', data.response.source_project_id);
    //           assert.equal('string', data.response.state);
    //           assert.equal('string', data.response.subscribed);
    //           assert.equal('string', data.response.target_branch);
    //           assert.equal('string', data.response.target_project_id);
    //           assert.equal('string', data.response.title);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('string', data.response.upvotes);
    //           assert.equal('string', data.response.user_notes_count);
    //           assert.equal('string', data.response.web_url);
    //           assert.equal('string', data.response.work_in_progress);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdMergeRequests', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    let projectMilestoneId;
    const projectMilestonePostBody = {
      title: 'testMilestone101'
    };

    describe('#postV4ProjectsIdMilestones - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV4ProjectsIdMilestones(projectId, projectMilestonePostBody, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.due_date);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.iid);
                assert.equal('string', data.response.project_id);
                assert.equal('string', data.response.start_date);
                assert.equal('string', data.response.state);
                assert.equal('string', data.response.title);
                assert.equal('string', data.response.updated_at);
              } else {
                runCommonAsserts(data, error);
              }

              projectMilestoneId = data.response.id;
              saveMockData('Projects', 'postV4ProjectsIdMilestones', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdMilestones - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdMilestones(projectId, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.due_date);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.iid);
                assert.equal('string', data.response.project_id);
                assert.equal('string', data.response.start_date);
                assert.equal('string', data.response.state);
                assert.equal('string', data.response.title);
                assert.equal('string', data.response.updated_at);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdMilestones', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdMilestonesMilestoneId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdMilestonesMilestoneId(projectId, projectMilestoneId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.due_date);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.iid);
                assert.equal('string', data.response.project_id);
                assert.equal('string', data.response.start_date);
                assert.equal('string', data.response.state);
                assert.equal('string', data.response.title);
                assert.equal('string', data.response.updated_at);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdMilestonesMilestoneId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const projectMilestonePutBody = {
      title: 'testMilestone101-changed'
    };

    describe('#putV4ProjectsIdMilestonesMilestoneId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putV4ProjectsIdMilestonesMilestoneId(projectId, projectMilestoneId, projectMilestonePutBody, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.due_date);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.iid);
                assert.equal('string', data.response.project_id);
                assert.equal('string', data.response.start_date);
                assert.equal('string', data.response.state);
                assert.equal('string', data.response.title);
                assert.equal('string', data.response.updated_at);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'putV4ProjectsIdMilestonesMilestoneId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdMilestonesMilestoneIdIssues - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdMilestonesMilestoneIdIssues(projectId, projectMilestoneId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.assignee);
                assert.equal('object', typeof data.response.author);
                assert.equal('string', data.response.confidential);
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.downvotes);
                assert.equal('string', data.response.due_date);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.iid);
                assert.equal('string', data.response.labels);
                assert.equal('object', typeof data.response.milestone);
                assert.equal('string', data.response.project_id);
                assert.equal('string', data.response.state);
                assert.equal('string', data.response.subscribed);
                assert.equal('string', data.response.title);
                assert.equal('string', data.response.updated_at);
                assert.equal('string', data.response.upvotes);
                assert.equal('string', data.response.user_notes_count);
                assert.equal('string', data.response.web_url);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdMilestonesMilestoneIdIssues', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdNotificationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdNotificationSettings(projectId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.events);
                assert.equal('string', data.response.level);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdNotificationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const projectNotificationSettingsPutBody = {
      level: 'watch'
    };

    describe('#putV4ProjectsIdNotificationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putV4ProjectsIdNotificationSettings(projectId, projectNotificationSettingsPutBody, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.events);
                assert.equal('string', data.response.level);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'putV4ProjectsIdNotificationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#postV4ProjectsIdPipeline - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdPipeline('fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.before_sha);
    //           assert.equal('string', data.response.committed_at);
    //           assert.equal('string', data.response.coverage);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.duration);
    //           assert.equal('string', data.response.finished_at);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.ref);
    //           assert.equal('string', data.response.sha);
    //           assert.equal('string', data.response.started_at);
    //           assert.equal('string', data.response.status);
    //           assert.equal('string', data.response.tag);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('object', typeof data.response.user);
    //           assert.equal('string', data.response.yaml_errors);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdPipeline', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdPipelines - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdPipelines(projectId, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.before_sha);
                assert.equal('string', data.response.committed_at);
                assert.equal('string', data.response.coverage);
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.duration);
                assert.equal('string', data.response.finished_at);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.ref);
                assert.equal('string', data.response.sha);
                assert.equal('string', data.response.started_at);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.tag);
                assert.equal('string', data.response.updated_at);
                assert.equal('object', typeof data.response.user);
                assert.equal('string', data.response.yaml_errors);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdPipelines', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#getV4ProjectsIdPipelinesPipelineId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdPipelinesPipelineId('fakedata', 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.before_sha);
    //           assert.equal('string', data.response.committed_at);
    //           assert.equal('string', data.response.coverage);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.duration);
    //           assert.equal('string', data.response.finished_at);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.ref);
    //           assert.equal('string', data.response.sha);
    //           assert.equal('string', data.response.started_at);
    //           assert.equal('string', data.response.status);
    //           assert.equal('string', data.response.tag);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('object', typeof data.response.user);
    //           assert.equal('string', data.response.yaml_errors);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdPipelinesPipelineId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdPipelinesPipelineIdCancel - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdPipelinesPipelineIdCancel('fakedata', 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.before_sha);
    //           assert.equal('string', data.response.committed_at);
    //           assert.equal('string', data.response.coverage);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.duration);
    //           assert.equal('string', data.response.finished_at);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.ref);
    //           assert.equal('string', data.response.sha);
    //           assert.equal('string', data.response.started_at);
    //           assert.equal('string', data.response.status);
    //           assert.equal('string', data.response.tag);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('object', typeof data.response.user);
    //           assert.equal('string', data.response.yaml_errors);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdPipelinesPipelineIdCancel', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdPipelinesPipelineIdRetry - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdPipelinesPipelineIdRetry('fakedata', 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.before_sha);
    //           assert.equal('string', data.response.committed_at);
    //           assert.equal('string', data.response.coverage);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.duration);
    //           assert.equal('string', data.response.finished_at);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.ref);
    //           assert.equal('string', data.response.sha);
    //           assert.equal('string', data.response.started_at);
    //           assert.equal('string', data.response.status);
    //           assert.equal('string', data.response.tag);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('object', typeof data.response.user);
    //           assert.equal('string', data.response.yaml_errors);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdPipelinesPipelineIdRetry', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdRepositoryArchive - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdRepositoryArchive('fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdRepositoryArchive', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdRepositoryBlobsSha - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdRepositoryBlobsSha('fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdRepositoryBlobsSha', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    let projectBranchName = 'master';

    describe('#getV4ProjectsIdRepositoryBranches - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdRepositoryBranches(projectId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.commit);
                assert.equal('string', data.response.developers_can_merge);
                assert.equal('string', data.response.developers_can_push);
                assert.equal('string', data.response.merged);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.protected);
                projectBranchName = data.response.name;
              } else {
                runCommonAsserts(data, error);
                projectBranchName = data.response[0].name;
              }

              saveMockData('Projects', 'getV4ProjectsIdRepositoryBranches', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const projectTestBranchName = 'testBranch';
    const projectBranchPostBody = {
      branch: projectTestBranchName,
      ref: 'master'
    };

    describe('#postV4ProjectsIdRepositoryBranches - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV4ProjectsIdRepositoryBranches(projectId, projectBranchPostBody, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.commit);
                assert.equal('string', data.response.developers_can_merge);
                assert.equal('string', data.response.developers_can_push);
                assert.equal('string', data.response.merged);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.protected);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'postV4ProjectsIdRepositoryBranches', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdRepositoryBranchesBranch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdRepositoryBranchesBranch(projectId, projectBranchName, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.commit);
                assert.equal('string', data.response.developers_can_merge);
                assert.equal('string', data.response.developers_can_push);
                assert.equal('string', data.response.merged);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.protected);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdRepositoryBranchesBranch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putV4ProjectsIdRepositoryBranchesBranchProtect - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdRepositoryBranchesBranchProtect('fakedata', 'fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.commit);
    //           assert.equal('string', data.response.developers_can_merge);
    //           assert.equal('string', data.response.developers_can_push);
    //           assert.equal('string', data.response.merged);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.protected);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdRepositoryBranchesBranchProtect', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4ProjectsIdRepositoryBranchesBranchUnprotect - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdRepositoryBranchesBranchUnprotect('fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.commit);
    //           assert.equal('string', data.response.developers_can_merge);
    //           assert.equal('string', data.response.developers_can_push);
    //           assert.equal('string', data.response.merged);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.protected);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdRepositoryBranchesBranchUnprotect', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#deleteV4ProjectsIdRepositoryBranchesBranch - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteV4ProjectsIdRepositoryBranchesBranch(projectId, projectTestBranchName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'deleteV4ProjectsIdRepositoryBranchesBranch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdRepositoryCommits - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdRepositoryCommits(projectId, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.author_email);
                assert.equal('string', data.response.author_name);
                assert.equal('string', data.response.committer_email);
                assert.equal('string', data.response.committer_name);
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.message);
                assert.equal('string', data.response.short_id);
                assert.equal('string', data.response.title);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdRepositoryCommits', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#postV4ProjectsIdRepositoryCommits - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdRepositoryCommits('fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.author_email);
    //           assert.equal('string', data.response.author_name);
    //           assert.equal('string', data.response.authored_date);
    //           assert.equal('string', data.response.committed_date);
    //           assert.equal('string', data.response.committer_email);
    //           assert.equal('string', data.response.committer_name);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.message);
    //           assert.equal('string', data.response.parent_ids);
    //           assert.equal('string', data.response.short_id);
    //           assert.equal('object', typeof data.response.stats);
    //           assert.equal('string', data.response.status);
    //           assert.equal('string', data.response.title);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdRepositoryCommits', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdRepositoryCommitsSha - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdRepositoryCommitsSha('fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.author_email);
    //           assert.equal('string', data.response.author_name);
    //           assert.equal('string', data.response.authored_date);
    //           assert.equal('string', data.response.committed_date);
    //           assert.equal('string', data.response.committer_email);
    //           assert.equal('string', data.response.committer_name);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.message);
    //           assert.equal('string', data.response.parent_ids);
    //           assert.equal('string', data.response.short_id);
    //           assert.equal('object', typeof data.response.stats);
    //           assert.equal('string', data.response.status);
    //           assert.equal('string', data.response.title);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdRepositoryCommitsSha', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdRepositoryCommitsShaCherryPick - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdRepositoryCommitsShaCherryPick('fakedata', 'fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.author_email);
    //           assert.equal('string', data.response.author_name);
    //           assert.equal('string', data.response.committer_email);
    //           assert.equal('string', data.response.committer_name);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.message);
    //           assert.equal('string', data.response.short_id);
    //           assert.equal('string', data.response.title);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdRepositoryCommitsShaCherryPick', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdRepositoryCommitsShaComments - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdRepositoryCommitsShaComments('fakedata', 12, 12, 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.line);
    //           assert.equal('string', data.response.line_type);
    //           assert.equal('string', data.response.note);
    //           assert.equal('string', data.response.pathParam);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdRepositoryCommitsShaComments', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdRepositoryCommitsShaComments - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdRepositoryCommitsShaComments('fakedata', 'fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.line);
    //           assert.equal('string', data.response.line_type);
    //           assert.equal('string', data.response.note);
    //           assert.equal('string', data.response.pathParam);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdRepositoryCommitsShaComments', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdRepositoryCommitsShaDiff - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdRepositoryCommitsShaDiff('fakedata', 'fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdRepositoryCommitsShaDiff', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdRepositoryCommitsShaStatuses - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdRepositoryCommitsShaStatuses('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.allow_failure);
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.finished_at);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.ref);
    //           assert.equal('string', data.response.sha);
    //           assert.equal('string', data.response.started_at);
    //           assert.equal('string', data.response.status);
    //           assert.equal('string', data.response.target_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdRepositoryCommitsShaStatuses', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdRepositoryCompare - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdRepositoryCompare(projectId, projectBranchName, projectBranchName, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.commit);
                assert.equal('object', typeof data.response.commits);
                assert.equal('string', data.response.compare_same_ref);
                assert.equal('string', data.response.compare_timeout);
                assert.equal('object', typeof data.response.diffs);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdRepositoryCompare', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdRepositoryContributors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdRepositoryContributors(projectId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.additions);
                assert.equal('string', data.response.commits);
                assert.equal('string', data.response.deletions);
                assert.equal('string', data.response.email);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdRepositoryContributors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#getV4ProjectsIdRepositoryRawBlobsSha - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdRepositoryRawBlobsSha('fakedata', 'fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdRepositoryRawBlobsSha', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    let projectTagName;
    const projectTagPostBody = {
      tag_name: 'testingTag',
      ref: 'master'
    };

    describe('#postV4ProjectsIdRepositoryTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV4ProjectsIdRepositoryTags(projectId, projectTagPostBody, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.commit);
                assert.equal('string', data.response.message);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.release);
              } else {
                runCommonAsserts(data, error);
              }

              projectTagName = data.response.name;
              saveMockData('Projects', 'postV4ProjectsIdRepositoryTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdRepositoryTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdRepositoryTags(projectId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.commit);
                assert.equal('string', data.response.message);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.release);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdRepositoryTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdRepositoryTagsTagName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdRepositoryTagsTagName(projectId, projectTagName, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.commit);
                assert.equal('string', data.response.message);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.release);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdRepositoryTagsTagName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const projectTagReleasePostBody = {
      description: 'this is a test!'
    };

    describe('#postV4ProjectsIdRepositoryTagsTagNameRelease - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV4ProjectsIdRepositoryTagsTagNameRelease(projectId, projectTagName, projectTagReleasePostBody, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.tagName);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'postV4ProjectsIdRepositoryTagsTagNameRelease', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const projectTagReleasePutBody = {
      description: 'this is a test!-changed'
    };

    describe('#putV4ProjectsIdRepositoryTagsTagNameRelease - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putV4ProjectsIdRepositoryTagsTagNameRelease(projectId, projectTagName, projectTagReleasePutBody, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.tagName);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'putV4ProjectsIdRepositoryTagsTagNameRelease', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdRepositoryTagsTagName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteV4ProjectsIdRepositoryTagsTagName(projectId, projectTagName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'deleteV4ProjectsIdRepositoryTagsTagName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let projectFileName;

    describe('#getV4ProjectsIdRepositoryTree - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdRepositoryTree(projectId, null, null, true, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.mode);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.pathParam);
                assert.equal('string', data.response.type);
                projectFileName = data.response.name;
              } else {
                runCommonAsserts(data, error);
                projectFileName = data.response[0].name;
              }

              saveMockData('Projects', 'getV4ProjectsIdRepositoryTree', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdRunners - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdRunners(projectId, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.active);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.is_shared);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdRunners', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#postV4ProjectsIdRunners - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdRunners('fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.active);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.is_shared);
    //           assert.equal('string', data.response.name);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdRunners', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdRunnersRunnerId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdRunnersRunnerId('fakedata', 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.active);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.is_shared);
    //           assert.equal('string', data.response.name);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdRunnersRunnerId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdServicesAsana - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getV4ProjectsIdServicesAsana(projectId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdServicesAsana', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putV4ProjectsIdServicesAsana - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdServicesAsana(12, {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdServicesAsana', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdServicesAsana - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdServicesAsana('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdServicesAsana', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdServicesAssembla - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getV4ProjectsIdServicesAssembla(projectId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdServicesAssembla', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putV4ProjectsIdServicesAssembla - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdServicesAssembla(12, {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdServicesAssembla', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdServicesAssembla - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdServicesAssembla('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdServicesAssembla', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdServicesBamboo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getV4ProjectsIdServicesBamboo(projectId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdServicesBamboo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putV4ProjectsIdServicesBamboo - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdServicesBamboo('fakedata', {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdServicesBamboo', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdServicesBamboo - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdServicesBamboo('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdServicesBamboo', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdServicesBugzilla - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getV4ProjectsIdServicesBugzilla(projectId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdServicesBugzilla', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putV4ProjectsIdServicesBugzilla - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdServicesBugzilla('fakedata', {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdServicesBugzilla', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdServicesBugzilla - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdServicesBugzilla('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdServicesBugzilla', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdServicesBuildkite - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getV4ProjectsIdServicesBuildkite(projectId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdServicesBuildkite', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putV4ProjectsIdServicesBuildkite - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdServicesBuildkite('fakedata', {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdServicesBuildkite', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdServicesBuildkite - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdServicesBuildkite('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdServicesBuildkite', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4ProjectsIdServicesBuildsEmail - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdServicesBuildsEmail(12, {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdServicesBuildsEmail', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdServicesCampfire - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getV4ProjectsIdServicesCampfire(projectId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdServicesCampfire', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putV4ProjectsIdServicesCampfire - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdServicesCampfire('fakedata', {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdServicesCampfire', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdServicesCampfire - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdServicesCampfire('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdServicesCampfire', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdServicesCustomIssueTracker - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getV4ProjectsIdServicesCustomIssueTracker(projectId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdServicesCustomIssueTracker', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putV4ProjectsIdServicesCustomIssueTracker - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdServicesCustomIssueTracker('fakedata', {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdServicesCustomIssueTracker', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsServicesCustomIssueTracker - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsServicesCustomIssueTracker('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsServicesCustomIssueTracker', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdServicesDroneCi - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getV4ProjectsIdServicesDroneCi(projectId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdServicesDroneCi', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putV4ProjectsIdServicesDroneCi - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdServicesDroneCi('fakedata', {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdServicesDroneCi', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdServicesDroneCi - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdServicesDroneCi('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdServicesDroneCi', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdServicesEmailOnPush - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getV4ProjectsIdServicesEmailOnPush(projectId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdServicesEmailOnPush', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putV4ProjectsIdServicesEmailsOnPush - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdServicesEmailsOnPush('fakedata', {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdServicesEmailsOnPush', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdServicesEmailOnPush - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdServicesEmailOnPush('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdServicesEmailOnPush', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdServicesExternalWiki - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getV4ProjectsIdServicesExternalWiki(projectId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdServicesExternalWiki', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putV4ProjectsIdServicesExternalWiki - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdServicesExternalWiki('fakedata', {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdServicesExternalWiki', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdServicesExternalWiki - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdServicesExternalWiki('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdServicesExternalWiki', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdServicesFlowdock - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getV4ProjectsIdServicesFlowdock(projectId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdServicesFlowdock', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putV4ProjectsIdServicesFlowdock - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdServicesFlowdock('fakedata', {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdServicesFlowdock', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsServicesFlowdock - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsServicesFlowdock('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsServicesFlowdock', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdServicesHipchat - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getV4ProjectsIdServicesHipchat(projectId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdServicesHipchat', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putV4ProjectsIdServicesHipchat - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdServicesHipchat('fakedata', {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdServicesHipchat', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdServicesHipchat - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdServicesHipchat('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdServicesHipchat', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdServicesIrker - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getV4ProjectsIdServicesIrker(projectId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdServicesIrker', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putV4ProjectsIdServicesIrker - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdServicesIrker('fakedata', {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdServicesIrker', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdServicesIrker - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdServicesIrker('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdServicesIrker', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdServicesJira - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getV4ProjectsIdServicesJira(projectId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdServicesJira', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putV4ProjectsIdServicesJira - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdServicesJira('fakedata', {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdServicesJira', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdServicesJira - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdServicesJira('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdServicesJira', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdServicesKubernetes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getV4ProjectsIdServicesKubernetes(projectId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdServicesKubernetes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putV4ProjectsIdServicesKubernetes - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdServicesKubernetes('fakedata', {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdServicesKubernetes', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdServicesKubernetes - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdServicesKubernetes('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdServicesKubernetes', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdServicesMattermost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getV4ProjectsIdServicesMattermost(projectId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdServicesMattermost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putV4ProjectsIdServicesMattermost - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdServicesMattermost('fakedata', {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdServicesMattermost', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdServicesMattermost - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdServicesMattermost('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdServicesMattermost', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdServicesMattermostSlashCommands - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getV4ProjectsIdServicesMattermostSlashCommands(projectId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdServicesMattermostSlashCommands', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putV4ProjectsIdServicesMattermostSlashCommands - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdServicesMattermostSlashCommands('fakedata', {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdServicesMattermostSlashCommands', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdServicesMattermostSlashCommands - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdServicesMattermostSlashCommands('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdServicesMattermostSlashCommands', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdServicesPipelinesEmail - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getV4ProjectsIdServicesPipelinesEmail(projectId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdServicesPipelinesEmail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putV4ProjectsIdServicesPipelinesEmail - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdServicesPipelinesEmail('fakedata', {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdServicesPipelinesEmail', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdServicesPipelinesEmail - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdServicesPipelinesEmail('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdServicesPipelinesEmail', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdServicesPivotaltracker - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getV4ProjectsIdServicesPivotaltracker(projectId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdServicesPivotaltracker', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putV4ProjectsIdServicesPivotaltracker - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdServicesPivotaltracker('fakedata', {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdServicesPivotaltracker', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdServicesPivotaltracker - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdServicesPivotaltracker('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdServicesPivotaltracker', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdServicesPushover - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getV4ProjectsIdServicesPushover(projectId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdServicesPushover', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putV4ProjectsIdServicesPushover - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdServicesPushover('fakedata', {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdServicesPushover', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdServicesPushover - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdServicesPushover('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdServicesPushover', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdServicesRedmine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getV4ProjectsIdServicesRedmine(projectId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdServicesRedmine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putV4ProjectsIdServicesRedmine - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdServicesRedmine('fakedata', {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdServicesRedmine', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdServicesRedmine - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdServicesRedmine('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdServicesRedmine', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdServicesSlack - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getV4ProjectsIdServicesSlack(projectId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdServicesSlack', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putV4ProjectsIdServicesSlack - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdServicesSlack('fakedata', {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdServicesSlack', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdServicesSlack - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdServicesSlack('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdServicesSlack', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdServicesSlackSlashCommands - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdServicesSlackSlashCommands(projectId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(9, data.response.id);
                assert.equal('string', data.response.title);
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.updated_at);
                assert.equal(true, data.response.active);
                assert.equal(true, data.response.push_events);
                assert.equal(true, data.response.issues_events);
                assert.equal(true, data.response.confidential_issues_events);
                assert.equal(true, data.response.merge_requests_events);
                assert.equal(true, data.response.tag_push_events);
                assert.equal(false, data.response.note_events);
                assert.equal(false, data.response.job_events);
                assert.equal(false, data.response.pipeline_events);
                assert.equal('object', typeof data.response.properties);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdServicesSlackSlashCommands', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putV4ProjectsIdServicesSlackSlashCommands - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdServicesSlackSlashCommands('fakedata', {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdServicesSlackSlashCommands', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdServicesSlackSlashCommands - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdServicesSlackSlashCommands('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdServicesSlackSlashCommands', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdServicesTeamcity - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getV4ProjectsIdServicesTeamcity(projectId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdServicesTeamcity', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putV4ProjectsIdServicesTeamcity - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdServicesTeamcity('fakedata', {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdServicesTeamcity', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdServicesTeamcity - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdServicesTeamcity('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdServicesTeamcity', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdShare - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdShare('fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.expires_at);
    //           assert.equal('string', data.response.group_access);
    //           assert.equal('string', data.response.groupId);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.project_id);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdShare', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdShareGroupId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdShareGroupId('fakedata', 12, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdShareGroupId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdSnippets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdSnippets(projectId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(4, data.response.id);
                assert.equal('string', data.response.title);
                assert.equal('string', data.response.file_name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.author);
                assert.equal('string', data.response.updated_at);
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.web_url);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdSnippets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#postV4ProjectsIdSnippets - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdSnippets('fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(9, data.response.id);
    //           assert.equal('string', data.response.title);
    //           assert.equal('string', data.response.file_name);
    //           assert.equal('string', data.response.description);
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.web_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdSnippets', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdSnippetsSnippetId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdSnippetsSnippetId('fakedata', 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(4, data.response.id);
    //           assert.equal('string', data.response.title);
    //           assert.equal('string', data.response.file_name);
    //           assert.equal('string', data.response.description);
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.web_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdSnippetsSnippetId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4ProjectsIdSnippetsSnippetId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdSnippetsSnippetId('fakedata', 12, {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(5, data.response.id);
    //           assert.equal('string', data.response.title);
    //           assert.equal('string', data.response.file_name);
    //           assert.equal('string', data.response.description);
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.web_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdSnippetsSnippetId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdSnippetsSnippetId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdSnippetsSnippetId('fakedata', 12, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdSnippetsSnippetId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdSnippetsSnippetIdAwardEmoji - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdSnippetsSnippetIdAwardEmoji('fakedata', 12, 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.awardable_id);
    //           assert.equal('string', data.response.awardable_type);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('object', typeof data.response.user);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdSnippetsSnippetIdAwardEmoji', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdSnippetsSnippetIdAwardEmoji - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdSnippetsSnippetIdAwardEmoji(12, 12, {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.awardable_id);
    //           assert.equal('string', data.response.awardable_type);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('object', typeof data.response.user);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdSnippetsSnippetIdAwardEmoji', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdSnippetsSnippetIdAwardEmojiAwardId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdSnippetsSnippetIdAwardEmojiAwardId(12, 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.awardable_id);
    //           assert.equal('string', data.response.awardable_type);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('object', typeof data.response.user);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdSnippetsSnippetIdAwardEmojiAwardId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdSnippetsSnippetIdAwardEmojiAwardId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdSnippetsSnippetIdAwardEmojiAwardId(12, 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.awardable_id);
    //           assert.equal('string', data.response.awardable_type);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('object', typeof data.response.user);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdSnippetsSnippetIdAwardEmojiAwardId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmoji - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmoji('fakedata', 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.awardable_id);
    //           assert.equal('string', data.response.awardable_type);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('object', typeof data.response.user);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmoji', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmoji - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmoji(12, 12, 12, {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.awardable_id);
    //           assert.equal('string', data.response.awardable_type);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('object', typeof data.response.user);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmoji', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmojiAwardId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmojiAwardId(12, 'fakedata', 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.awardable_id);
    //           assert.equal('string', data.response.awardable_type);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('object', typeof data.response.user);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmojiAwardId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmojiAwardId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmojiAwardId(12, 12, 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.awardable_id);
    //           assert.equal('string', data.response.awardable_type);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('object', typeof data.response.user);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmojiAwardId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdSnippetsSnippetIdRaw - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdSnippetsSnippetIdRaw('fakedata', 12, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdSnippetsSnippetIdRaw', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdStatusesSha - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdStatusesSha('fakedata', 'fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.allow_failure);
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.finished_at);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.ref);
    //           assert.equal('string', data.response.sha);
    //           assert.equal('string', data.response.started_at);
    //           assert.equal('string', data.response.status);
    //           assert.equal('string', data.response.target_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdStatusesSha', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdTriggers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdTriggers(projectId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.last_used);
                assert.equal('string', data.response.token);
                assert.equal('string', data.response.updated_at);
                assert.equal(7, data.response.id);
                assert.equal('string', data.response.owner);
                assert.equal('string', data.response.description);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdTriggers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#postV4ProjectsIdTriggers - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdTriggers('fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.last_used);
    //           assert.equal('string', data.response.token);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal(8, data.response.id);
    //           assert.equal('string', data.response.owner);
    //           assert.equal('string', data.response.description);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdTriggers', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdUploads - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdUploads('fakedata', 'fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdUploads', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdUsers(projectId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.avatar_url);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.state);
                assert.equal('string', data.response.username);
                assert.equal('string', data.response.web_url);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdVariables - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdVariables(projectId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.key);
                assert.equal('string', data.response.value);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdVariables', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#postV4ProjectsIdVariables - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdVariables('fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.key);
    //           assert.equal('string', data.response.value);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdVariables', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdVariablesKey - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdVariablesKey('fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.key);
    //           assert.equal('string', data.response.value);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdVariablesKey', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4ProjectsIdVariablesKey - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdVariablesKey('fakedata', 'fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.key);
    //           assert.equal('string', data.response.value);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdVariablesKey', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdVariablesKey - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdVariablesKey('fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.key);
    //           assert.equal('string', data.response.value);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdVariablesKey', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdLanguages - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdLanguages(projectId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(5, data.response.Ruby);
                assert.equal(7, data.response.JavaScript);
                assert.equal(8, data.response.HTML);
                assert.equal(6, data.response.CoffeeScript);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdLanguages', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#getV4ProjectsIdPushRule - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdPushRule('fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(2, data.response.id);
    //           assert.equal(4, data.response.project_id);
    //           assert.equal('string', data.response.commit_message_regex);
    //           assert.equal('string', data.response.branch_name_regex);
    //           assert.equal(true, data.response.deny_delete_tag);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal(true, data.response.member_check);
    //           assert.equal(false, data.response.prevent_secrets);
    //           assert.equal('string', data.response.author_email_regex);
    //           assert.equal('string', data.response.file_name_regex);
    //           assert.equal(4, data.response.max_file_size);
    //           assert.equal(true, data.response.commit_committer_check);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdPushRule', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4ProjectsIdPushRule - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdPushRule('fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(4, data.response.id);
    //           assert.equal(10, data.response.project_id);
    //           assert.equal('string', data.response.commit_message_regex);
    //           assert.equal('string', data.response.branch_name_regex);
    //           assert.equal(false, data.response.deny_delete_tag);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal(false, data.response.member_check);
    //           assert.equal(false, data.response.prevent_secrets);
    //           assert.equal('string', data.response.author_email_regex);
    //           assert.equal('string', data.response.file_name_regex);
    //           assert.equal(10, data.response.max_file_size);
    //           assert.equal(true, data.response.commit_committer_check);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdPushRule', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4PorjectsIdPushRule - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4PorjectsIdPushRule('fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(7, data.response.id);
    //           assert.equal(7, data.response.project_id);
    //           assert.equal('string', data.response.commit_message_regex);
    //           assert.equal('string', data.response.branch_name_regex);
    //           assert.equal(false, data.response.deny_delete_tag);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal(false, data.response.member_check);
    //           assert.equal(false, data.response.prevent_secrets);
    //           assert.equal('string', data.response.author_email_regex);
    //           assert.equal('string', data.response.file_name_regex);
    //           assert.equal(9, data.response.max_file_size);
    //           assert.equal(true, data.response.commit_committer_check);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4PorjectsIdPushRule', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#delelteV4ProjectsIdPushRule - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.delelteV4ProjectsIdPushRule('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'delelteV4ProjectsIdPushRule', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdExport - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdExport(projectId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(3, data.response.id);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.name_with_namespace);
                assert.equal('string', data.response.pathParam);
                assert.equal('string', data.response.path_with_namespace);
                assert.equal('string', data.response.createdAt);
                assert.equal('string', data.response.export_status);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdExport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#postV4ProjectsIdExport - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdExport('fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.message);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdExport', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdExportDownload - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdExportDownload('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdExportDownload', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdImport - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdImport('fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(5, data.response.id);
    //           assert.equal('object', typeof data.response.description);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.name_with_namespace);
    //           assert.equal('string', data.response.pathParam);
    //           assert.equal('string', data.response.path_with_namespace);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.import_status);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdImport', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdImport - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdImport('fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(10, data.response.id);
    //           assert.equal('object', typeof data.response.description);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.name_with_namespace);
    //           assert.equal('string', data.response.pathParam);
    //           assert.equal('string', data.response.path_with_namespace);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.import_status);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdImport', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdBadges - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdBadges(projectId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response['0']);
                assert.equal('object', typeof data.response['1']);
                assert.equal('object', typeof data.response['2']);
                assert.equal('object', typeof data.response['3']);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdBadges', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#postV4ProjectsIdBadges - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdBadges('fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(6, data.response.id);
    //           assert.equal('string', data.response.linkUrl);
    //           assert.equal('string', data.response.imageUrl);
    //           assert.equal('string', data.response.rendered_link_url);
    //           assert.equal('string', data.response.rendered_image_url);
    //           assert.equal('string', data.response.kind);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdBadges', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdBadgesBadgeId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdBadgesBadgeId('fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(7, data.response.id);
    //           assert.equal('string', data.response.linkUrl);
    //           assert.equal('string', data.response.imageUrl);
    //           assert.equal('string', data.response.rendered_link_url);
    //           assert.equal('string', data.response.rendered_image_url);
    //           assert.equal('string', data.response.kind);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdBadgesBadgeId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdBadgesBadgeId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdBadgesBadgeId('fakedata', 'fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(4, data.response.id);
    //           assert.equal('string', data.response.linkUrl);
    //           assert.equal('string', data.response.imageUrl);
    //           assert.equal('string', data.response.rendered_link_url);
    //           assert.equal('string', data.response.rendered_image_url);
    //           assert.equal('string', data.response.kind);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdBadgesBadgeId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdBadgesBadgeId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdBadgesBadgeId('fakedata', 'fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdBadgesBadgeId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdBadgesRender - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdBadgesRender('fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.linkUrl);
    //           assert.equal('string', data.response.imageUrl);
    //           assert.equal('string', data.response.rendered_link_url);
    //           assert.equal('string', data.response.rendered_image_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdBadgesRender', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdRegistryRepositories - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdRegistryRepositories(projectId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response['0']);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdRegistryRepositories', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#deleteV4ProjectsIdRegistryRepositoriesRepositoryId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdRegistryRepositoriesRepositoryId('fakedata', 'fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdRegistryRepositoriesRepositoryId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdRegistryRepositoriesRepositoryIdTags - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdRegistryRepositoriesRepositoryIdTags('fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response['0']);
    //           assert.equal('object', typeof data.response['1']);
    //           assert.equal('object', typeof data.response['2']);
    //           assert.equal('object', typeof data.response['3']);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdRegistryRepositoriesRepositoryIdTags', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdRegistryRepositoriesRepositoryIdTags - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdRegistryRepositoriesRepositoryIdTags('fakedata', 'fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdRegistryRepositoriesRepositoryIdTags', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdRegistryRepositoriesRepositoryIdTagsTagName - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdRegistryRepositoriesRepositoryIdTagsTagName('fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.pathParam);
    //           assert.equal('string', data.response.location);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdRegistryRepositoriesRepositoryIdTagsTagName', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdRegistryRepositoriesRepositoryIdTagsTagName - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdRegistryRepositoriesRepositoryIdTagsTagName('fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdRegistryRepositoriesRepositoryIdTagsTagName', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdCustomAttributes - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdCustomAttributes('fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response['0']);
    //           assert.equal('object', typeof data.response['1']);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdCustomAttributes', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdCustomAttributesKey - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdCustomAttributesKey('fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.key);
    //           assert.equal('string', data.response.value);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdCustomAttributesKey', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4ProjectsIdCustomAttributesKey - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdCustomAttributesKey('fakedata', 'fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.key);
    //           assert.equal('string', data.response.value);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdCustomAttributesKey', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdCustomAttributesKey - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdCustomAttributesKey('fakedata', 'fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdCustomAttributesKey', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdJobs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdJobs(projectId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response['0']);
                assert.equal('object', typeof data.response['1']);
                assert.equal('object', typeof data.response['2']);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdJobs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#getV4ProjectsIdPipelinesPipelineIdJobs - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdPipelinesPipelineIdJobs('fakedata', 12, 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response.commit);
    //           assert.equal('object', typeof data.response.coverage);
    //           assert.equal(false, data.response.allow_failure);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.started_at);
    //           assert.equal('string', data.response.finished_at);
    //           assert.equal(8, data.response.duration);
    //           assert.equal('string', data.response.artifacts_expire_at);
    //           assert.equal(8, data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('object', typeof data.response.pipeline);
    //           assert.equal('string', data.response.ref);
    //           assert.equal(true, Array.isArray(data.response.artifacts));
    //           assert.equal('object', typeof data.response.runner);
    //           assert.equal('string', data.response.stage);
    //           assert.equal('string', data.response.status);
    //           assert.equal(false, data.response.tag);
    //           assert.equal('string', data.response.web_url);
    //           assert.equal('object', typeof data.response.user);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdPipelinesPipelineIdJobs', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdJobsJobId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdJobsJobId('fakedata', 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response.commit);
    //           assert.equal('object', typeof data.response.coverage);
    //           assert.equal(false, data.response.allow_failure);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.started_at);
    //           assert.equal('string', data.response.finished_at);
    //           assert.equal(8, data.response.duration);
    //           assert.equal('string', data.response.artifacts_expire_at);
    //           assert.equal(8, data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('object', typeof data.response.pipeline);
    //           assert.equal('string', data.response.ref);
    //           assert.equal(true, Array.isArray(data.response.artifacts));
    //           assert.equal('object', typeof data.response.runner);
    //           assert.equal('string', data.response.stage);
    //           assert.equal('string', data.response.status);
    //           assert.equal(true, data.response.tag);
    //           assert.equal('string', data.response.web_url);
    //           assert.equal('object', typeof data.response.user);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdJobsJobId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdJobsJobIdTrace - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdJobsJobIdTrace('fakedata', 12, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdJobsJobIdTrace', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdJobsJobIdRetry - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdJobsJobIdRetry('fakedata', 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response.commit);
    //           assert.equal('object', typeof data.response.coverage);
    //           assert.equal(false, data.response.allow_failure);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.started_at);
    //           assert.equal('object', typeof data.response.finished_at);
    //           assert.equal(7, data.response.duration);
    //           assert.equal(6, data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.ref);
    //           assert.equal(true, Array.isArray(data.response.artifacts));
    //           assert.equal('object', typeof data.response.runner);
    //           assert.equal('string', data.response.stage);
    //           assert.equal('string', data.response.status);
    //           assert.equal(false, data.response.tag);
    //           assert.equal('string', data.response.web_url);
    //           assert.equal('object', typeof data.response.user);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdJobsJobIdRetry', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdJobsJobIdErase - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdJobsJobIdErase('fakedata', 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response.commit);
    //           assert.equal('object', typeof data.response.coverage);
    //           assert.equal(true, data.response.allow_failure);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.started_at);
    //           assert.equal('object', typeof data.response.finished_at);
    //           assert.equal(7, data.response.duration);
    //           assert.equal(1, data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.ref);
    //           assert.equal(true, Array.isArray(data.response.artifacts));
    //           assert.equal('object', typeof data.response.runner);
    //           assert.equal('string', data.response.stage);
    //           assert.equal('string', data.response.status);
    //           assert.equal(false, data.response.tag);
    //           assert.equal('string', data.response.web_url);
    //           assert.equal('object', typeof data.response.user);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdJobsJobIdErase', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdJobsJobIdCancel - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdJobsJobIdCancel('fakedata', 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response.commit);
    //           assert.equal('object', typeof data.response.coverage);
    //           assert.equal(true, data.response.allow_failure);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.started_at);
    //           assert.equal('object', typeof data.response.finished_at);
    //           assert.equal(8, data.response.duration);
    //           assert.equal(1, data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.ref);
    //           assert.equal(true, Array.isArray(data.response.artifacts));
    //           assert.equal('object', typeof data.response.runner);
    //           assert.equal('string', data.response.stage);
    //           assert.equal('string', data.response.status);
    //           assert.equal(true, data.response.tag);
    //           assert.equal('string', data.response.web_url);
    //           assert.equal('object', typeof data.response.user);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdJobsJobIdCancel', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdJobsJobIdPlay - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdJobsJobIdPlay('fakedata', 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response.commit);
    //           assert.equal('object', typeof data.response.coverage);
    //           assert.equal(true, data.response.allow_failure);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.started_at);
    //           assert.equal('object', typeof data.response.finished_at);
    //           assert.equal(9, data.response.duration);
    //           assert.equal(4, data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.ref);
    //           assert.equal(true, Array.isArray(data.response.artifacts));
    //           assert.equal('object', typeof data.response.runner);
    //           assert.equal('string', data.response.stage);
    //           assert.equal('string', data.response.status);
    //           assert.equal(false, data.response.tag);
    //           assert.equal('string', data.response.web_url);
    //           assert.equal('object', typeof data.response.user);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdJobsJobIdPlay', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdPagesDomains - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdPagesDomains('fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.domain);
    //           assert.equal('string', data.response.url);
    //           assert.equal('object', typeof data.response.certificate);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdPagesDomains', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdPagesDomains - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdPagesDomains('fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.domain);
    //           assert.equal('string', data.response.url);
    //           assert.equal('object', typeof data.response.certificate);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdPagesDomains', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdPagesDomainsDomain - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdPagesDomainsDomain('fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.domain);
    //           assert.equal('string', data.response.url);
    //           assert.equal('object', typeof data.response.certificate);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdPagesDomainsDomain', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4ProjectsIdPagesDomainsDomain - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdPagesDomainsDomain('fakedata', 'fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.domain);
    //           assert.equal('string', data.response.url);
    //           assert.equal('object', typeof data.response.certificate);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdPagesDomainsDomain', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdPagesDomainsDomain - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdPagesDomainsDomain('fakedata', 'fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdPagesDomainsDomain', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdPipelineSchedules - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdPipelineSchedules('fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(10, data.response.id);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.ref);
    //           assert.equal('string', data.response.cron);
    //           assert.equal('string', data.response.cronTimezone);
    //           assert.equal('string', data.response.next_run_at);
    //           assert.equal(true, data.response.active);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('object', typeof data.response.owner);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdPipelineSchedules', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsPipelineSchedules - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsPipelineSchedules('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(5, data.response.id);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.ref);
    //           assert.equal('string', data.response.cron);
    //           assert.equal('string', data.response.cronTimezone);
    //           assert.equal('string', data.response.next_run_at);
    //           assert.equal(true, data.response.active);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('object', typeof data.response.owner);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsPipelineSchedules', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdPipelineSchedulesPipelineScheduleId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdPipelineSchedulesPipelineScheduleId('fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(5, data.response.id);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.ref);
    //           assert.equal('string', data.response.cron);
    //           assert.equal('string', data.response.cronTimezone);
    //           assert.equal('string', data.response.next_run_at);
    //           assert.equal(true, data.response.active);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('object', typeof data.response.owner);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdPipelineSchedulesPipelineScheduleId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4ProjectsPipelineSchedulesPipelineScheduleId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsPipelineSchedulesPipelineScheduleId('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(7, data.response.id);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.ref);
    //           assert.equal('string', data.response.cron);
    //           assert.equal('string', data.response.cronTimezone);
    //           assert.equal('string', data.response.next_run_at);
    //           assert.equal(true, data.response.active);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('object', typeof data.response.owner);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsPipelineSchedulesPipelineScheduleId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdPipelineSchedulesPipelineScheduleId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdPipelineSchedulesPipelineScheduleId('fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(1, data.response.id);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.ref);
    //           assert.equal('string', data.response.cron);
    //           assert.equal('string', data.response.cronTimezone);
    //           assert.equal('string', data.response.next_run_at);
    //           assert.equal(true, data.response.active);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('object', typeof data.response.owner);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdPipelineSchedulesPipelineScheduleId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdPipelineSchedulesPipelineScheduleIdTakeOwnership - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdPipelineSchedulesPipelineScheduleIdTakeOwnership('fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(4, data.response.id);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.ref);
    //           assert.equal('string', data.response.cron);
    //           assert.equal('string', data.response.cronTimezone);
    //           assert.equal('string', data.response.next_run_at);
    //           assert.equal(false, data.response.active);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('object', typeof data.response.owner);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdPipelineSchedulesPipelineScheduleIdTakeOwnership', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariables - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariables('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.key);
    //           assert.equal('string', data.response.value);
    //           assert.equal('string', data.response.variableType);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariables', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariablesKey - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariablesKey('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.key);
    //           assert.equal('string', data.response.value);
    //           assert.equal('string', data.response.variableType);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariablesKey', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariablesKey - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariablesKey('fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariablesKey', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdClusters - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdClusters(12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response['0']);
    //           assert.equal('object', typeof data.response['1']);
    //           assert.equal('object', typeof data.response['2']);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdClusters', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdClustersClusterId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdClustersClusterId(12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(6, data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.domain);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.provider_type);
    //           assert.equal('string', data.response.platform_type);
    //           assert.equal('string', data.response.environment_scope);
    //           assert.equal('string', data.response.cluster_type);
    //           assert.equal('object', typeof data.response.user);
    //           assert.equal('object', typeof data.response.platform_kubernetes);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdClustersClusterId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4ProjectsIdClustesClusterId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdClustesClusterId(12, 12, {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(9, data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.domain);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.provider_type);
    //           assert.equal('string', data.response.platform_type);
    //           assert.equal('string', data.response.environment_scope);
    //           assert.equal('string', data.response.cluster_type);
    //           assert.equal('object', typeof data.response.user);
    //           assert.equal('object', typeof data.response.platform_kubernetes);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdClustesClusterId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdClustersClusterId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdClustersClusterId(12, 12, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdClustersClusterId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdClustersUser - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdClustersUser(12, {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(8, data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.domain);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.provider_type);
    //           assert.equal('string', data.response.platform_type);
    //           assert.equal('string', data.response.environment_scope);
    //           assert.equal('string', data.response.cluster_type);
    //           assert.equal('object', typeof data.response.user);
    //           assert.equal('object', typeof data.response.platform_kubernetes);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdClustersUser', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdProtectedBranches - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdProtectedBranches('fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response['0']);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdProtectedBranches', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProtectsIdProtectedBranches - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProtectsIdProtectedBranches('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.name);
    //           assert.equal(true, Array.isArray(data.response.push_access_levels));
    //           assert.equal(true, Array.isArray(data.response.merge_access_levels));
    //           assert.equal(true, Array.isArray(data.response.unprotect_access_levels));
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProtectsIdProtectedBranches', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdProtectedBranchesName - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdProtectedBranchesName('fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.name);
    //           assert.equal(true, Array.isArray(data.response.push_access_levels));
    //           assert.equal(true, Array.isArray(data.response.merge_access_levels));
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdProtectedBranchesName', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdProtectedBranchesName - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdProtectedBranchesName('fakedata', 'fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdProtectedBranchesName', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdProtectedTags - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdProtectedTags('fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response['0']);
    //           assert.equal('object', typeof data.response['1']);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdProtectedTags', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdProtectedTags - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdProtectedTags('fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.name);
    //           assert.equal(true, Array.isArray(data.response.create_access_levels));
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdProtectedTags', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdProtectedTagsName - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdProtectedTagsName('fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.name);
    //           assert.equal(true, Array.isArray(data.response.create_access_levels));
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdProtectedTagsName', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdProtectedTagsName - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdProtectedTagsName('fakedata', 'fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdProtectedTagsName', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdReleases - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdReleases('fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response['0']);
    //           assert.equal('object', typeof data.response['1']);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdReleases', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdReleases - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdReleases('fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.tagName);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.description_html);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('object', typeof data.response.commit);
    //           assert.equal('object', typeof data.response.assets);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdReleases', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdReleases - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdReleases('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdReleases', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdReleasesTagName - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdReleasesTagName('fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.tagName);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.description_html);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('object', typeof data.response.commit);
    //           assert.equal('object', typeof data.response.assets);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdReleasesTagName', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4ProjectsIdReleasesTagName - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdReleasesTagName('fakedata', 'fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.tagName);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.description_html);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('object', typeof data.response.commit);
    //           assert.equal('object', typeof data.response.assets);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdReleasesTagName', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4ProjectsIdRepositorySubmodulesSubmodule - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdRepositorySubmodulesSubmodule('fakedata', 'fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.short_id);
    //           assert.equal('string', data.response.title);
    //           assert.equal('string', data.response.author_name);
    //           assert.equal('string', data.response.author_email);
    //           assert.equal('string', data.response.committer_name);
    //           assert.equal('string', data.response.committer_email);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.message);
    //           assert.equal(true, Array.isArray(data.response.parent_ids));
    //           assert.equal('string', data.response.committed_date);
    //           assert.equal('string', data.response.authored_date);
    //           assert.equal('object', typeof data.response.status);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdRepositorySubmodulesSubmodule', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdIssuesIssueiidResourceLabelEvents - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdIssuesIssueiidResourceLabelEvents('fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response['0']);
    //           assert.equal('object', typeof data.response['1']);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdIssuesIssueiidResourceLabelEvents', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdIssuesIssueiidResourceLabelEventsId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdIssuesIssueiidResourceLabelEventsId('fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(2, data.response.id);
    //           assert.equal('object', typeof data.response.user);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.resource_type);
    //           assert.equal(1, data.response.resource_id);
    //           assert.equal('object', typeof data.response.label);
    //           assert.equal('string', data.response.action);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdIssuesIssueiidResourceLabelEventsId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdMergeRequestiidResourceLabelEvents - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdMergeRequestiidResourceLabelEvents('fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response['0']);
    //           assert.equal('object', typeof data.response['1']);
    //           assert.equal('object', typeof data.response['2']);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdMergeRequestiidResourceLabelEvents', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdSearch - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdSearch('fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response['0']);
    //           assert.equal('object', typeof data.response['1']);
    //           assert.equal('object', typeof data.response['2']);
    //           assert.equal('object', typeof data.response['3']);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdSearch', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdWikis - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdWikis(true, 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response['0']);
    //           assert.equal('object', typeof data.response['1']);
    //           assert.equal('object', typeof data.response['2']);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdWikis', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdWikis - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdWikis(true, 'fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.content);
    //           assert.equal('string', data.response.format);
    //           assert.equal('string', data.response.slug);
    //           assert.equal('string', data.response.title);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdWikis', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdWikisSlug - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdWikisSlug('fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.content);
    //           assert.equal('string', data.response.format);
    //           assert.equal('string', data.response.slug);
    //           assert.equal('string', data.response.title);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdWikisSlug', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4ProjectsIdWikisSlug - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdWikisSlug('fakedata', 'fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.content);
    //           assert.equal('string', data.response.format);
    //           assert.equal('string', data.response.slug);
    //           assert.equal('string', data.response.title);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdWikisSlug', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdWikis - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdWikis('fakedata', 'fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdWikis', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdWikisAttachments - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdWikisAttachments(true, 'fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.file_name);
    //           assert.equal('string', data.response.filePath);
    //           assert.equal('string', data.response.branch);
    //           assert.equal('object', typeof data.response.link);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdWikisAttachments', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdRepositoryFiles - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdRepositoryFiles('fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.file_name);
    //           assert.equal('string', data.response.filePath);
    //           assert.equal(2, data.response.size);
    //           assert.equal('string', data.response.encoding);
    //           assert.equal('string', data.response.content);
    //           assert.equal('string', data.response.content_sha256);
    //           assert.equal('string', data.response.ref);
    //           assert.equal('string', data.response.blob_id);
    //           assert.equal('string', data.response.commitId);
    //           assert.equal('string', data.response.last_commit_id);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdRepositoryFiles', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4ProjectsIdRepositoryFiles - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdRepositoryFiles('fakedata', 'fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.branch);
    //           assert.equal('string', data.response.commit_message);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdRepositoryFiles', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdRepositoryFiles - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdRepositoryFiles('fakedata', 'fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.filePath);
    //           assert.equal('string', data.response.branch);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdRepositoryFiles', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdRepositoryFiles - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdRepositoryFiles('fakedata', 'fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdRepositoryFiles', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsRepositoryFilesRaw - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsRepositoryFilesRaw('fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.file_name);
    //           assert.equal('string', data.response.filePath);
    //           assert.equal(2, data.response.size);
    //           assert.equal('string', data.response.encoding);
    //           assert.equal('string', data.response.content);
    //           assert.equal('string', data.response.content_sha256);
    //           assert.equal('string', data.response.ref);
    //           assert.equal('string', data.response.blob_id);
    //           assert.equal('string', data.response.commitId);
    //           assert.equal('string', data.response.last_commit_id);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsRepositoryFilesRaw', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdJobssJobIdArtifacts - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdJobssJobIdArtifacts('fakedata', 12, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdJobssJobIdArtifacts', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdJobsJobIdArtifacts - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdJobsJobIdArtifacts('fakedata', 12, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdJobsJobIdArtifacts', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdJobsJobIdArtifactsKeep - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdJobsJobIdArtifactsKeep('fakedata', 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response.commit);
    //           assert.equal('object', typeof data.response.coverage);
    //           assert.equal(true, data.response.allow_failure);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.started_at);
    //           assert.equal('object', typeof data.response.finished_at);
    //           assert.equal(5, data.response.duration);
    //           assert.equal(1, data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.ref);
    //           assert.equal(true, Array.isArray(data.response.artifacts));
    //           assert.equal('object', typeof data.response.runner);
    //           assert.equal('string', data.response.stage);
    //           assert.equal('string', data.response.status);
    //           assert.equal(false, data.response.tag);
    //           assert.equal('string', data.response.web_url);
    //           assert.equal('object', typeof data.response.user);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdJobsJobIdArtifactsKeep', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdJobsArtifactsRefNameDownload - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdJobsArtifactsRefNameDownload('fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdJobsArtifactsRefNameDownload', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdJobsJobIdArtifactsArtifcactPath - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdJobsJobIdArtifactsArtifcactPath('fakedata', 12, 'fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdJobsJobIdArtifactsArtifcactPath', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdJobsArtifactsRefNameRaw - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdJobsArtifactsRefNameRaw('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdJobsArtifactsRefNameRaw', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdRepositoryCommitsShaRefs - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdRepositoryCommitsShaRefs('fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.type);
    //           assert.equal('string', data.response.name);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdRepositoryCommitsShaRefs', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdTriggersTriggerId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdTriggersTriggerId('fakedata', 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.last_used);
    //           assert.equal('string', data.response.token);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal(5, data.response.id);
    //           assert.equal('string', data.response.owner);
    //           assert.equal('string', data.response.description);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdTriggersTriggerId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdTriggersTriggerId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdTriggersTriggerId(12, 'fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.last_used);
    //           assert.equal('string', data.response.token);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal(6, data.response.id);
    //           assert.equal('string', data.response.owner);
    //           assert.equal('string', data.response.description);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdTriggersTriggerId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdTriggersTriggerId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdTriggersTriggerId('fakedata', 12, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdTriggersTriggerId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdTriggersTriggerIdOwnership - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdTriggersTriggerIdOwnership(12, 'fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.last_used);
    //           assert.equal('string', data.response.token);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal(2, data.response.id);
    //           assert.equal('string', data.response.owner);
    //           assert.equal('string', data.response.description);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdTriggersTriggerIdOwnership', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdServicesHangoutschat - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getV4ProjectsIdServicesHangoutschat(projectId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdServicesHangoutschat', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putV4ProjectsIdServicesHangoutschat - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdServicesHangoutschat('fakedata', {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdServicesHangoutschat', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdServicesHangoutschat - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdServicesHangoutschat('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdServicesHangoutschat', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdServicesPackagist - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getV4ProjectsIdServicesPackagist(projectId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdServicesPackagist', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putV4ProjectsIdServicesPackagist - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdServicesPackagist('fakedata', {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdServicesPackagist', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdServicesPackagist - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdServicesPackagist('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdServicesPackagist', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdServicesMicrosoftTeams - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getV4ProjectsIdServicesMicrosoftTeams(projectId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdServicesMicrosoftTeams', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putV4ProjectsIdServicesMicrosoftTeams - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdServicesMicrosoftTeams('fakedata', {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdServicesMicrosoftTeams', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdServicesMicrosoftTeams - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdServicesMicrosoftTeams('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdServicesMicrosoftTeams', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdServicesJenkins - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdServicesJenkins(projectId, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdServicesJenkins', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4ProjectsIdServicesJenkins - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdServicesJenkins('fakedata', {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdServicesJenkins', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdServicesJenkins - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdServicesJenkins('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdServicesJenkins', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdServicesMockCi - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdServicesMockCi(projectId, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdServicesMockCi', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4ProjectsIdServicesMockCi - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdServicesMockCi('fakedata', {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdServicesMockCi', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdServicesMockCi - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdServicesMockCi('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdServicesMockCi', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdServicesYoutrack - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getV4ProjectsIdServicesYoutrack(projectId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdServicesYoutrack', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putV4ProjectsIdServicesYoutrack - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdServicesYoutrack('fakedata', {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdServicesYoutrack', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdServicesYoutrack - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdServicesYoutrack('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdServicesYoutrack', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdSnippetIdDiscussions - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdSnippetIdDiscussions(12, 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response['0']);
    //           assert.equal('object', typeof data.response['1']);
    //           assert.equal('object', typeof data.response['2']);
    //           assert.equal('object', typeof data.response['3']);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdSnippetIdDiscussions', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdSnippetIdDiscussions - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdSnippetIdDiscussions('fakedata', 'fakedata', 12, {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdSnippetIdDiscussions', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdSnippetIdDiscussionId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdSnippetIdDiscussionId('fakedata', 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.id);
    //           assert.equal(true, data.response.individual_note);
    //           assert.equal(true, Array.isArray(data.response.notes));
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdSnippetIdDiscussionId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdSnippetIdDiscussionIdNotes - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdSnippetIdDiscussionIdNotes('fakedata', 'fakedata', 12, 12, {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdSnippetIdDiscussionIdNotes', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4ProjectsIdSnippetIdDiscussionIdNoteId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdSnippetIdDiscussionIdNoteId('fakedata', 12, 12, 12, 'fakedata', {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdSnippetIdDiscussionIdNoteId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdSnippetIdDiscussionIdNoteId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdSnippetIdDiscussionIdNoteId('fakedata', 12, 12, 12, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdSnippetIdDiscussionIdNoteId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    let projectIssueDiscussionId;

    describe('#getV4ProjectsIdIssuesIidDiscussions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdIssuesIidDiscussions(projectId, projectIssueIid, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response['0']);
                assert.equal('object', typeof data.response['1']);
              } else {
                runCommonAsserts(data, error);
              }

              projectIssueDiscussionId = data.response[0].id;
              saveMockData('Projects', 'getV4ProjectsIdIssuesIidDiscussions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#postV4ProjectsIdIssuesIidDiscussions - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdIssuesIidDiscussions('fakedata', 'fakedata', 12, {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdIssuesIidDiscussions', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4ProjectsIdIssuesIidDiscussionsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ProjectsIdIssuesIidDiscussionsId(projectId, projectIssueIid, projectIssueDiscussionId, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.id);
                assert.equal(true, data.response.individual_note);
                assert.equal(true, Array.isArray(data.response.notes));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'getV4ProjectsIdIssuesIidDiscussionsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#postV4ProjectsIdIssuesIidDiscussionsIdNotes - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdIssuesIidDiscussionsIdNotes('fakedata', 'fakedata', 12, 12, {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdIssuesIidDiscussionsIdNotes', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4ProjectsIdIssuesIidDiscussionsIdNotesId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdIssuesIidDiscussionsIdNotesId('fakedata', 'fakedata', 12, 12, 12, {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdIssuesIidDiscussionsIdNotesId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdIssuesIidDiscussionsIdNotesId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdIssuesIidDiscussionsIdNotesId('fakedata', 12, 12, 12, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdIssuesIidDiscussionsIdNotesId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdMergeRequestsIidDiscussions - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdMergeRequestsIidDiscussions('fakedata', 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.id);
    //           assert.equal(true, data.response.individual_note);
    //           assert.equal(true, Array.isArray(data.response.notes));
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdMergeRequestsIidDiscussions', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdMergeRequestsIidDiscussionsId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdMergeRequestsIidDiscussionsId('fakedata', 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.id);
    //           assert.equal(true, data.response.individual_note);
    //           assert.equal(true, Array.isArray(data.response.notes));
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdMergeRequestsIidDiscussionsId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4ProjectsIdMergeRequestsIidDiscussionsId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdMergeRequestsIidDiscussionsId(true, 'fakedata', 12, 12, {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdMergeRequestsIidDiscussionsId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId('fakedata', true, 'fakedata', 12, 12, 12, {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId('fakedata', 12, 12, 12, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdCommitsIdDiscussions - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdCommitsIdDiscussions('fakedata', 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response['0']);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdCommitsIdDiscussions', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdCommitsIdDiscussions - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdCommitsIdDiscussions('fakedata', 'fakedata', 12, {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdCommitsIdDiscussions', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdCommitsIdDiscussionsId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdCommitsIdDiscussionsId('fakedata', 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.id);
    //           assert.equal(false, data.response.individual_note);
    //           assert.equal(true, Array.isArray(data.response.notes));
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdCommitsIdDiscussionsId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdCommitsIdDiscussionsIdNotes - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdCommitsIdDiscussionsIdNotes('fakedata', 'fakedata', 12, 12, {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdCommitsIdDiscussionsIdNotes', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4ProjectsIdCommitsIdDiscussionsIdNotesId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdCommitsIdDiscussionsIdNotesId('fakedata', 12, 12, 12, 'fakedata', {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdCommitsIdDiscussionsIdNotesId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdCommitsIdDiscussionsIdNotesId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdCommitsIdDiscussionsIdNotesId('fakedata', 12, 12, 12, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdCommitsIdDiscussionsIdNotesId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdSnippetsIdNotes - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdSnippetsIdNotes('fakedata', 12, 'fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response['0']);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdSnippetsIdNotes', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdSnippetsIdNotes - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdSnippetsIdNotes('fakedata', 12, 'fakedata', 'fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(10, data.response.id);
    //           assert.equal('string', data.response.title);
    //           assert.equal('string', data.response.file_name);
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('string', data.response.createdAt);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdSnippetsIdNotes', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdSnippetsIdNoteId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdSnippetsIdNoteId('fakedata', 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(8, data.response.id);
    //           assert.equal('string', data.response.title);
    //           assert.equal('string', data.response.file_name);
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('string', data.response.createdAt);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdSnippetsIdNoteId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4ProjectsIdSnippetsIdNoteId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdSnippetsIdNoteId('fakedata', 12, 12, 'fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(2, data.response.id);
    //           assert.equal('string', data.response.title);
    //           assert.equal('string', data.response.file_name);
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('string', data.response.createdAt);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdSnippetsIdNoteId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdSnippetsIdNoteId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdSnippetsIdNoteId('fakedata', 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.attachment);
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.bodyQuery);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.noteable_id);
    //           assert.equal('string', data.response.noteable_type);
    //           assert.equal('string', data.response.system);
    //           assert.equal('string', data.response.updated_at);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdSnippetsIdNoteId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdMergeRequestsIidNotes - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdMergeRequestsIidNotes('fakedata', 12, 'fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.attachment);
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.bodyQuery);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.noteable_id);
    //           assert.equal('string', data.response.noteable_type);
    //           assert.equal('string', data.response.system);
    //           assert.equal('string', data.response.updated_at);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdMergeRequestsIidNotes', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdMergeRequestsIidNotes - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdMergeRequestsIidNotes('fakedata', 12, 'fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.attachment);
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.bodyQuery);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.noteable_id);
    //           assert.equal('string', data.response.noteable_type);
    //           assert.equal('string', data.response.system);
    //           assert.equal('string', data.response.updated_at);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdMergeRequestsIidNotes', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdIssuesIdSubscribe - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdIssuesIdSubscribe('fakedata', 12, {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response.assignee);
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.confidential);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.downvotes);
    //           assert.equal('string', data.response.due_date);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.iid);
    //           assert.equal('string', data.response.labels);
    //           assert.equal('object', typeof data.response.milestone);
    //           assert.equal('string', data.response.project_id);
    //           assert.equal('string', data.response.state);
    //           assert.equal('string', data.response.subscribed);
    //           assert.equal('string', data.response.title);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('string', data.response.upvotes);
    //           assert.equal('string', data.response.user_notes_count);
    //           assert.equal('string', data.response.web_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdIssuesIdSubscribe', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdIssuesIdUnsubscribe - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdIssuesIdUnsubscribe('fakedata', 12, {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response.assignee);
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.confidential);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.downvotes);
    //           assert.equal('string', data.response.due_date);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.iid);
    //           assert.equal('string', data.response.labels);
    //           assert.equal('object', typeof data.response.milestone);
    //           assert.equal('string', data.response.project_id);
    //           assert.equal('string', data.response.state);
    //           assert.equal('string', data.response.subscribed);
    //           assert.equal('string', data.response.title);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('string', data.response.upvotes);
    //           assert.equal('string', data.response.user_notes_count);
    //           assert.equal('string', data.response.web_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdIssuesIdUnsubscribe', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdIssuesIssueIdMove - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdIssuesIssueIdMove('fakedata', 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response.assignee);
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.confidential);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.downvotes);
    //           assert.equal('string', data.response.due_date);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.iid);
    //           assert.equal('string', data.response.labels);
    //           assert.equal('object', typeof data.response.milestone);
    //           assert.equal('string', data.response.project_id);
    //           assert.equal('string', data.response.state);
    //           assert.equal('string', data.response.subscribed);
    //           assert.equal('string', data.response.title);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('string', data.response.upvotes);
    //           assert.equal('string', data.response.user_notes_count);
    //           assert.equal('string', data.response.web_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdIssuesIssueIdMove', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdIssuesIidNotes - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdIssuesIidNotes('fakedata', 12, 'fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response['0']);
    //           assert.equal('object', typeof data.response['1']);
    //           assert.equal('object', typeof data.response['2']);
    //           assert.equal('object', typeof data.response['3']);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdIssuesIidNotes', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdIssuesIidNotes - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdIssuesIidNotes('fakedata', 12, 'fakedata', {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdIssuesIidNotes', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmoji - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmoji(12, 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.awardable_id);
    //           assert.equal('string', data.response.awardable_type);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('object', typeof data.response.user);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmoji', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmoji - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmoji(12, 12, 12, {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.awardable_id);
    //           assert.equal('string', data.response.awardable_type);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('object', typeof data.response.user);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmoji', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmojiAwardId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmojiAwardId(12, 12, 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.awardable_id);
    //           assert.equal('string', data.response.awardable_type);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('object', typeof data.response.user);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmojiAwardId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmojiAwardId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmojiAwardId(12, 12, 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.awardable_id);
    //           assert.equal('string', data.response.awardable_type);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('object', typeof data.response.user);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmojiAwardId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdIssuesIidNotesId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdIssuesIidNotesId('fakedata', 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.attachment);
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.bodyQuery);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.noteable_id);
    //           assert.equal('string', data.response.noteable_type);
    //           assert.equal('string', data.response.system);
    //           assert.equal('string', data.response.updated_at);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdIssuesIidNotesId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4ProjectsIdIssuesIidNotesId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdIssuesIidNotesId('fakedata', 'fakedata', 12, 12, {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdIssuesIidNotesId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdIssuesIidNotesId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdIssuesIidNotesId('fakedata', 12, 12, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdIssuesIidNotesId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdMergeRequestsMergeRequestId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdMergeRequestsMergeRequestId('fakedata', 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response.assignee);
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.downvotes);
    //           assert.equal('string', data.response.force_remove_source_branch);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.iid);
    //           assert.equal('string', data.response.labels);
    //           assert.equal('string', data.response.merge_commit_sha);
    //           assert.equal('string', data.response.merge_status);
    //           assert.equal('string', data.response.merge_when_build_succeeds);
    //           assert.equal('object', typeof data.response.milestone);
    //           assert.equal('string', data.response.project_id);
    //           assert.equal('string', data.response.sha);
    //           assert.equal('string', data.response.should_remove_source_branch);
    //           assert.equal('string', data.response.source_branch);
    //           assert.equal('string', data.response.source_project_id);
    //           assert.equal('string', data.response.state);
    //           assert.equal('string', data.response.subscribed);
    //           assert.equal('string', data.response.target_branch);
    //           assert.equal('string', data.response.target_project_id);
    //           assert.equal('string', data.response.title);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('string', data.response.upvotes);
    //           assert.equal('string', data.response.user_notes_count);
    //           assert.equal('string', data.response.web_url);
    //           assert.equal('string', data.response.work_in_progress);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdMergeRequestsMergeRequestId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4ProjectsIdMergeRequestsMergeRequestId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdMergeRequestsMergeRequestId('fakedata', 12, {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response.assignee);
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.downvotes);
    //           assert.equal('string', data.response.force_remove_source_branch);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.iid);
    //           assert.equal('string', data.response.labels);
    //           assert.equal('string', data.response.merge_commit_sha);
    //           assert.equal('string', data.response.merge_status);
    //           assert.equal('string', data.response.merge_when_build_succeeds);
    //           assert.equal('object', typeof data.response.milestone);
    //           assert.equal('string', data.response.project_id);
    //           assert.equal('string', data.response.sha);
    //           assert.equal('string', data.response.should_remove_source_branch);
    //           assert.equal('string', data.response.source_branch);
    //           assert.equal('string', data.response.source_project_id);
    //           assert.equal('string', data.response.state);
    //           assert.equal('string', data.response.subscribed);
    //           assert.equal('string', data.response.target_branch);
    //           assert.equal('string', data.response.target_project_id);
    //           assert.equal('string', data.response.title);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('string', data.response.upvotes);
    //           assert.equal('string', data.response.user_notes_count);
    //           assert.equal('string', data.response.web_url);
    //           assert.equal('string', data.response.work_in_progress);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdMergeRequestsMergeRequestId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdMergeRequestsMergeRequestId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdMergeRequestsMergeRequestId('fakedata', 12, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdMergeRequestsMergeRequestId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdMergeRequestsMergeRequestIdAddSpentTime - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdMergeRequestsMergeRequestIdAddSpentTime('fakedata', 12, {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdMergeRequestsMergeRequestIdAddSpentTime', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdMergeRequestsMergeRequestIdAwardEmoji - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdMergeRequestsMergeRequestIdAwardEmoji('fakedata', 12, 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.awardable_id);
    //           assert.equal('string', data.response.awardable_type);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('object', typeof data.response.user);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdMergeRequestsMergeRequestIdAwardEmoji', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdMergeRequestsMergeRequestIdAwardEmoji - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdMergeRequestsMergeRequestIdAwardEmoji('fakedata', 12, {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.awardable_id);
    //           assert.equal('string', data.response.awardable_type);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('object', typeof data.response.user);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdMergeRequestsMergeRequestIdAwardEmoji', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdMergeRequestsMergeRequestIdAwardEmojiAwardId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdMergeRequestsMergeRequestIdAwardEmojiAwardId(12, 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.awardable_id);
    //           assert.equal('string', data.response.awardable_type);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('object', typeof data.response.user);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdMergeRequestsMergeRequestIdAwardEmojiAwardId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdMergeRequestsMergeRequestIdAwardEmojiAwardId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdMergeRequestsMergeRequestIdAwardEmojiAwardId(12, 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.awardable_id);
    //           assert.equal('string', data.response.awardable_type);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('object', typeof data.response.user);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdMergeRequestsMergeRequestIdAwardEmojiAwardId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdMergeRequestsMergeRequestIdCancelMergeWhenBuildSucceeds - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdMergeRequestsMergeRequestIdCancelMergeWhenBuildSucceeds('fakedata', 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response.assignee);
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.downvotes);
    //           assert.equal('string', data.response.force_remove_source_branch);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.iid);
    //           assert.equal('string', data.response.labels);
    //           assert.equal('string', data.response.merge_commit_sha);
    //           assert.equal('string', data.response.merge_status);
    //           assert.equal('string', data.response.merge_when_build_succeeds);
    //           assert.equal('object', typeof data.response.milestone);
    //           assert.equal('string', data.response.project_id);
    //           assert.equal('string', data.response.sha);
    //           assert.equal('string', data.response.should_remove_source_branch);
    //           assert.equal('string', data.response.source_branch);
    //           assert.equal('string', data.response.source_project_id);
    //           assert.equal('string', data.response.state);
    //           assert.equal('string', data.response.subscribed);
    //           assert.equal('string', data.response.target_branch);
    //           assert.equal('string', data.response.target_project_id);
    //           assert.equal('string', data.response.title);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('string', data.response.upvotes);
    //           assert.equal('string', data.response.user_notes_count);
    //           assert.equal('string', data.response.web_url);
    //           assert.equal('string', data.response.work_in_progress);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdMergeRequestsMergeRequestIdCancelMergeWhenBuildSucceeds', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdMergeRequestsMergeRequestIdChanges - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdMergeRequestsMergeRequestIdChanges('fakedata', 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response.assignee);
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('object', typeof data.response.changes);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.downvotes);
    //           assert.equal('string', data.response.force_remove_source_branch);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.iid);
    //           assert.equal('string', data.response.labels);
    //           assert.equal('string', data.response.merge_commit_sha);
    //           assert.equal('string', data.response.merge_status);
    //           assert.equal('string', data.response.merge_when_build_succeeds);
    //           assert.equal('object', typeof data.response.milestone);
    //           assert.equal('string', data.response.project_id);
    //           assert.equal('string', data.response.sha);
    //           assert.equal('string', data.response.should_remove_source_branch);
    //           assert.equal('string', data.response.source_branch);
    //           assert.equal('string', data.response.source_project_id);
    //           assert.equal('string', data.response.state);
    //           assert.equal('string', data.response.subscribed);
    //           assert.equal('string', data.response.target_branch);
    //           assert.equal('string', data.response.target_project_id);
    //           assert.equal('string', data.response.title);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('string', data.response.upvotes);
    //           assert.equal('string', data.response.user_notes_count);
    //           assert.equal('string', data.response.web_url);
    //           assert.equal('string', data.response.work_in_progress);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdMergeRequestsMergeRequestIdChanges', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdMergeRequestsMergeRequestIdClosesIssues - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdMergeRequestsMergeRequestIdClosesIssues('fakedata', 12, 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.note);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdMergeRequestsMergeRequestIdClosesIssues', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdMergeRequestsMergeRequestIdComments - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdMergeRequestsMergeRequestIdComments('fakedata', 12, 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.note);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdMergeRequestsMergeRequestIdComments', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdMergeRequestsMergeRequestIdComments - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdMergeRequestsMergeRequestIdComments('fakedata', 12, {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.note);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdMergeRequestsMergeRequestIdComments', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdMergeRequestsMergeRequestIdCommits - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdMergeRequestsMergeRequestIdCommits('fakedata', 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.author_email);
    //           assert.equal('string', data.response.author_name);
    //           assert.equal('string', data.response.committer_email);
    //           assert.equal('string', data.response.committer_name);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.message);
    //           assert.equal('string', data.response.short_id);
    //           assert.equal('string', data.response.title);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdMergeRequestsMergeRequestIdCommits', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4ProjectsIdMergeRequestsMergeRequestIdMerge - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdMergeRequestsMergeRequestIdMerge('fakedata', 12, {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response.assignee);
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.downvotes);
    //           assert.equal('string', data.response.force_remove_source_branch);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.iid);
    //           assert.equal('string', data.response.labels);
    //           assert.equal('string', data.response.merge_commit_sha);
    //           assert.equal('string', data.response.merge_status);
    //           assert.equal('string', data.response.merge_when_build_succeeds);
    //           assert.equal('object', typeof data.response.milestone);
    //           assert.equal('string', data.response.project_id);
    //           assert.equal('string', data.response.sha);
    //           assert.equal('string', data.response.should_remove_source_branch);
    //           assert.equal('string', data.response.source_branch);
    //           assert.equal('string', data.response.source_project_id);
    //           assert.equal('string', data.response.state);
    //           assert.equal('string', data.response.subscribed);
    //           assert.equal('string', data.response.target_branch);
    //           assert.equal('string', data.response.target_project_id);
    //           assert.equal('string', data.response.title);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('string', data.response.upvotes);
    //           assert.equal('string', data.response.user_notes_count);
    //           assert.equal('string', data.response.web_url);
    //           assert.equal('string', data.response.work_in_progress);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdMergeRequestsMergeRequestIdMerge', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmoji - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmoji(12, 12, 12, 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.awardable_id);
    //           assert.equal('string', data.response.awardable_type);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('object', typeof data.response.user);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmoji', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmoji - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmoji(12, 12, 12, {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.awardable_id);
    //           assert.equal('string', data.response.awardable_type);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('object', typeof data.response.user);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmoji', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmojiAwardId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmojiAwardId('fakedata', 12, 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.awardable_id);
    //           assert.equal('string', data.response.awardable_type);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('object', typeof data.response.user);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmojiAwardId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmojiAwardId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmojiAwardId('fakedata', 12, 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.awardable_id);
    //           assert.equal('string', data.response.awardable_type);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('object', typeof data.response.user);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmojiAwardId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdMergeRequestsMergeRequestIdResetSpentTime - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdMergeRequestsMergeRequestIdResetSpentTime('fakedata', 12, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdMergeRequestsMergeRequestIdResetSpentTime', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdMergeRequestsMergeRequestIdResetTimeEstimate - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdMergeRequestsMergeRequestIdResetTimeEstimate('fakedata', 12, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdMergeRequestsMergeRequestIdResetTimeEstimate', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdMergeRequestsMergeRequestIdTimeEstimate - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdMergeRequestsMergeRequestIdTimeEstimate('fakedata', 12, {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdMergeRequestsMergeRequestIdTimeEstimate', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdMergeRequestsMergeRequestIdTimeStats - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdMergeRequestsMergeRequestIdTimeStats('fakedata', 12, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdMergeRequestsMergeRequestIdTimeStats', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdMergeRequestsMergeRequestIdTodo - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdMergeRequestsMergeRequestIdTodo('fakedata', 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.action_name);
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.bodyQuery);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('object', typeof data.response.project);
    //           assert.equal('string', data.response.state);
    //           assert.equal('string', data.response.target);
    //           assert.equal('string', data.response.target_type);
    //           assert.equal('string', data.response.target_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdMergeRequestsMergeRequestIdTodo', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdMergeRequestsMergeRequestIdVersions - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdMergeRequestsMergeRequestIdVersions('fakedata', 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.base_commit_sha);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.head_commit_sha);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.merge_request_id);
    //           assert.equal('string', data.response.real_size);
    //           assert.equal('string', data.response.start_commit_sha);
    //           assert.equal('string', data.response.state);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdMergeRequestsMergeRequestIdVersions', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdMergeRequestsMergeRequestIdVersionsVersionId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdMergeRequestsMergeRequestIdVersionsVersionId('fakedata', 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.base_commit_sha);
    //           assert.equal('object', typeof data.response.commits);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('object', typeof data.response.diffs);
    //           assert.equal('string', data.response.head_commit_sha);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.merge_request_id);
    //           assert.equal('string', data.response.real_size);
    //           assert.equal('string', data.response.start_commit_sha);
    //           assert.equal('string', data.response.state);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdMergeRequestsMergeRequestIdVersionsVersionId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdMergeRequestsIidNotesId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdMergeRequestsIidNotesId('fakedata', 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.attachment);
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.bodyQuery);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.noteable_id);
    //           assert.equal('string', data.response.noteable_type);
    //           assert.equal('string', data.response.system);
    //           assert.equal('string', data.response.updated_at);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdMergeRequestsIidNotesId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4ProjectsIdMergeRequestsIidNotesId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4ProjectsIdMergeRequestsIidNotesId('fakedata', 12, 12, 'fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.attachment);
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.bodyQuery);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.noteable_id);
    //           assert.equal('string', data.response.noteable_type);
    //           assert.equal('string', data.response.system);
    //           assert.equal('string', data.response.updated_at);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'putV4ProjectsIdMergeRequestsIidNotesId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4ProjectsIdMergeRequestsIidNotesId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4ProjectsIdMergeRequestsIidNotesId('fakedata', 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.attachment);
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.bodyQuery);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.noteable_id);
    //           assert.equal('string', data.response.noteable_type);
    //           assert.equal('string', data.response.system);
    //           assert.equal('string', data.response.updated_at);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'deleteV4ProjectsIdMergeRequestsIidNotesId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdMergeRequestIidDiscussionsIdNotes - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdMergeRequestIidDiscussionsIdNotes('fakedata', 'fakedata', 12, 12, {}, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdMergeRequestIidDiscussionsIdNotes', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4ProjectsIdMergeRequestiidResourceLabelEventId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4ProjectsIdMergeRequestiidResourceLabelEventId('fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(5, data.response.id);
    //           assert.equal('object', typeof data.response.user);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.resource_type);
    //           assert.equal(3, data.response.resource_id);
    //           assert.equal('object', typeof data.response.label);
    //           assert.equal('string', data.response.action);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'getV4ProjectsIdMergeRequestiidResourceLabelEventId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdMergeRequestsIidSubscribe - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdMergeRequestsIidSubscribe('fakedata', 12, {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(4, data.response.id);
    //           assert.equal(6, data.response.iid);
    //           assert.equal(8, data.response.project_id);
    //           assert.equal('string', data.response.title);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.state);
    //           assert.equal('object', typeof data.response.merged_by);
    //           assert.equal('string', data.response.merged_at);
    //           assert.equal('object', typeof data.response.closed_by);
    //           assert.equal('object', typeof data.response.closed_at);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('string', data.response.target_branch);
    //           assert.equal('string', data.response.source_branch);
    //           assert.equal(4, data.response.upvotes);
    //           assert.equal(3, data.response.downvotes);
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('object', typeof data.response.assignee);
    //           assert.equal(true, Array.isArray(data.response.assignees));
    //           assert.equal(9, data.response.source_project_id);
    //           assert.equal(7, data.response.target_project_id);
    //           assert.equal(true, Array.isArray(data.response.labels));
    //           assert.equal(false, data.response.work_in_progress);
    //           assert.equal('object', typeof data.response.milestone);
    //           assert.equal(true, data.response.merge_when_pipeline_succeeds);
    //           assert.equal('string', data.response.merge_status);
    //           assert.equal('string', data.response.sha);
    //           assert.equal('object', typeof data.response.merge_commit_sha);
    //           assert.equal(2, data.response.user_notes_count);
    //           assert.equal('object', typeof data.response.discussion_locked);
    //           assert.equal(false, data.response.should_remove_source_branch);
    //           assert.equal(false, data.response.force_remove_source_branch);
    //           assert.equal(true, data.response.allow_collaboration);
    //           assert.equal(true, data.response.allow_maintainer_to_push);
    //           assert.equal('string', data.response.web_url);
    //           assert.equal('object', typeof data.response.time_stats);
    //           assert.equal(false, data.response.squash);
    //           assert.equal('object', typeof data.response.task_completion_status);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdMergeRequestsIidSubscribe', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ProjectsIdMergeRequestsIidUnsubscribe - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ProjectsIdMergeRequestsIidUnsubscribe(12, 12, {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(10, data.response.id);
    //           assert.equal(10, data.response.iid);
    //           assert.equal(7, data.response.project_id);
    //           assert.equal('string', data.response.title);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.state);
    //           assert.equal('object', typeof data.response.merged_by);
    //           assert.equal('string', data.response.merged_at);
    //           assert.equal('object', typeof data.response.closed_by);
    //           assert.equal('object', typeof data.response.closed_at);
    //           assert.equal('string', data.response.createdAt);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('string', data.response.target_branch);
    //           assert.equal('string', data.response.source_branch);
    //           assert.equal(6, data.response.upvotes);
    //           assert.equal(1, data.response.downvotes);
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('object', typeof data.response.assignee);
    //           assert.equal(true, Array.isArray(data.response.assignees));
    //           assert.equal(1, data.response.source_project_id);
    //           assert.equal(3, data.response.target_project_id);
    //           assert.equal(true, Array.isArray(data.response.labels));
    //           assert.equal(true, data.response.work_in_progress);
    //           assert.equal('object', typeof data.response.milestone);
    //           assert.equal(true, data.response.merge_when_pipeline_succeeds);
    //           assert.equal('string', data.response.merge_status);
    //           assert.equal('string', data.response.sha);
    //           assert.equal('object', typeof data.response.merge_commit_sha);
    //           assert.equal(4, data.response.user_notes_count);
    //           assert.equal('object', typeof data.response.discussion_locked);
    //           assert.equal(false, data.response.should_remove_source_branch);
    //           assert.equal(false, data.response.force_remove_source_branch);
    //           assert.equal(false, data.response.allow_collaboration);
    //           assert.equal(true, data.response.allow_maintainer_to_push);
    //           assert.equal('string', data.response.web_url);
    //           assert.equal('object', typeof data.response.time_stats);
    //           assert.equal(false, data.response.squash);
    //           assert.equal('object', typeof data.response.task_completion_status);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Projects', 'postV4ProjectsIdMergeRequestsIidUnsubscribe', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4Runners - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4Runners(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.active);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.is_shared);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Runners', 'getV4Runners', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#getV4RunnersAll - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4RunnersAll(null, null, null, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.active);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.is_shared);
    //           assert.equal('string', data.response.name);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Runners', 'getV4RunnersAll', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4RunnersId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4RunnersId(12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.active);
    //           assert.equal('string', data.response.architecture);
    //           assert.equal('string', data.response.contacted_at);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.is_shared);
    //           assert.equal('string', data.response.locked);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.platform);
    //           assert.equal('object', typeof data.response.projects);
    //           assert.equal('string', data.response.revision);
    //           assert.equal('string', data.response.run_untagged);
    //           assert.equal('string', data.response.tagList);
    //           assert.equal('string', data.response.token);
    //           assert.equal('string', data.response.version);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Runners', 'getV4RunnersId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4RunnersId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4RunnersId(12, {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.active);
    //           assert.equal('string', data.response.architecture);
    //           assert.equal('string', data.response.contacted_at);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.is_shared);
    //           assert.equal('string', data.response.locked);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.platform);
    //           assert.equal('object', typeof data.response.projects);
    //           assert.equal('string', data.response.revision);
    //           assert.equal('string', data.response.run_untagged);
    //           assert.equal('string', data.response.tagList);
    //           assert.equal('string', data.response.token);
    //           assert.equal('string', data.response.version);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Runners', 'putV4RunnersId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4RunnersId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4RunnersId(12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.active);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.is_shared);
    //           assert.equal('string', data.response.name);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Runners', 'deleteV4RunnersId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4Session - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4Session({}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.avatar_url);
    //           assert.equal('string', data.response.bio);
    //           assert.equal('string', data.response.can_create_group);
    //           assert.equal('string', data.response.can_create_project);
    //           assert.equal('string', data.response.color_scheme_id);
    //           assert.equal('string', data.response.confirmed_at);
    //           assert.equal('string', data.response.created_at);
    //           assert.equal('string', data.response.current_sign_in_at);
    //           assert.equal('string', data.response.email);
    //           assert.equal('string', data.response.external);
    //           assert.equal('string', data.response.id);
    //           assert.equal('object', typeof data.response.identities);
    //           assert.equal('string', data.response.is_admin);
    //           assert.equal('string', data.response.last_sign_in_at);
    //           assert.equal('string', data.response.linkedin);
    //           assert.equal('string', data.response.location);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.organization);
    //           assert.equal('string', data.response.private_token);
    //           assert.equal('string', data.response.projects_limit);
    //           assert.equal('string', data.response.skype);
    //           assert.equal('string', data.response.state);
    //           assert.equal('string', data.response.theme_id);
    //           assert.equal('string', data.response.twitter);
    //           assert.equal('string', data.response.two_factor_enabled);
    //           assert.equal('string', data.response.username);
    //           assert.equal('string', data.response.web_url);
    //           assert.equal('string', data.response.website_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Session', 'postV4Session', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4SidekiqCompoundMetrics - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4SidekiqCompoundMetrics((data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Sidekiq', 'getV4SidekiqCompoundMetrics', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4SidekiqJobStats - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4SidekiqJobStats((data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Sidekiq', 'getV4SidekiqJobStats', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4SidekiqProcessMetrics - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4SidekiqProcessMetrics((data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Sidekiq', 'getV4SidekiqProcessMetrics', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4SidekiqQueueMetrics - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4SidekiqQueueMetrics((data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Sidekiq', 'getV4SidekiqQueueMetrics', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4Snippets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4Snippets((data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.author);
                assert.equal('string', data.response.created_at);
                assert.equal('string', data.response.file_name);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.raw_url);
                assert.equal('string', data.response.title);
                assert.equal('string', data.response.updated_at);
                assert.equal('string', data.response.web_url);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Snippets', 'getV4Snippets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#postV4Snippets - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4Snippets({}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.created_at);
    //           assert.equal('string', data.response.file_name);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.raw_url);
    //           assert.equal('string', data.response.title);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('string', data.response.web_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Snippets', 'postV4Snippets', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4SnippetsPublic - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4SnippetsPublic((data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.created_at);
    //           assert.equal('string', data.response.file_name);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.raw_url);
    //           assert.equal('string', data.response.title);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('string', data.response.web_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Snippets', 'getV4SnippetsPublic', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4SnippetsId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4SnippetsId(12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.created_at);
    //           assert.equal('string', data.response.file_name);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.raw_url);
    //           assert.equal('string', data.response.title);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('string', data.response.web_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Snippets', 'getV4SnippetsId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4SnippetsId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4SnippetsId(12, {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.created_at);
    //           assert.equal('string', data.response.file_name);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.raw_url);
    //           assert.equal('string', data.response.title);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('string', data.response.web_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Snippets', 'putV4SnippetsId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4SnippetsId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4SnippetsId(12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.created_at);
    //           assert.equal('string', data.response.file_name);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.raw_url);
    //           assert.equal('string', data.response.title);
    //           assert.equal('string', data.response.updated_at);
    //           assert.equal('string', data.response.web_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Snippets', 'deleteV4SnippetsId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4SnippetsIdRaw - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4SnippetsIdRaw(12, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Snippets', 'getV4SnippetsIdRaw', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    let getV4TemplatesDockerfilesKey;

    describe('#getV4TemplatesDockerfiles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4TemplatesDockerfiles((data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.key);
                assert.equal('string', data.response.name);
                getV4TemplatesDockerfilesKey = data.response.key;
              } else {
                runCommonAsserts(data, error);
                getV4TemplatesDockerfilesKey = data.response[0].key;
              }

              saveMockData('Templates', 'getV4TemplatesDockerfiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4TemplatesGitignores - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4TemplatesGitignores((data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Templates', 'getV4TemplatesGitignores', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4TemplatesGitlabCiYmls - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4TemplatesGitlabCiYmls((data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Templates', 'getV4TemplatesGitlabCiYmls', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4TemplatesLicenses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4TemplatesLicenses(null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.conditions);
                assert.equal('string', data.response.content);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.html_url);
                assert.equal('string', data.response.key);
                assert.equal('string', data.response.limitations);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.nickname);
                assert.equal('string', data.response.permissions);
                assert.equal('string', data.response.popular);
                assert.equal('string', data.response.source_url);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Templates', 'getV4TemplatesLicenses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4TemplatesDockerfilesKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4TemplatesDockerfilesKey(getV4TemplatesDockerfilesKey, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.content);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Templates', 'getV4TemplatesDockerfilesKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#getV4TemplatesGitignoresKey - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4TemplatesGitignoresKey('fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.content);
    //           assert.equal('string', data.response.name);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Templates', 'getV4TemplatesGitignoresKey', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4TemplatesGitlabCiYmlsKey - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4TemplatesGitlabCiYmlsKey('fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.content);
    //           assert.equal('string', data.response.name);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Templates', 'getV4TemplatesGitlabCiYmlsKey', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4TemplatesLicensesKey - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4TemplatesLicensesKey('fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.conditions);
    //           assert.equal('string', data.response.content);
    //           assert.equal('string', data.response.description);
    //           assert.equal('string', data.response.html_url);
    //           assert.equal('string', data.response.key);
    //           assert.equal('string', data.response.limitations);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.nickname);
    //           assert.equal('string', data.response.permissions);
    //           assert.equal('string', data.response.popular);
    //           assert.equal('string', data.response.source_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Templates', 'getV4TemplatesLicensesKey', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4Todos - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4Todos(null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.action_name);
                assert.equal('object', typeof data.response.author);
                assert.equal('string', data.response.body);
                assert.equal('string', data.response.created_at);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.project);
                assert.equal('string', data.response.state);
                assert.equal('string', data.response.target);
                assert.equal('string', data.response.target_type);
                assert.equal('string', data.response.target_url);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Todos', 'getV4Todos', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#postV4TodosMarkAsDone - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4TodosMarkAsDone((data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Todos', 'postV4TodosMarkAsDone', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4TodosId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4TodosId(12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.action_name);
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.body);
    //           assert.equal('string', data.response.created_at);
    //           assert.equal('string', data.response.id);
    //           assert.equal('object', typeof data.response.project);
    //           assert.equal('string', data.response.state);
    //           assert.equal('string', data.response.target);
    //           assert.equal('string', data.response.target_type);
    //           assert.equal('string', data.response.target_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Todos', 'deleteV4TodosId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4Users - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4Users('fakedata', 'fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.avatar_url);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.state);
    //           assert.equal('string', data.response.username);
    //           assert.equal('string', data.response.web_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Users', 'getV4Users', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4Users - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4Users({}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.avatar_url);
    //           assert.equal('string', data.response.bio);
    //           assert.equal('string', data.response.can_create_group);
    //           assert.equal('string', data.response.can_create_project);
    //           assert.equal('string', data.response.color_scheme_id);
    //           assert.equal('string', data.response.confirmed_at);
    //           assert.equal('string', data.response.created_at);
    //           assert.equal('string', data.response.current_sign_in_at);
    //           assert.equal('string', data.response.email);
    //           assert.equal('string', data.response.external);
    //           assert.equal('string', data.response.id);
    //           assert.equal('object', typeof data.response.identities);
    //           assert.equal('string', data.response.is_admin);
    //           assert.equal('string', data.response.last_sign_in_at);
    //           assert.equal('string', data.response.linkedin);
    //           assert.equal('string', data.response.location);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.organization);
    //           assert.equal('string', data.response.projects_limit);
    //           assert.equal('string', data.response.skype);
    //           assert.equal('string', data.response.state);
    //           assert.equal('string', data.response.theme_id);
    //           assert.equal('string', data.response.twitter);
    //           assert.equal('string', data.response.two_factor_enabled);
    //           assert.equal('string', data.response.username);
    //           assert.equal('string', data.response.web_url);
    //           assert.equal('string', data.response.website_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Users', 'postV4Users', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4Users - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4Users(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.avatar_url);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.state);
                assert.equal('string', data.response.username);
                assert.equal('string', data.response.web_url);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Users', 'getV4Users', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#putV4UsersId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4UsersId(12, {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.avatar_url);
    //           assert.equal('string', data.response.bio);
    //           assert.equal('string', data.response.can_create_group);
    //           assert.equal('string', data.response.can_create_project);
    //           assert.equal('string', data.response.color_scheme_id);
    //           assert.equal('string', data.response.confirmed_at);
    //           assert.equal('string', data.response.created_at);
    //           assert.equal('string', data.response.current_sign_in_at);
    //           assert.equal('string', data.response.email);
    //           assert.equal('string', data.response.external);
    //           assert.equal('string', data.response.id);
    //           assert.equal('object', typeof data.response.identities);
    //           assert.equal('string', data.response.is_admin);
    //           assert.equal('string', data.response.last_sign_in_at);
    //           assert.equal('string', data.response.linkedin);
    //           assert.equal('string', data.response.location);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.organization);
    //           assert.equal('string', data.response.projects_limit);
    //           assert.equal('string', data.response.skype);
    //           assert.equal('string', data.response.state);
    //           assert.equal('string', data.response.theme_id);
    //           assert.equal('string', data.response.twitter);
    //           assert.equal('string', data.response.two_factor_enabled);
    //           assert.equal('string', data.response.username);
    //           assert.equal('string', data.response.web_url);
    //           assert.equal('string', data.response.website_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Users', 'putV4UsersId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4UsersId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4UsersId(12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.email);
    //           assert.equal('string', data.response.id);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Users', 'deleteV4UsersId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4UsersIdBlock - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4UsersIdBlock(12, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Users', 'putV4UsersIdBlock', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4UsersIdEmails - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4UsersIdEmails(12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.email);
    //           assert.equal('string', data.response.id);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Users', 'getV4UsersIdEmails', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4UsersIdEmails - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4UsersIdEmails(12, {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.email);
    //           assert.equal('string', data.response.id);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Users', 'postV4UsersIdEmails', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4UsersIdEmailsEmailId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4UsersIdEmailsEmailId(12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.email);
    //           assert.equal('string', data.response.id);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Users', 'deleteV4UsersIdEmailsEmailId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4UsersIdEvents - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4UsersIdEvents(12, 12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.action_name);
    //           assert.equal('object', typeof data.response.author);
    //           assert.equal('string', data.response.author_id);
    //           assert.equal('string', data.response.author_username);
    //           assert.equal('string', data.response.created_at);
    //           assert.equal('string', data.response.data);
    //           assert.equal('object', typeof data.response.note);
    //           assert.equal('string', data.response.project_id);
    //           assert.equal('string', data.response.target_id);
    //           assert.equal('string', data.response.target_title);
    //           assert.equal('string', data.response.target_type);
    //           assert.equal('string', data.response.title);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Users', 'getV4UsersIdEvents', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4UsersIdKeys - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4UsersIdKeys(12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.can_push);
    //           assert.equal('string', data.response.created_at);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.key);
    //           assert.equal('string', data.response.title);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Users', 'getV4UsersIdKeys', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4UsersIdKeys - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4UsersIdKeys(12, {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.can_push);
    //           assert.equal('string', data.response.created_at);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.key);
    //           assert.equal('string', data.response.title);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Users', 'postV4UsersIdKeys', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4UsersIdKeysKeyId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4UsersIdKeysKeyId(12, 12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.can_push);
    //           assert.equal('string', data.response.created_at);
    //           assert.equal('string', data.response.id);
    //           assert.equal('string', data.response.key);
    //           assert.equal('string', data.response.title);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Users', 'deleteV4UsersIdKeysKeyId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4UsersIdUnblock - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4UsersIdUnblock(12, (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Users', 'putV4UsersIdUnblock', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4UsersIdProjects - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4UsersIdProjects('fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response['0']);
    //           assert.equal('object', typeof data.response['1']);
    //           assert.equal('object', typeof data.response['2']);
    //           assert.equal('object', typeof data.response['3']);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Users', 'getV4UsersIdProjects', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4UsersIdStatus - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4UsersIdStatus(12, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.emoji);
    //           assert.equal('string', data.response.message);
    //           assert.equal('string', data.response.message_html);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Users', 'getV4UsersIdStatus', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4Version - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4Version((data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.revision);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Version', 'getV4Version', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#getV4Applications - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4Applications((data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response['0']);
    //           assert.equal('object', typeof data.response['1']);
    //           assert.equal('object', typeof data.response['2']);
    //           assert.equal('object', typeof data.response['3']);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Applications', 'getV4Applications', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4Applications - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4Applications((data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(3, data.response.id);
    //           assert.equal('string', data.response.application_id);
    //           assert.equal('string', data.response.application_name);
    //           assert.equal('string', data.response.secret);
    //           assert.equal('string', data.response.callback_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Applications', 'postV4Applications', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4Application - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4Application('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Applications', 'deleteV4Application', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4PagesDomains - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4PagesDomains((data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.domain);
    //           assert.equal('string', data.response.url);
    //           assert.equal(9, data.response.project_id);
    //           assert.equal('object', typeof data.response.certificate);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Pages', 'getV4PagesDomains', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4Avatar - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4Avatar('fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.avatar_url);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Avatar', 'getV4Avatar', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4BroadcastMessage - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4BroadcastMessage((data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response['0']);
    //           assert.equal('object', typeof data.response['1']);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('BroadcastMessage', 'getV4BroadcastMessage', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4BroadcastMessage - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4BroadcastMessage({}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.message);
    //           assert.equal('string', data.response.starts_at);
    //           assert.equal('string', data.response.ends_at);
    //           assert.equal('string', data.response.color);
    //           assert.equal('string', data.response.font);
    //           assert.equal(6, data.response.id);
    //           assert.equal(true, data.response.active);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('BroadcastMessage', 'postV4BroadcastMessage', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#getV4BroadcastMessageId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4BroadcastMessageId('fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.message);
    //           assert.equal('string', data.response.starts_at);
    //           assert.equal('string', data.response.ends_at);
    //           assert.equal('string', data.response.color);
    //           assert.equal('string', data.response.font);
    //           assert.equal(2, data.response.id);
    //           assert.equal(false, data.response.active);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('BroadcastMessage', 'getV4BroadcastMessageId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4BroadcastMessageId - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4BroadcastMessageId('fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.message);
    //           assert.equal('string', data.response.starts_at);
    //           assert.equal('string', data.response.ends_at);
    //           assert.equal('string', data.response.color);
    //           assert.equal('string', data.response.font);
    //           assert.equal(3, data.response.id);
    //           assert.equal(true, data.response.active);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('BroadcastMessage', 'putV4BroadcastMessageId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4BroadcastMessageId - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4BroadcastMessageId('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('BroadcastMessage', 'deleteV4BroadcastMessageId', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4Events - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4Events((data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response['0']);
                assert.equal('object', typeof data.response['1']);
                assert.equal('object', typeof data.response['2']);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Events', 'getV4Events', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#getV4Features - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4Features((data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response['0']);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Features', 'getV4Features', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4FeaturesName - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4FeaturesName('fakedata', {}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.state);
    //           assert.equal(true, Array.isArray(data.response.gates));
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Features', 'postV4FeaturesName', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#deleteV4FeaturesName - errors', () => {
    //   it('should work if integrated but since no mockdata should error when run standalone', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.deleteV4FeaturesName('fakedata', (data, error) => {
    //         if (stub) {
    //           const displayE = 'Error 400 received on request';
    //           runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Features', 'deleteV4FeaturesName', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4ImportGithub - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4ImportGithub({}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(10, data.response.id);
    //           assert.equal('string', data.response.name);
    //           assert.equal('string', data.response.full_path);
    //           assert.equal('string', data.response.full_name);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('ImportGithub', 'postV4ImportGithub', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4Markdown - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4Markdown({}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.html);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Markdown', 'postV4Markdown', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#getV4MergeRequests - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4MergeRequests(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(8, data.response.id);
                assert.equal(8, data.response.iid);
                assert.equal(7, data.response.project_id);
                assert.equal('string', data.response.title);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.state);
                assert.equal('object', typeof data.response.merged_by);
                assert.equal('string', data.response.merged_at);
                assert.equal('object', typeof data.response.closed_by);
                assert.equal('object', typeof data.response.closed_at);
                assert.equal('string', data.response.created_at);
                assert.equal('string', data.response.updated_at);
                assert.equal('string', data.response.target_branch);
                assert.equal('string', data.response.sourceBranch);
                assert.equal(10, data.response.upvotes);
                assert.equal(9, data.response.downvotes);
                assert.equal('object', typeof data.response.author);
                assert.equal('object', typeof data.response.assignee);
                assert.equal(true, Array.isArray(data.response.assignees));
                assert.equal(7, data.response.source_project_id);
                assert.equal(10, data.response.target_project_id);
                assert.equal(true, Array.isArray(data.response.labels));
                assert.equal(true, data.response.work_in_progress);
                assert.equal('object', typeof data.response.milestone);
                assert.equal(true, data.response.merge_when_pipeline_succeeds);
                assert.equal('string', data.response.merge_status);
                assert.equal('string', data.response.sha);
                assert.equal('object', typeof data.response.merge_commit_sha);
                assert.equal(8, data.response.user_notes_count);
                assert.equal('object', typeof data.response.discussion_locked);
                assert.equal(true, data.response.should_remove_source_branch);
                assert.equal(false, data.response.force_remove_source_branch);
                assert.equal(true, data.response.allow_collaboration);
                assert.equal(true, data.response.allow_maintainer_to_push);
                assert.equal('string', data.response.web_url);
                assert.equal('object', typeof data.response.time_stats);
                assert.equal(true, data.response.squash);
                assert.equal('object', typeof data.response.task_completion_status);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('MergeRequests', 'getV4MergeRequests', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#getV4Search - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.getV4Search('fakedata', 'fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('object', typeof data.response['0']);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Search', 'getV4Search', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#putV4SuggestionIdApply - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.putV4SuggestionIdApply('fakedata', (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal(8, data.response.id);
    //           assert.equal(6, data.response.from_line);
    //           assert.equal(1, data.response.to_line);
    //           assert.equal(false, data.response.appliable);
    //           assert.equal(true, data.response.applied);
    //           assert.equal('string', data.response.from_content);
    //           assert.equal('string', data.response.to_content);
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Suggestions', 'putV4SuggestionIdApply', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    // describe('#postV4Lint - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     const p = new Promise((resolve) => {
    //       a.postV4Lint({}, (data, error) => {
    //         runCommonAsserts(data, error);

    //         if (stub) {
    //           assert.equal('string', data.response.status);
    //           assert.equal(true, Array.isArray(data.response.errors));
    //         } else {
    //           runCommonAsserts(data, error);
    //         }

    //         saveMockData('Lint', 'postV4Lint', 'default', data);
    //         resolve(data);
    //         done();
    //       });
    //     });
    //     // log just done to get rid of const lint issue!
    //     log.debug(p);
    //   }).timeout(attemptTimeout);
    // });

    describe('#deleteV4ProjectsIdIssuesIssueId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteV4ProjectsIdIssuesIssueId(projectId, projectIssueIid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'deleteV4ProjectsIdIssuesIssueId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectIdMilestonesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteV4ProjectIdMilestonesId(projectId, projectMilestoneId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'deleteV4ProjectIdMilestonesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteV4ProjectsId(projectId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'deleteV4ProjectsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);

      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteV4ProjectsId(newForkProjectId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Projects', 'deleteV4ProjectsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4SubGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4SubGroups(groupId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gitlab-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('groups', 'getV4SubGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
