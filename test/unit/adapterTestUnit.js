/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs-extra');
const mocha = require('mocha');
const path = require('path');
const util = require('util');
const winston = require('winston');
const execute = require('child_process').execSync;
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
samProps.request.attempt_timeout = 60000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-gitlab',
      type: 'Gitlab',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const Gitlab = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] GitLab Adapter Test', () => {
  describe('Gitlab Class Tests', () => {
    const a = new Gitlab(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          const { PJV } = require('package-json-validator');
          const options = {
            warnings: true, // show warnings
            recommendations: true // show recommendations
          };
          const results = PJV.validate(JSON.stringify(packageDotJson), 'npm', options);

          if (results.valid === false) {
            log.error('The package.json contains the following errors: ');
            log.error(util.inspect(results));
            assert.equal(true, results.valid);
          } else {
            assert.equal(true, results.valid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('gitlab'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js && npm install --package-lock-only --ignore-scripts && npx npm-force-resolutions', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('nyc --reporter html --reporter text mocha --reporter dot test/*', packageDotJson.scripts['test:cover']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^6.12.0', packageDotJson.dependencies.ajv);
          assert.equal('^0.21.0', packageDotJson.dependencies.axios);
          assert.equal('^2.20.0', packageDotJson.dependencies.commander);
          assert.equal('^8.1.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^9.0.1', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.5.3', packageDotJson.dependencies['network-diagnostics']);
          assert.equal('^15.1.0', packageDotJson.dependencies.nyc);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.3.2', packageDotJson.dependencies.semver);
          assert.equal('^3.3.3', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.4', packageDotJson.devDependencies.chai);
          assert.equal('^7.29.0', packageDotJson.devDependencies.eslint);
          assert.equal('^14.2.1', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.23.4', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.0.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^0.6.3', packageDotJson.devDependencies['package-json-validator']);
          assert.equal('^3.16.1', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('gitlab'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('Gitlab', pronghornDotJson.export);
          assert.equal('GitLab', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-gitlab', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('gitlab'));
          assert.equal('Gitlab', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#iapHasAdapterEntity', () => {
    //   it('should have a iapHasAdapterEntity function', (done) => {
    //     try {
    //       assert.equal(true, typeof a.iapHasAdapterEntity === 'function');
    //       done();
    //     } catch (error) {
    //       log.error(`Test Failure: ${error}`);
    //       done(error);
    //     }
    //   });
    //   it('should find entity', (done) => {
    //     try {
    //       a.iapHasAdapterEntity('template_entity', // 'a9e9c33dc61122760072455df62663d2', (data) => {
    //         try {
    //           assert.equal(true, data[0]);
    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    //   it('should not find entity', (done) => {
    //     try {
    //       a.iapHasAdapterEntity('template_entity', 'blah', (data) => {
    //         try {
    //           assert.equal(false, data[0]);
    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#getV4ApplicationSettings - errors', () => {
      it('should have a getV4ApplicationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ApplicationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ApplicationSettings - errors', () => {
      it('should have a putV4ApplicationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ApplicationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ApplicationSettings(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ApplicationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4DeployKeys - errors', () => {
      it('should have a getV4DeployKeys function', (done) => {
        try {
          assert.equal(true, typeof a.getV4DeployKeys === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4Groups - errors', () => {
      it('should have a getV4Groups function', (done) => {
        try {
          assert.equal(true, typeof a.getV4Groups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4Groups - errors', () => {
      it('should have a postV4Groups function', (done) => {
        try {
          assert.equal(true, typeof a.postV4Groups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4Groups(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4Groups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4GroupsId - errors', () => {
      it('should have a getV4GroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4GroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4GroupsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4GroupsId - errors', () => {
      it('should have a putV4GroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.putV4GroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4GroupsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4GroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4GroupsId - errors', () => {
      it('should have a deleteV4GroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4GroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4GroupsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4GroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4GroupsIdAccessRequests - errors', () => {
      it('should have a getV4GroupsIdAccessRequests function', (done) => {
        try {
          assert.equal(true, typeof a.getV4GroupsIdAccessRequests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4GroupsIdAccessRequests(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdAccessRequests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4GroupsIdAccessRequests - errors', () => {
      it('should have a postV4GroupsIdAccessRequests function', (done) => {
        try {
          assert.equal(true, typeof a.postV4GroupsIdAccessRequests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4GroupsIdAccessRequests(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4GroupsIdAccessRequests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4GroupsIdAccessRequestsUserId - errors', () => {
      it('should have a deleteV4GroupsIdAccessRequestsUserId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4GroupsIdAccessRequestsUserId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4GroupsIdAccessRequestsUserId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4GroupsIdAccessRequestsUserId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.deleteV4GroupsIdAccessRequestsUserId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4GroupsIdAccessRequestsUserId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4GroupsIdAccessRequestsUserIdApprove - errors', () => {
      it('should have a putV4GroupsIdAccessRequestsUserIdApprove function', (done) => {
        try {
          assert.equal(true, typeof a.putV4GroupsIdAccessRequestsUserIdApprove === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4GroupsIdAccessRequestsUserIdApprove(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4GroupsIdAccessRequestsUserIdApprove', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.putV4GroupsIdAccessRequestsUserIdApprove('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4GroupsIdAccessRequestsUserIdApprove', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4GroupsIdIssues - errors', () => {
      it('should have a getV4GroupsIdIssues function', (done) => {
        try {
          assert.equal(true, typeof a.getV4GroupsIdIssues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4GroupsIdIssues(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdIssues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4GroupsIdMembers - errors', () => {
      it('should have a getV4GroupsIdMembers function', (done) => {
        try {
          assert.equal(true, typeof a.getV4GroupsIdMembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4GroupsIdMembers(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdMembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4GroupsIdMembers - errors', () => {
      it('should have a postV4GroupsIdMembers function', (done) => {
        try {
          assert.equal(true, typeof a.postV4GroupsIdMembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4GroupsIdMembers(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4GroupsIdMembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4GroupsIdMembers('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4GroupsIdMembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4GroupsIdMembersUserId - errors', () => {
      it('should have a getV4GroupsIdMembersUserId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4GroupsIdMembersUserId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4GroupsIdMembersUserId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdMembersUserId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.getV4GroupsIdMembersUserId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdMembersUserId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4GroupsIdMembersUserId - errors', () => {
      it('should have a putV4GroupsIdMembersUserId function', (done) => {
        try {
          assert.equal(true, typeof a.putV4GroupsIdMembersUserId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4GroupsIdMembersUserId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4GroupsIdMembersUserId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.putV4GroupsIdMembersUserId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4GroupsIdMembersUserId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4GroupsIdMembersUserId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4GroupsIdMembersUserId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4GroupsIdMembersUserId - errors', () => {
      it('should have a deleteV4GroupsIdMembersUserId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4GroupsIdMembersUserId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4GroupsIdMembersUserId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4GroupsIdMembersUserId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.deleteV4GroupsIdMembersUserId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4GroupsIdMembersUserId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4GroupsIdNotificationSettings - errors', () => {
      it('should have a getV4GroupsIdNotificationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getV4GroupsIdNotificationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4GroupsIdNotificationSettings(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdNotificationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4GroupsIdNotificationSettings - errors', () => {
      it('should have a putV4GroupsIdNotificationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.putV4GroupsIdNotificationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4GroupsIdNotificationSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4GroupsIdNotificationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4GroupsIdProjects - errors', () => {
      it('should have a getV4GroupsIdProjects function', (done) => {
        try {
          assert.equal(true, typeof a.getV4GroupsIdProjects === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4GroupsIdProjects(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdProjects', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4GroupsIdProjectsProjectId - errors', () => {
      it('should have a postV4GroupsIdProjectsProjectId function', (done) => {
        try {
          assert.equal(true, typeof a.postV4GroupsIdProjectsProjectId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4GroupsIdProjectsProjectId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4GroupsIdProjectsProjectId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.postV4GroupsIdProjectsProjectId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4GroupsIdProjectsProjectId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4GroupsIdCustomAttributes - errors', () => {
      it('should have a getV4GroupsIdCustomAttributes function', (done) => {
        try {
          assert.equal(true, typeof a.getV4GroupsIdCustomAttributes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4GroupsIdCustomAttributes(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdCustomAttributes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4GroupsIdCustomAttributesKey - errors', () => {
      it('should have a getV4GroupsIdCustomAttributesKey function', (done) => {
        try {
          assert.equal(true, typeof a.getV4GroupsIdCustomAttributesKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.getV4GroupsIdCustomAttributesKey(null, null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdCustomAttributesKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4GroupsIdCustomAttributesKey('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdCustomAttributesKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4GroupsIdCustomAttributes - errors', () => {
      it('should have a putV4GroupsIdCustomAttributes function', (done) => {
        try {
          assert.equal(true, typeof a.putV4GroupsIdCustomAttributes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.putV4GroupsIdCustomAttributes(null, null, null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4GroupsIdCustomAttributes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4GroupsIdCustomAttributes('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4GroupsIdCustomAttributes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4GroupsIdCustomAttributes('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4GroupsIdCustomAttributes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4GroupsIdCustomAttributes - errors', () => {
      it('should have a deleteV4GroupsIdCustomAttributes function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4GroupsIdCustomAttributes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4GroupsIdCustomAttributes(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4GroupsIdCustomAttributes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.deleteV4GroupsIdCustomAttributes('fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4GroupsIdCustomAttributes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4GroupsIdBadges - errors', () => {
      it('should have a getV4GroupsIdBadges function', (done) => {
        try {
          assert.equal(true, typeof a.getV4GroupsIdBadges === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4GroupsIdBadges(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdBadges', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4GroupsIdBadges - errors', () => {
      it('should have a postV4GroupsIdBadges function', (done) => {
        try {
          assert.equal(true, typeof a.postV4GroupsIdBadges === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4GroupsIdBadges(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4GroupsIdBadges', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4GroupsIdBadges('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4GroupsIdBadges', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4GroupsIdBadgesBadgeId - errors', () => {
      it('should have a getV4GroupsIdBadgesBadgeId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4GroupsIdBadgesBadgeId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4GroupsIdBadgesBadgeId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdBadgesBadgeId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing badgeId', (done) => {
        try {
          a.getV4GroupsIdBadgesBadgeId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'badgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdBadgesBadgeId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4GroupsIdBadgesBadgedId - errors', () => {
      it('should have a putV4GroupsIdBadgesBadgedId function', (done) => {
        try {
          assert.equal(true, typeof a.putV4GroupsIdBadgesBadgedId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4GroupsIdBadgesBadgedId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4GroupsIdBadgesBadgedId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing badgeId', (done) => {
        try {
          a.putV4GroupsIdBadgesBadgedId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'badgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4GroupsIdBadgesBadgedId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4GroupsIdBadgesBadgedId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4GroupsIdBadgesBadgedId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4GroupsIdBadgesBadgesId - errors', () => {
      it('should have a deleteV4GroupsIdBadgesBadgesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4GroupsIdBadgesBadgesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4GroupsIdBadgesBadgesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4GroupsIdBadgesBadgesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing badgeId', (done) => {
        try {
          a.deleteV4GroupsIdBadgesBadgesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'badgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4GroupsIdBadgesBadgesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4GroupsIdBadgesRender - errors', () => {
      it('should have a getV4GroupsIdBadgesRender function', (done) => {
        try {
          assert.equal(true, typeof a.getV4GroupsIdBadgesRender === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4GroupsIdBadgesRender(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdBadgesRender', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing imageUrl', (done) => {
        try {
          a.getV4GroupsIdBadgesRender('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'imageUrl is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdBadgesRender', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkUrl', (done) => {
        try {
          a.getV4GroupsIdBadgesRender('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'linkUrl is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdBadgesRender', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4GroupsIdBoards - errors', () => {
      it('should have a getV4GroupsIdBoards function', (done) => {
        try {
          assert.equal(true, typeof a.getV4GroupsIdBoards === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4GroupsIdBoards(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdBoards', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4GroupsIdBoards - errors', () => {
      it('should have a postV4GroupsIdBoards function', (done) => {
        try {
          assert.equal(true, typeof a.postV4GroupsIdBoards === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postV4GroupsIdBoards(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4GroupsIdBoards', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4GroupsIdBoards('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4GroupsIdBoards', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4GroupsIdBoardsBoardId - errors', () => {
      it('should have a getV4GroupsIdBoardsBoardId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4GroupsIdBoardsBoardId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing boardId', (done) => {
        try {
          a.getV4GroupsIdBoardsBoardId(null, null, (data, error) => {
            try {
              const displayE = 'boardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdBoardsBoardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4GroupsIdBoardsBoardId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdBoardsBoardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4GroupsIdBoardsBoardId - errors', () => {
      it('should have a putV4GroupsIdBoardsBoardId function', (done) => {
        try {
          assert.equal(true, typeof a.putV4GroupsIdBoardsBoardId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing boardId', (done) => {
        try {
          a.putV4GroupsIdBoardsBoardId(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'boardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4GroupsIdBoardsBoardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4GroupsIdBoardsBoardId('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4GroupsIdBoardsBoardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4GroupsIdBoardsBoardId - errors', () => {
      it('should have a deleteV4GroupsIdBoardsBoardId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4GroupsIdBoardsBoardId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4GroupsIdBoardsBoardId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4GroupsIdBoardsBoardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing boardId', (done) => {
        try {
          a.deleteV4GroupsIdBoardsBoardId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'boardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4GroupsIdBoardsBoardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4GroupsIdBoardsBoardIdLists - errors', () => {
      it('should have a getV4GroupsIdBoardsBoardIdLists function', (done) => {
        try {
          assert.equal(true, typeof a.getV4GroupsIdBoardsBoardIdLists === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing boardId', (done) => {
        try {
          a.getV4GroupsIdBoardsBoardIdLists(null, null, (data, error) => {
            try {
              const displayE = 'boardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdBoardsBoardIdLists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4GroupsIdBoardsBoardIdLists('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdBoardsBoardIdLists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4GroupsIdBoardsBoardIdLists - errors', () => {
      it('should have a postV4GroupsIdBoardsBoardIdLists function', (done) => {
        try {
          assert.equal(true, typeof a.postV4GroupsIdBoardsBoardIdLists === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labelId', (done) => {
        try {
          a.postV4GroupsIdBoardsBoardIdLists(null, null, null, (data, error) => {
            try {
              const displayE = 'labelId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4GroupsIdBoardsBoardIdLists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4GroupsIdBoardsBoardIdLists('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4GroupsIdBoardsBoardIdLists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing boardId', (done) => {
        try {
          a.postV4GroupsIdBoardsBoardIdLists('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'boardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4GroupsIdBoardsBoardIdLists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4GroupsIdBoardsBoardIdListsListId - errors', () => {
      it('should have a getV4GroupsIdBoardsBoardIdListsListId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4GroupsIdBoardsBoardIdListsListId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4GroupsIdBoardsBoardIdListsListId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdBoardsBoardIdListsListId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing boardId', (done) => {
        try {
          a.getV4GroupsIdBoardsBoardIdListsListId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'boardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdBoardsBoardIdListsListId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing listId', (done) => {
        try {
          a.getV4GroupsIdBoardsBoardIdListsListId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'listId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdBoardsBoardIdListsListId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4GroupsIdBoardsBoardsIdListsListId - errors', () => {
      it('should have a putV4GroupsIdBoardsBoardsIdListsListId function', (done) => {
        try {
          assert.equal(true, typeof a.putV4GroupsIdBoardsBoardsIdListsListId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing position', (done) => {
        try {
          a.putV4GroupsIdBoardsBoardsIdListsListId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'position is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4GroupsIdBoardsBoardsIdListsListId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4GroupsIdBoardsBoardsIdListsListId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4GroupsIdBoardsBoardsIdListsListId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing boardId', (done) => {
        try {
          a.putV4GroupsIdBoardsBoardsIdListsListId('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'boardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4GroupsIdBoardsBoardsIdListsListId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing listId', (done) => {
        try {
          a.putV4GroupsIdBoardsBoardsIdListsListId('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'listId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4GroupsIdBoardsBoardsIdListsListId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4GroupsIdBoardsBoardIdListsListId - errors', () => {
      it('should have a deleteV4GroupsIdBoardsBoardIdListsListId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4GroupsIdBoardsBoardIdListsListId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4GroupsIdBoardsBoardIdListsListId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4GroupsIdBoardsBoardIdListsListId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing boardId', (done) => {
        try {
          a.deleteV4GroupsIdBoardsBoardIdListsListId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'boardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4GroupsIdBoardsBoardIdListsListId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing listId', (done) => {
        try {
          a.deleteV4GroupsIdBoardsBoardIdListsListId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'listId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4GroupsIdBoardsBoardIdListsListId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4GroupsIdLabels - errors', () => {
      it('should have a getV4GroupsIdLabels function', (done) => {
        try {
          assert.equal(true, typeof a.getV4GroupsIdLabels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4GroupsIdLabels(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdLabels', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4GroupsIdLabels - errors', () => {
      it('should have a putV4GroupsIdLabels function', (done) => {
        try {
          assert.equal(true, typeof a.putV4GroupsIdLabels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4GroupsIdLabels(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4GroupsIdLabels', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4GroupsIdLabels('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4GroupsIdLabels', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4GroupsIdLabels - errors', () => {
      it('should have a postV4GroupsIdLabels function', (done) => {
        try {
          assert.equal(true, typeof a.postV4GroupsIdLabels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4GroupsIdLabels(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4GroupsIdLabels', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4GroupsIdLabels('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4GroupsIdLabels', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4GroupsIdLabels - errors', () => {
      it('should have a deleteV4GroupsIdLabels function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4GroupsIdLabels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteV4GroupsIdLabels(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4GroupsIdLabels', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4GroupsIdLabels('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4GroupsIdLabels', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4GroupsIdLabelsLabelIdSubscribe - errors', () => {
      it('should have a postV4GroupsIdLabelsLabelIdSubscribe function', (done) => {
        try {
          assert.equal(true, typeof a.postV4GroupsIdLabelsLabelIdSubscribe === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4GroupsIdLabelsLabelIdSubscribe(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4GroupsIdLabelsLabelIdSubscribe', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labelId', (done) => {
        try {
          a.postV4GroupsIdLabelsLabelIdSubscribe('fakeparam', null, (data, error) => {
            try {
              const displayE = 'labelId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4GroupsIdLabelsLabelIdSubscribe', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4GroupsIdLabelsLabelIdUnsubscribe - errors', () => {
      it('should have a postV4GroupsIdLabelsLabelIdUnsubscribe function', (done) => {
        try {
          assert.equal(true, typeof a.postV4GroupsIdLabelsLabelIdUnsubscribe === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4GroupsIdLabelsLabelIdUnsubscribe(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4GroupsIdLabelsLabelIdUnsubscribe', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labelId', (done) => {
        try {
          a.postV4GroupsIdLabelsLabelIdUnsubscribe('fakeparam', null, (data, error) => {
            try {
              const displayE = 'labelId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4GroupsIdLabelsLabelIdUnsubscribe', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4GroupsIdVariables - errors', () => {
      it('should have a getV4GroupsIdVariables function', (done) => {
        try {
          assert.equal(true, typeof a.getV4GroupsIdVariables === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4GroupsIdVariables(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdVariables', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4GroupsIdVariables - errors', () => {
      it('should have a postV4GroupsIdVariables function', (done) => {
        try {
          assert.equal(true, typeof a.postV4GroupsIdVariables === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4GroupsIdVariables(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4GroupsIdVariables', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4GroupsIdVariablesKey - errors', () => {
      it('should have a putV4GroupsIdVariablesKey function', (done) => {
        try {
          assert.equal(true, typeof a.putV4GroupsIdVariablesKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4GroupsIdVariablesKey(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4GroupsIdVariablesKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.putV4GroupsIdVariablesKey('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4GroupsIdVariablesKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4GroupsIdVariableKey - errors', () => {
      it('should have a deleteV4GroupsIdVariableKey function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4GroupsIdVariableKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4GroupsIdVariableKey(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4GroupsIdVariableKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.deleteV4GroupsIdVariableKey('fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4GroupsIdVariableKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4GroupsIdMilestones - errors', () => {
      it('should have a getV4GroupsIdMilestones function', (done) => {
        try {
          assert.equal(true, typeof a.getV4GroupsIdMilestones === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4GroupsIdMilestones(null, 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdMilestones', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4GroupsIdMilestones - errors', () => {
      it('should have a postV4GroupsIdMilestones function', (done) => {
        try {
          assert.equal(true, typeof a.postV4GroupsIdMilestones === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4GroupsIdMilestones(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4GroupsIdMilestones', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4GroupsIdMilestones('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4GroupsIdMilestones', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4GroupsIdMilestonesMilestonesId - errors', () => {
      it('should have a getV4GroupsIdMilestonesMilestonesId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4GroupsIdMilestonesMilestonesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4GroupsIdMilestonesMilestonesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdMilestonesMilestonesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing milestonesId', (done) => {
        try {
          a.getV4GroupsIdMilestonesMilestonesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'milestonesId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdMilestonesMilestonesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4GroupsIdMilestonesMilestonesId - errors', () => {
      it('should have a putV4GroupsIdMilestonesMilestonesId function', (done) => {
        try {
          assert.equal(true, typeof a.putV4GroupsIdMilestonesMilestonesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4GroupsIdMilestonesMilestonesId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4GroupsIdMilestonesMilestonesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing milestonesId', (done) => {
        try {
          a.putV4GroupsIdMilestonesMilestonesId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'milestonesId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4GroupsIdMilestonesMilestonesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4GroupsIdMilestonesMilestonesId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4GroupsIdMilestonesMilestonesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4GroupsIdMilestonesMilestonesId - errors', () => {
      it('should have a deleteV4GroupsIdMilestonesMilestonesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4GroupsIdMilestonesMilestonesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4GroupsIdMilestonesMilestonesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4GroupsIdMilestonesMilestonesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing milestonesId', (done) => {
        try {
          a.deleteV4GroupsIdMilestonesMilestonesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'milestonesId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4GroupsIdMilestonesMilestonesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4GroupsIdMilestonesMilestoneIdMergeRequests - errors', () => {
      it('should have a getV4GroupsIdMilestonesMilestoneIdMergeRequests function', (done) => {
        try {
          assert.equal(true, typeof a.getV4GroupsIdMilestonesMilestoneIdMergeRequests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing milestonesId', (done) => {
        try {
          a.getV4GroupsIdMilestonesMilestoneIdMergeRequests(null, null, (data, error) => {
            try {
              const displayE = 'milestonesId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdMilestonesMilestoneIdMergeRequests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4GroupsIdMilestonesMilestoneIdMergeRequests('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdMilestonesMilestoneIdMergeRequests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4GroupsIdMilestonesMilestoneIdIssues - errors', () => {
      it('should have a getV4GroupsIdMilestonesMilestoneIdIssues function', (done) => {
        try {
          assert.equal(true, typeof a.getV4GroupsIdMilestonesMilestoneIdIssues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4GroupsIdMilestonesMilestoneIdIssues(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdMilestonesMilestoneIdIssues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing milestonesId', (done) => {
        try {
          a.getV4GroupsIdMilestonesMilestoneIdIssues('fakeparam', null, (data, error) => {
            try {
              const displayE = 'milestonesId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdMilestonesMilestoneIdIssues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4GroupsIdMilestonesMilestoneIdBurndownEvents - errors', () => {
      it('should have a getV4GroupsIdMilestonesMilestoneIdBurndownEvents function', (done) => {
        try {
          assert.equal(true, typeof a.getV4GroupsIdMilestonesMilestoneIdBurndownEvents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4GroupsIdMilestonesMilestoneIdBurndownEvents(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdMilestonesMilestoneIdBurndownEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing milestonesId', (done) => {
        try {
          a.getV4GroupsIdMilestonesMilestoneIdBurndownEvents('fakeparam', null, (data, error) => {
            try {
              const displayE = 'milestonesId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdMilestonesMilestoneIdBurndownEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4GroupsIdEpicsEpicsIdResourceLabelEvents - errors', () => {
      it('should have a getV4GroupsIdEpicsEpicsIdResourceLabelEvents function', (done) => {
        try {
          assert.equal(true, typeof a.getV4GroupsIdEpicsEpicsIdResourceLabelEvents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4GroupsIdEpicsEpicsIdResourceLabelEvents(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdEpicsEpicsIdResourceLabelEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing epicsId', (done) => {
        try {
          a.getV4GroupsIdEpicsEpicsIdResourceLabelEvents('fakeparam', null, (data, error) => {
            try {
              const displayE = 'epicsId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdEpicsEpicsIdResourceLabelEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4GroupsIdEpicsEpicsIdResourceLabelEventsReourceLabelEventId - errors', () => {
      it('should have a getV4GroupsIdEpicsEpicsIdResourceLabelEventsReourceLabelEventId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4GroupsIdEpicsEpicsIdResourceLabelEventsReourceLabelEventId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4GroupsIdEpicsEpicsIdResourceLabelEventsReourceLabelEventId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdEpicsEpicsIdResourceLabelEventsReourceLabelEventId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing epicsId', (done) => {
        try {
          a.getV4GroupsIdEpicsEpicsIdResourceLabelEventsReourceLabelEventId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'epicsId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdEpicsEpicsIdResourceLabelEventsReourceLabelEventId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceLabelEventId', (done) => {
        try {
          a.getV4GroupsIdEpicsEpicsIdResourceLabelEventsReourceLabelEventId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'resourceLabelEventId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdEpicsEpicsIdResourceLabelEventsReourceLabelEventId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4GroupsIdSearch - errors', () => {
      it('should have a getV4GroupsIdSearch function', (done) => {
        try {
          assert.equal(true, typeof a.getV4GroupsIdSearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scope', (done) => {
        try {
          a.getV4GroupsIdSearch(null, null, null, (data, error) => {
            try {
              const displayE = 'scope is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing search', (done) => {
        try {
          a.getV4GroupsIdSearch('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'search is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4GroupsIdSearch('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4GroupsIdEpicsIdDiscussions - errors', () => {
      it('should have a getV4GroupsIdEpicsIdDiscussions function', (done) => {
        try {
          assert.equal(true, typeof a.getV4GroupsIdEpicsIdDiscussions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4GroupsIdEpicsIdDiscussions(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdEpicsIdDiscussions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing epicsId', (done) => {
        try {
          a.getV4GroupsIdEpicsIdDiscussions('fakeparam', null, (data, error) => {
            try {
              const displayE = 'epicsId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdEpicsIdDiscussions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4GroupsIdEpicsIdDiscussions - errors', () => {
      it('should have a postV4GroupsIdEpicsIdDiscussions function', (done) => {
        try {
          assert.equal(true, typeof a.postV4GroupsIdEpicsIdDiscussions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4GroupsIdEpicsIdDiscussions(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4GroupsIdEpicsIdDiscussions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing epicsId', (done) => {
        try {
          a.postV4GroupsIdEpicsIdDiscussions('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'epicsId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4GroupsIdEpicsIdDiscussions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bodyQuery', (done) => {
        try {
          a.postV4GroupsIdEpicsIdDiscussions('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'bodyQuery is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4GroupsIdEpicsIdDiscussions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4GroupsIdEpicsIdDiscussions('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4GroupsIdEpicsIdDiscussions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4GroupsIdEpicsIdDiscussionsId - errors', () => {
      it('should have a getV4GroupsIdEpicsIdDiscussionsId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4GroupsIdEpicsIdDiscussionsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4GroupsIdEpicsIdDiscussionsId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdEpicsIdDiscussionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing epicsId', (done) => {
        try {
          a.getV4GroupsIdEpicsIdDiscussionsId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'epicsId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdEpicsIdDiscussionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing discussionId', (done) => {
        try {
          a.getV4GroupsIdEpicsIdDiscussionsId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'discussionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4GroupsIdEpicsIdDiscussionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4GroupsIdEpicsIdDiscussionsIdNotes - errors', () => {
      it('should have a postV4GroupsIdEpicsIdDiscussionsIdNotes function', (done) => {
        try {
          assert.equal(true, typeof a.postV4GroupsIdEpicsIdDiscussionsIdNotes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4GroupsIdEpicsIdDiscussionsIdNotes(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4GroupsIdEpicsIdDiscussionsIdNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing epicsId', (done) => {
        try {
          a.postV4GroupsIdEpicsIdDiscussionsIdNotes('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'epicsId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4GroupsIdEpicsIdDiscussionsIdNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing discussionId', (done) => {
        try {
          a.postV4GroupsIdEpicsIdDiscussionsIdNotes('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'discussionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4GroupsIdEpicsIdDiscussionsIdNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4GroupsIdEpicsIdDiscussionsIdNotes('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4GroupsIdEpicsIdDiscussionsIdNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4GroupsIdEpicsIdDiscussionsIdNoteId - errors', () => {
      it('should have a putV4GroupsIdEpicsIdDiscussionsIdNoteId function', (done) => {
        try {
          assert.equal(true, typeof a.putV4GroupsIdEpicsIdDiscussionsIdNoteId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4GroupsIdEpicsIdDiscussionsIdNoteId(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4GroupsIdEpicsIdDiscussionsIdNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing epicsId', (done) => {
        try {
          a.putV4GroupsIdEpicsIdDiscussionsIdNoteId('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'epicsId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4GroupsIdEpicsIdDiscussionsIdNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing discussionId', (done) => {
        try {
          a.putV4GroupsIdEpicsIdDiscussionsIdNoteId('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'discussionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4GroupsIdEpicsIdDiscussionsIdNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing noteId', (done) => {
        try {
          a.putV4GroupsIdEpicsIdDiscussionsIdNoteId('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'noteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4GroupsIdEpicsIdDiscussionsIdNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bodyQuery', (done) => {
        try {
          a.putV4GroupsIdEpicsIdDiscussionsIdNoteId('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'bodyQuery is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4GroupsIdEpicsIdDiscussionsIdNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4GroupsIdEpicsIdDiscussionsIdNoteId('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4GroupsIdEpicsIdDiscussionsIdNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4GroupsIdEpicsIdDiscussionsIdNoteId - errors', () => {
      it('should have a deleteV4GroupsIdEpicsIdDiscussionsIdNoteId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4GroupsIdEpicsIdDiscussionsIdNoteId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4GroupsIdEpicsIdDiscussionsIdNoteId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4GroupsIdEpicsIdDiscussionsIdNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing epicsId', (done) => {
        try {
          a.deleteV4GroupsIdEpicsIdDiscussionsIdNoteId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'epicsId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4GroupsIdEpicsIdDiscussionsIdNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing discussionId', (done) => {
        try {
          a.deleteV4GroupsIdEpicsIdDiscussionsIdNoteId('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'discussionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4GroupsIdEpicsIdDiscussionsIdNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing noteId', (done) => {
        try {
          a.deleteV4GroupsIdEpicsIdDiscussionsIdNoteId('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'noteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4GroupsIdEpicsIdDiscussionsIdNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4Hooks - errors', () => {
      it('should have a getV4Hooks function', (done) => {
        try {
          assert.equal(true, typeof a.getV4Hooks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4Hooks - errors', () => {
      it('should have a postV4Hooks function', (done) => {
        try {
          assert.equal(true, typeof a.postV4Hooks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4Hooks(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4Hooks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4HooksId - errors', () => {
      it('should have a getV4HooksId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4HooksId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4HooksId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4HooksId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4HooksId - errors', () => {
      it('should have a deleteV4HooksId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4HooksId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4HooksId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4HooksId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4Issues - errors', () => {
      it('should have a getV4Issues function', (done) => {
        try {
          assert.equal(true, typeof a.getV4Issues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4KeysId - errors', () => {
      it('should have a getV4KeysId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4KeysId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4KeysId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4KeysId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4Namespaces - errors', () => {
      it('should have a getV4Namespaces function', (done) => {
        try {
          assert.equal(true, typeof a.getV4Namespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4NotificationSettings - errors', () => {
      it('should have a getV4NotificationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getV4NotificationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4NotificationSettings - errors', () => {
      it('should have a putV4NotificationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.putV4NotificationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4Projects - errors', () => {
      it('should have a getV4Projects function', (done) => {
        try {
          assert.equal(true, typeof a.getV4Projects === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4Projects - errors', () => {
      it('should have a postV4Projects function', (done) => {
        try {
          assert.equal(true, typeof a.postV4Projects === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4Projects(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4Projects', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsUserUserId - errors', () => {
      it('should have a postV4ProjectsUserUserId function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsUserUserId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.postV4ProjectsUserUserId(null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsUserUserId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsUserUserId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsUserUserId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsId - errors', () => {
      it('should have a getV4ProjectsId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#approveV4Project - errors', () => {
      it('should have a approveV4Project function', (done) => {
        try {
          assert.equal(true, typeof a.approveV4Project === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.approveV4Project(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-approveV4Project', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.approveV4Project('fakedata', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-approveV4Project', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsId - errors', () => {
      it('should have a putV4ProjectsId function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsId - errors', () => {
      it('should have a deleteV4ProjectsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdAccessRequests - errors', () => {
      it('should have a getV4ProjectsIdAccessRequests function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdAccessRequests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdAccessRequests(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdAccessRequests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdAccessRequests - errors', () => {
      it('should have a postV4ProjectsIdAccessRequests function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdAccessRequests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdAccessRequests(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdAccessRequests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdAccessRequestsUserId - errors', () => {
      it('should have a deleteV4ProjectsIdAccessRequestsUserId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdAccessRequestsUserId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdAccessRequestsUserId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdAccessRequestsUserId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.deleteV4ProjectsIdAccessRequestsUserId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdAccessRequestsUserId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdAccessRequestsUserIdApprove - errors', () => {
      it('should have a putV4ProjectsIdAccessRequestsUserIdApprove function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdAccessRequestsUserIdApprove === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdAccessRequestsUserIdApprove(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdAccessRequestsUserIdApprove', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.putV4ProjectsIdAccessRequestsUserIdApprove('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdAccessRequestsUserIdApprove', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdArchive - errors', () => {
      it('should have a postV4ProjectsIdArchive function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdArchive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdArchive(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdArchive', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdBoards - errors', () => {
      it('should have a getV4ProjectsIdBoards function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdBoards === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdBoards(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdBoards', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdBoardsBoardIdLists - errors', () => {
      it('should have a getV4ProjectsIdBoardsBoardIdLists function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdBoardsBoardIdLists === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdBoardsBoardIdLists(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdBoardsBoardIdLists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing boardId', (done) => {
        try {
          a.getV4ProjectsIdBoardsBoardIdLists('fakeparam', null, (data, error) => {
            try {
              const displayE = 'boardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdBoardsBoardIdLists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdBoardsBoardIdLists - errors', () => {
      it('should have a postV4ProjectsIdBoardsBoardIdLists function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdBoardsBoardIdLists === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdBoardsBoardIdLists(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdBoardsBoardIdLists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing boardId', (done) => {
        try {
          a.postV4ProjectsIdBoardsBoardIdLists('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'boardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdBoardsBoardIdLists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdBoardsBoardIdLists('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdBoardsBoardIdLists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdBoardsBoardIdListsListId - errors', () => {
      it('should have a getV4ProjectsIdBoardsBoardIdListsListId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdBoardsBoardIdListsListId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdBoardsBoardIdListsListId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdBoardsBoardIdListsListId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing boardId', (done) => {
        try {
          a.getV4ProjectsIdBoardsBoardIdListsListId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'boardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdBoardsBoardIdListsListId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing listId', (done) => {
        try {
          a.getV4ProjectsIdBoardsBoardIdListsListId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'listId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdBoardsBoardIdListsListId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdBoardsBoardIdListsListId - errors', () => {
      it('should have a putV4ProjectsIdBoardsBoardIdListsListId function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdBoardsBoardIdListsListId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdBoardsBoardIdListsListId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdBoardsBoardIdListsListId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing boardId', (done) => {
        try {
          a.putV4ProjectsIdBoardsBoardIdListsListId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'boardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdBoardsBoardIdListsListId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing listId', (done) => {
        try {
          a.putV4ProjectsIdBoardsBoardIdListsListId('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'listId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdBoardsBoardIdListsListId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdBoardsBoardIdListsListId('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdBoardsBoardIdListsListId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdBoardsBoardIdListsListId - errors', () => {
      it('should have a deleteV4ProjectsIdBoardsBoardIdListsListId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdBoardsBoardIdListsListId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdBoardsBoardIdListsListId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdBoardsBoardIdListsListId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing boardId', (done) => {
        try {
          a.deleteV4ProjectsIdBoardsBoardIdListsListId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'boardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdBoardsBoardIdListsListId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing listId', (done) => {
        try {
          a.deleteV4ProjectsIdBoardsBoardIdListsListId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'listId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdBoardsBoardIdListsListId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdDeployKeys - errors', () => {
      it('should have a getV4ProjectsIdDeployKeys function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdDeployKeys === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdDeployKeys(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdDeployKeys', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdDeployKeys - errors', () => {
      it('should have a postV4ProjectsIdDeployKeys function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdDeployKeys === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdDeployKeys(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdDeployKeys', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdDeployKeys('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdDeployKeys', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdDeployKeysKeyId - errors', () => {
      it('should have a getV4ProjectsIdDeployKeysKeyId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdDeployKeysKeyId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdDeployKeysKeyId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdDeployKeysKeyId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keyId', (done) => {
        try {
          a.getV4ProjectsIdDeployKeysKeyId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'keyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdDeployKeysKeyId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdDeployKeysKeyId - errors', () => {
      it('should have a deleteV4ProjectsIdDeployKeysKeyId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdDeployKeysKeyId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdDeployKeysKeyId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdDeployKeysKeyId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keyId', (done) => {
        try {
          a.deleteV4ProjectsIdDeployKeysKeyId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'keyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdDeployKeysKeyId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdDeployKeysKeyIdEnable - errors', () => {
      it('should have a postV4ProjectsIdDeployKeysKeyIdEnable function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdDeployKeysKeyIdEnable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdDeployKeysKeyIdEnable(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdDeployKeysKeyIdEnable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keyId', (done) => {
        try {
          a.postV4ProjectsIdDeployKeysKeyIdEnable('fakeparam', null, (data, error) => {
            try {
              const displayE = 'keyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdDeployKeysKeyIdEnable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdDeployments - errors', () => {
      it('should have a getV4ProjectsIdDeployments function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdDeployments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdDeployments(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdDeployments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdDeploymentsDeploymentId - errors', () => {
      it('should have a getV4ProjectsIdDeploymentsDeploymentId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdDeploymentsDeploymentId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdDeploymentsDeploymentId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdDeploymentsDeploymentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deploymentId', (done) => {
        try {
          a.getV4ProjectsIdDeploymentsDeploymentId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'deploymentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdDeploymentsDeploymentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdEnvironments - errors', () => {
      it('should have a getV4ProjectsIdEnvironments function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdEnvironments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdEnvironments(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdEnvironments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdEnvironments - errors', () => {
      it('should have a postV4ProjectsIdEnvironments function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdEnvironments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdEnvironments(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdEnvironments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdEnvironments('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdEnvironments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsEnvironmentsEnvironmentId - errors', () => {
      it('should have a getV4ProjectsEnvironmentsEnvironmentId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsEnvironmentsEnvironmentId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsEnvironmentsEnvironmentId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsEnvironmentsEnvironmentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing environmentId', (done) => {
        try {
          a.getV4ProjectsEnvironmentsEnvironmentId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'environmentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsEnvironmentsEnvironmentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdEnvironmentsEnvironmentId - errors', () => {
      it('should have a putV4ProjectsIdEnvironmentsEnvironmentId function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdEnvironmentsEnvironmentId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdEnvironmentsEnvironmentId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdEnvironmentsEnvironmentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing environmentId', (done) => {
        try {
          a.putV4ProjectsIdEnvironmentsEnvironmentId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'environmentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdEnvironmentsEnvironmentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdEnvironmentsEnvironmentId - errors', () => {
      it('should have a deleteV4ProjectsIdEnvironmentsEnvironmentId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdEnvironmentsEnvironmentId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdEnvironmentsEnvironmentId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdEnvironmentsEnvironmentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing environmentId', (done) => {
        try {
          a.deleteV4ProjectsIdEnvironmentsEnvironmentId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'environmentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdEnvironmentsEnvironmentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdEvents - errors', () => {
      it('should have a getV4ProjectsIdEvents function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdEvents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdEvents(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdFork - errors', () => {
      it('should have a postV4ProjectsIdFork function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdFork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdFork(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdFork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdFork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdFork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdFork - errors', () => {
      it('should have a deleteV4ProjectsIdFork function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdFork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdFork(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdFork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdForkForkedFromId - errors', () => {
      it('should have a postV4ProjectsIdForkForkedFromId function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdForkForkedFromId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdForkForkedFromId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdForkForkedFromId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing forkedFromId', (done) => {
        try {
          a.postV4ProjectsIdForkForkedFromId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'forkedFromId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdForkForkedFromId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdHooks - errors', () => {
      it('should have a getV4ProjectsIdHooks function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdHooks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdHooks(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdHooks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdHooks - errors', () => {
      it('should have a postV4ProjectsIdHooks function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdHooks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdHooks(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdHooks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdHooks('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdHooks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdHooksHookId - errors', () => {
      it('should have a getV4ProjectsIdHooksHookId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdHooksHookId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdHooksHookId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdHooksHookId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing hookId', (done) => {
        try {
          a.getV4ProjectsIdHooksHookId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'hookId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdHooksHookId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdHooksHookId - errors', () => {
      it('should have a putV4ProjectsIdHooksHookId function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdHooksHookId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdHooksHookId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdHooksHookId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing hookId', (done) => {
        try {
          a.putV4ProjectsIdHooksHookId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'hookId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdHooksHookId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdHooksHookId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdHooksHookId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdHooksHookId - errors', () => {
      it('should have a deleteV4ProjectsIdHooksHookId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdHooksHookId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdHooksHookId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdHooksHookId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing hookId', (done) => {
        try {
          a.deleteV4ProjectsIdHooksHookId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'hookId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdHooksHookId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdIssues - errors', () => {
      it('should have a getV4ProjectsIdIssues function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdIssues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdIssues(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdIssues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdIssues - errors', () => {
      it('should have a postV4ProjectsIdIssues function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdIssues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdIssues(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdIssues('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdLabels - errors', () => {
      it('should have a getV4ProjectsIdLabels function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdLabels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdLabels(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdLabels', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdLabels - errors', () => {
      it('should have a putV4ProjectsIdLabels function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdLabels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdLabels(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdLabels', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdLabels('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdLabels', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdLabels - errors', () => {
      it('should have a postV4ProjectsIdLabels function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdLabels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdLabels(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdLabels', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdLabels('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdLabels', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdLabels - errors', () => {
      it('should have a deleteV4ProjectsIdLabels function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdLabels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdLabels(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdLabels', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteV4ProjectsIdLabels('fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdLabels', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdMembers - errors', () => {
      it('should have a getV4ProjectsIdMembers function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdMembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdMembers(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdMembers - errors', () => {
      it('should have a postV4ProjectsIdMembers function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdMembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdMembers(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdMembers('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdMembersUserId - errors', () => {
      it('should have a getV4ProjectsIdMembersUserId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdMembersUserId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdMembersUserId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMembersUserId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.getV4ProjectsIdMembersUserId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMembersUserId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdMembersUserId - errors', () => {
      it('should have a putV4ProjectsIdMembersUserId function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdMembersUserId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdMembersUserId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdMembersUserId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.putV4ProjectsIdMembersUserId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdMembersUserId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdMembersUserId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdMembersUserId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdMembersUserId - errors', () => {
      it('should have a deleteV4ProjectsIdMembersUserId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdMembersUserId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdMembersUserId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdMembersUserId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.deleteV4ProjectsIdMembersUserId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdMembersUserId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdMergeRequests - errors', () => {
      it('should have a getV4ProjectsIdMergeRequests function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdMergeRequests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdMergeRequests(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdMergeRequests - errors', () => {
      it('should have a postV4ProjectsIdMergeRequests function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdMergeRequests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdMergeRequests(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdMergeRequests('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdMilestones - errors', () => {
      it('should have a getV4ProjectsIdMilestones function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdMilestones === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdMilestones(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMilestones', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdMilestones - errors', () => {
      it('should have a postV4ProjectsIdMilestones function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdMilestones === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdMilestones(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMilestones', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdMilestones('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMilestones', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdMilestonesMilestoneId - errors', () => {
      it('should have a getV4ProjectsIdMilestonesMilestoneId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdMilestonesMilestoneId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdMilestonesMilestoneId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMilestonesMilestoneId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing milestoneId', (done) => {
        try {
          a.getV4ProjectsIdMilestonesMilestoneId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'milestoneId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMilestonesMilestoneId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdMilestonesMilestoneId - errors', () => {
      it('should have a putV4ProjectsIdMilestonesMilestoneId function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdMilestonesMilestoneId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdMilestonesMilestoneId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdMilestonesMilestoneId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing milestoneId', (done) => {
        try {
          a.putV4ProjectsIdMilestonesMilestoneId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'milestoneId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdMilestonesMilestoneId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdMilestonesMilestoneId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdMilestonesMilestoneId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectIdMilestonesId - errors', () => {
      it('should have a deleteV4ProjectIdMilestonesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectIdMilestonesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectIdMilestonesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectIdMilestonesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing milestoneId', (done) => {
        try {
          a.deleteV4ProjectIdMilestonesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'milestoneId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectIdMilestonesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdMilestonesMilestoneIdIssues - errors', () => {
      it('should have a getV4ProjectsIdMilestonesMilestoneIdIssues function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdMilestonesMilestoneIdIssues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdMilestonesMilestoneIdIssues(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMilestonesMilestoneIdIssues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing milestoneId', (done) => {
        try {
          a.getV4ProjectsIdMilestonesMilestoneIdIssues('fakeparam', null, (data, error) => {
            try {
              const displayE = 'milestoneId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMilestonesMilestoneIdIssues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdNotificationSettings - errors', () => {
      it('should have a getV4ProjectsIdNotificationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdNotificationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdNotificationSettings(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdNotificationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdNotificationSettings - errors', () => {
      it('should have a putV4ProjectsIdNotificationSettings function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdNotificationSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdNotificationSettings(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdNotificationSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdPipeline - errors', () => {
      it('should have a postV4ProjectsIdPipeline function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdPipeline === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdPipeline(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdPipeline', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdPipeline('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdPipeline', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdPipelines - errors', () => {
      it('should have a getV4ProjectsIdPipelines function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdPipelines === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdPipelines(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdPipelines', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdPipelinesPipelineId - errors', () => {
      it('should have a getV4ProjectsIdPipelinesPipelineId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdPipelinesPipelineId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdPipelinesPipelineId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdPipelinesPipelineId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pipelineId', (done) => {
        try {
          a.getV4ProjectsIdPipelinesPipelineId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'pipelineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdPipelinesPipelineId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdPipelinesPipelineIdCancel - errors', () => {
      it('should have a postV4ProjectsIdPipelinesPipelineIdCancel function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdPipelinesPipelineIdCancel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdPipelinesPipelineIdCancel(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdPipelinesPipelineIdCancel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pipelineId', (done) => {
        try {
          a.postV4ProjectsIdPipelinesPipelineIdCancel('fakeparam', null, (data, error) => {
            try {
              const displayE = 'pipelineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdPipelinesPipelineIdCancel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdPipelinesPipelineIdRetry - errors', () => {
      it('should have a postV4ProjectsIdPipelinesPipelineIdRetry function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdPipelinesPipelineIdRetry === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdPipelinesPipelineIdRetry(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdPipelinesPipelineIdRetry', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pipelineId', (done) => {
        try {
          a.postV4ProjectsIdPipelinesPipelineIdRetry('fakeparam', null, (data, error) => {
            try {
              const displayE = 'pipelineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdPipelinesPipelineIdRetry', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdRepositoryArchive - errors', () => {
      it('should have a getV4ProjectsIdRepositoryArchive function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdRepositoryArchive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdRepositoryArchive(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRepositoryArchive', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdRepositoryBlobsSha - errors', () => {
      it('should have a getV4ProjectsIdRepositoryBlobsSha function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdRepositoryBlobsSha === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdRepositoryBlobsSha(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRepositoryBlobsSha', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sha', (done) => {
        try {
          a.getV4ProjectsIdRepositoryBlobsSha('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sha is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRepositoryBlobsSha', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filepath', (done) => {
        try {
          a.getV4ProjectsIdRepositoryBlobsSha('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'filepath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRepositoryBlobsSha', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdRepositoryBranches - errors', () => {
      it('should have a getV4ProjectsIdRepositoryBranches function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdRepositoryBranches === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdRepositoryBranches(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRepositoryBranches', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdRepositoryBranches - errors', () => {
      it('should have a postV4ProjectsIdRepositoryBranches function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdRepositoryBranches === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdRepositoryBranches(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdRepositoryBranches', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdRepositoryBranches('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdRepositoryBranches', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdRepositoryBranchesBranch - errors', () => {
      it('should have a getV4ProjectsIdRepositoryBranchesBranch function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdRepositoryBranchesBranch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdRepositoryBranchesBranch(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRepositoryBranchesBranch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing branch', (done) => {
        try {
          a.getV4ProjectsIdRepositoryBranchesBranch('fakeparam', null, (data, error) => {
            try {
              const displayE = 'branch is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRepositoryBranchesBranch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdRepositoryBranchesBranch - errors', () => {
      it('should have a deleteV4ProjectsIdRepositoryBranchesBranch function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdRepositoryBranchesBranch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdRepositoryBranchesBranch(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdRepositoryBranchesBranch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing branch', (done) => {
        try {
          a.deleteV4ProjectsIdRepositoryBranchesBranch('fakeparam', null, (data, error) => {
            try {
              const displayE = 'branch is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdRepositoryBranchesBranch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdRepositoryBranchesBranchProtect - errors', () => {
      it('should have a putV4ProjectsIdRepositoryBranchesBranchProtect function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdRepositoryBranchesBranchProtect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdRepositoryBranchesBranchProtect(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdRepositoryBranchesBranchProtect', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing branch', (done) => {
        try {
          a.putV4ProjectsIdRepositoryBranchesBranchProtect('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'branch is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdRepositoryBranchesBranchProtect', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdRepositoryBranchesBranchUnprotect - errors', () => {
      it('should have a putV4ProjectsIdRepositoryBranchesBranchUnprotect function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdRepositoryBranchesBranchUnprotect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdRepositoryBranchesBranchUnprotect(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdRepositoryBranchesBranchUnprotect', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing branch', (done) => {
        try {
          a.putV4ProjectsIdRepositoryBranchesBranchUnprotect('fakeparam', null, (data, error) => {
            try {
              const displayE = 'branch is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdRepositoryBranchesBranchUnprotect', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdRepositoryCommits - errors', () => {
      it('should have a getV4ProjectsIdRepositoryCommits function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdRepositoryCommits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdRepositoryCommits(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRepositoryCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdRepositoryCommits - errors', () => {
      it('should have a postV4ProjectsIdRepositoryCommits function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdRepositoryCommits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdRepositoryCommits(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdRepositoryCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdRepositoryCommits('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdRepositoryCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdRepositoryCommitsSha - errors', () => {
      it('should have a getV4ProjectsIdRepositoryCommitsSha function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdRepositoryCommitsSha === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdRepositoryCommitsSha(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRepositoryCommitsSha', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sha', (done) => {
        try {
          a.getV4ProjectsIdRepositoryCommitsSha('fakeparam', null, (data, error) => {
            try {
              const displayE = 'sha is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRepositoryCommitsSha', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdRepositoryCommitsShaCherryPick - errors', () => {
      it('should have a postV4ProjectsIdRepositoryCommitsShaCherryPick function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdRepositoryCommitsShaCherryPick === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdRepositoryCommitsShaCherryPick(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdRepositoryCommitsShaCherryPick', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sha', (done) => {
        try {
          a.postV4ProjectsIdRepositoryCommitsShaCherryPick('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sha is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdRepositoryCommitsShaCherryPick', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdRepositoryCommitsShaCherryPick('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdRepositoryCommitsShaCherryPick', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdRepositoryCommitsShaComments - errors', () => {
      it('should have a getV4ProjectsIdRepositoryCommitsShaComments function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdRepositoryCommitsShaComments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdRepositoryCommitsShaComments(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRepositoryCommitsShaComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sha', (done) => {
        try {
          a.getV4ProjectsIdRepositoryCommitsShaComments('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'sha is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRepositoryCommitsShaComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdRepositoryCommitsShaComments - errors', () => {
      it('should have a postV4ProjectsIdRepositoryCommitsShaComments function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdRepositoryCommitsShaComments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdRepositoryCommitsShaComments(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdRepositoryCommitsShaComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sha', (done) => {
        try {
          a.postV4ProjectsIdRepositoryCommitsShaComments('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sha is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdRepositoryCommitsShaComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdRepositoryCommitsShaComments('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdRepositoryCommitsShaComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdRepositoryCommitsShaDiff - errors', () => {
      it('should have a getV4ProjectsIdRepositoryCommitsShaDiff function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdRepositoryCommitsShaDiff === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdRepositoryCommitsShaDiff(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRepositoryCommitsShaDiff', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sha', (done) => {
        try {
          a.getV4ProjectsIdRepositoryCommitsShaDiff('fakeparam', null, (data, error) => {
            try {
              const displayE = 'sha is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRepositoryCommitsShaDiff', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdRepositoryCommitsShaStatuses - errors', () => {
      it('should have a getV4ProjectsIdRepositoryCommitsShaStatuses function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdRepositoryCommitsShaStatuses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdRepositoryCommitsShaStatuses(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRepositoryCommitsShaStatuses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sha', (done) => {
        try {
          a.getV4ProjectsIdRepositoryCommitsShaStatuses('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'sha is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRepositoryCommitsShaStatuses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdRepositoryCompare - errors', () => {
      it('should have a getV4ProjectsIdRepositoryCompare function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdRepositoryCompare === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdRepositoryCompare(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRepositoryCompare', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing from', (done) => {
        try {
          a.getV4ProjectsIdRepositoryCompare('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'from is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRepositoryCompare', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing to', (done) => {
        try {
          a.getV4ProjectsIdRepositoryCompare('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'to is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRepositoryCompare', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdRepositoryContributors - errors', () => {
      it('should have a getV4ProjectsIdRepositoryContributors function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdRepositoryContributors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdRepositoryContributors(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRepositoryContributors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdRepositoryRawBlobsSha - errors', () => {
      it('should have a getV4ProjectsIdRepositoryRawBlobsSha function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdRepositoryRawBlobsSha === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdRepositoryRawBlobsSha(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRepositoryRawBlobsSha', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sha', (done) => {
        try {
          a.getV4ProjectsIdRepositoryRawBlobsSha('fakeparam', null, (data, error) => {
            try {
              const displayE = 'sha is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRepositoryRawBlobsSha', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdRepositoryTags - errors', () => {
      it('should have a getV4ProjectsIdRepositoryTags function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdRepositoryTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdRepositoryTags(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRepositoryTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdRepositoryTags - errors', () => {
      it('should have a postV4ProjectsIdRepositoryTags function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdRepositoryTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdRepositoryTags(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdRepositoryTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdRepositoryTags('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdRepositoryTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdRepositoryTagsTagName - errors', () => {
      it('should have a getV4ProjectsIdRepositoryTagsTagName function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdRepositoryTagsTagName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdRepositoryTagsTagName(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRepositoryTagsTagName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tagName', (done) => {
        try {
          a.getV4ProjectsIdRepositoryTagsTagName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'tagName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRepositoryTagsTagName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdRepositoryTagsTagName - errors', () => {
      it('should have a deleteV4ProjectsIdRepositoryTagsTagName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdRepositoryTagsTagName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdRepositoryTagsTagName(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdRepositoryTagsTagName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tagName', (done) => {
        try {
          a.deleteV4ProjectsIdRepositoryTagsTagName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'tagName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdRepositoryTagsTagName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdRepositoryTagsTagNameRelease - errors', () => {
      it('should have a putV4ProjectsIdRepositoryTagsTagNameRelease function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdRepositoryTagsTagNameRelease === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdRepositoryTagsTagNameRelease(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdRepositoryTagsTagNameRelease', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tagName', (done) => {
        try {
          a.putV4ProjectsIdRepositoryTagsTagNameRelease('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'tagName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdRepositoryTagsTagNameRelease', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdRepositoryTagsTagNameRelease('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdRepositoryTagsTagNameRelease', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdRepositoryTagsTagNameRelease - errors', () => {
      it('should have a postV4ProjectsIdRepositoryTagsTagNameRelease function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdRepositoryTagsTagNameRelease === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdRepositoryTagsTagNameRelease(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdRepositoryTagsTagNameRelease', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tagName', (done) => {
        try {
          a.postV4ProjectsIdRepositoryTagsTagNameRelease('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'tagName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdRepositoryTagsTagNameRelease', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdRepositoryTagsTagNameRelease('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdRepositoryTagsTagNameRelease', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdRepositoryTree - errors', () => {
      it('should have a getV4ProjectsIdRepositoryTree function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdRepositoryTree === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdRepositoryTree(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRepositoryTree', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdRunners - errors', () => {
      it('should have a getV4ProjectsIdRunners function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdRunners === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdRunners(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRunners', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdRunners - errors', () => {
      it('should have a postV4ProjectsIdRunners function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdRunners === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdRunners(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdRunners', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdRunners('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdRunners', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdRunnersRunnerId - errors', () => {
      it('should have a deleteV4ProjectsIdRunnersRunnerId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdRunnersRunnerId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdRunnersRunnerId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdRunnersRunnerId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing runnerId', (done) => {
        try {
          a.deleteV4ProjectsIdRunnersRunnerId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'runnerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdRunnersRunnerId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdServicesAsana - errors', () => {
      it('should have a getV4ProjectsIdServicesAsana function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdServicesAsana === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdServicesAsana(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdServicesAsana', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdServicesAsana - errors', () => {
      it('should have a putV4ProjectsIdServicesAsana function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdServicesAsana === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdServicesAsana(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesAsana', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdServicesAsana('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesAsana', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdServicesAsana - errors', () => {
      it('should have a deleteV4ProjectsIdServicesAsana function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdServicesAsana === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdServicesAsana(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdServicesAsana', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdServicesAssembla - errors', () => {
      it('should have a getV4ProjectsIdServicesAssembla function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdServicesAssembla === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdServicesAssembla(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdServicesAssembla', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdServicesAssembla - errors', () => {
      it('should have a putV4ProjectsIdServicesAssembla function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdServicesAssembla === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdServicesAssembla(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesAssembla', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdServicesAssembla('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesAssembla', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdServicesAssembla - errors', () => {
      it('should have a deleteV4ProjectsIdServicesAssembla function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdServicesAssembla === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdServicesAssembla(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdServicesAssembla', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdServicesBamboo - errors', () => {
      it('should have a getV4ProjectsIdServicesBamboo function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdServicesBamboo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdServicesBamboo(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdServicesBamboo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdServicesBamboo - errors', () => {
      it('should have a putV4ProjectsIdServicesBamboo function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdServicesBamboo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdServicesBamboo(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesBamboo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdServicesBamboo('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesBamboo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdServicesBamboo - errors', () => {
      it('should have a deleteV4ProjectsIdServicesBamboo function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdServicesBamboo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdServicesBamboo(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdServicesBamboo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdServicesBugzilla - errors', () => {
      it('should have a getV4ProjectsIdServicesBugzilla function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdServicesBugzilla === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdServicesBugzilla(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdServicesBugzilla', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdServicesBugzilla - errors', () => {
      it('should have a putV4ProjectsIdServicesBugzilla function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdServicesBugzilla === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdServicesBugzilla(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesBugzilla', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdServicesBugzilla('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesBugzilla', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdServicesBugzilla - errors', () => {
      it('should have a deleteV4ProjectsIdServicesBugzilla function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdServicesBugzilla === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdServicesBugzilla(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdServicesBugzilla', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdServicesBuildkite - errors', () => {
      it('should have a getV4ProjectsIdServicesBuildkite function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdServicesBuildkite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdServicesBuildkite(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdServicesBuildkite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdServicesBuildkite - errors', () => {
      it('should have a putV4ProjectsIdServicesBuildkite function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdServicesBuildkite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdServicesBuildkite(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesBuildkite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdServicesBuildkite('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesBuildkite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdServicesBuildkite - errors', () => {
      it('should have a deleteV4ProjectsIdServicesBuildkite function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdServicesBuildkite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdServicesBuildkite(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdServicesBuildkite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdServicesBuildsEmail - errors', () => {
      it('should have a putV4ProjectsIdServicesBuildsEmail function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdServicesBuildsEmail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdServicesBuildsEmail(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesBuildsEmail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdServicesBuildsEmail('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesBuildsEmail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdServicesCampfire - errors', () => {
      it('should have a getV4ProjectsIdServicesCampfire function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdServicesCampfire === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdServicesCampfire(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdServicesCampfire', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdServicesCampfire - errors', () => {
      it('should have a putV4ProjectsIdServicesCampfire function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdServicesCampfire === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdServicesCampfire(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesCampfire', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdServicesCampfire('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesCampfire', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdServicesCampfire - errors', () => {
      it('should have a deleteV4ProjectsIdServicesCampfire function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdServicesCampfire === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdServicesCampfire(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdServicesCampfire', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdServicesCustomIssueTracker - errors', () => {
      it('should have a getV4ProjectsIdServicesCustomIssueTracker function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdServicesCustomIssueTracker === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdServicesCustomIssueTracker(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdServicesCustomIssueTracker', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdServicesCustomIssueTracker - errors', () => {
      it('should have a putV4ProjectsIdServicesCustomIssueTracker function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdServicesCustomIssueTracker === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdServicesCustomIssueTracker(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesCustomIssueTracker', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdServicesCustomIssueTracker('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesCustomIssueTracker', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsServicesCustomIssueTracker - errors', () => {
      it('should have a deleteV4ProjectsServicesCustomIssueTracker function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsServicesCustomIssueTracker === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsServicesCustomIssueTracker(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsServicesCustomIssueTracker', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdServicesDroneCi - errors', () => {
      it('should have a getV4ProjectsIdServicesDroneCi function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdServicesDroneCi === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdServicesDroneCi(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdServicesDroneCi', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdServicesDroneCi - errors', () => {
      it('should have a putV4ProjectsIdServicesDroneCi function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdServicesDroneCi === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdServicesDroneCi(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesDroneCi', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdServicesDroneCi('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesDroneCi', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdServicesDroneCi - errors', () => {
      it('should have a deleteV4ProjectsIdServicesDroneCi function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdServicesDroneCi === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdServicesDroneCi(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdServicesDroneCi', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdServicesEmailOnPush - errors', () => {
      it('should have a getV4ProjectsIdServicesEmailOnPush function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdServicesEmailOnPush === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdServicesEmailOnPush(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdServicesEmailOnPush', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdServicesEmailsOnPush - errors', () => {
      it('should have a putV4ProjectsIdServicesEmailsOnPush function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdServicesEmailsOnPush === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdServicesEmailsOnPush(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesEmailsOnPush', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdServicesEmailsOnPush('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesEmailsOnPush', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdServicesEmailOnPush - errors', () => {
      it('should have a deleteV4ProjectsIdServicesEmailOnPush function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdServicesEmailOnPush === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdServicesEmailOnPush(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdServicesEmailOnPush', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdServicesExternalWiki - errors', () => {
      it('should have a getV4ProjectsIdServicesExternalWiki function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdServicesExternalWiki === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdServicesExternalWiki(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdServicesExternalWiki', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdServicesExternalWiki - errors', () => {
      it('should have a putV4ProjectsIdServicesExternalWiki function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdServicesExternalWiki === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdServicesExternalWiki(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesExternalWiki', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdServicesExternalWiki('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesExternalWiki', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdServicesExternalWiki - errors', () => {
      it('should have a deleteV4ProjectsIdServicesExternalWiki function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdServicesExternalWiki === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdServicesExternalWiki(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdServicesExternalWiki', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdServicesFlowdock - errors', () => {
      it('should have a getV4ProjectsIdServicesFlowdock function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdServicesFlowdock === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdServicesFlowdock(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdServicesFlowdock', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdServicesFlowdock - errors', () => {
      it('should have a putV4ProjectsIdServicesFlowdock function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdServicesFlowdock === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdServicesFlowdock(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesFlowdock', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdServicesFlowdock('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesFlowdock', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsServicesFlowdock - errors', () => {
      it('should have a deleteV4ProjectsServicesFlowdock function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsServicesFlowdock === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsServicesFlowdock(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsServicesFlowdock', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdServicesHipchat - errors', () => {
      it('should have a getV4ProjectsIdServicesHipchat function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdServicesHipchat === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdServicesHipchat(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdServicesHipchat', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdServicesHipchat - errors', () => {
      it('should have a putV4ProjectsIdServicesHipchat function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdServicesHipchat === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdServicesHipchat(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesHipchat', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdServicesHipchat('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesHipchat', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdServicesHipchat - errors', () => {
      it('should have a deleteV4ProjectsIdServicesHipchat function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdServicesHipchat === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdServicesHipchat(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdServicesHipchat', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdServicesIrker - errors', () => {
      it('should have a getV4ProjectsIdServicesIrker function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdServicesIrker === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdServicesIrker(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdServicesIrker', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdServicesIrker - errors', () => {
      it('should have a putV4ProjectsIdServicesIrker function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdServicesIrker === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdServicesIrker(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesIrker', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdServicesIrker('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesIrker', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdServicesIrker - errors', () => {
      it('should have a deleteV4ProjectsIdServicesIrker function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdServicesIrker === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdServicesIrker(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdServicesIrker', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdServicesJira - errors', () => {
      it('should have a getV4ProjectsIdServicesJira function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdServicesJira === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdServicesJira(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdServicesJira', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdServicesJira - errors', () => {
      it('should have a putV4ProjectsIdServicesJira function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdServicesJira === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdServicesJira(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesJira', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdServicesJira('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesJira', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdServicesJira - errors', () => {
      it('should have a deleteV4ProjectsIdServicesJira function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdServicesJira === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdServicesJira(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdServicesJira', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdServicesKubernetes - errors', () => {
      it('should have a getV4ProjectsIdServicesKubernetes function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdServicesKubernetes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdServicesKubernetes(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdServicesKubernetes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdServicesKubernetes - errors', () => {
      it('should have a putV4ProjectsIdServicesKubernetes function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdServicesKubernetes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdServicesKubernetes(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesKubernetes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdServicesKubernetes('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesKubernetes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdServicesKubernetes - errors', () => {
      it('should have a deleteV4ProjectsIdServicesKubernetes function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdServicesKubernetes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdServicesKubernetes(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdServicesKubernetes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdServicesMattermost - errors', () => {
      it('should have a getV4ProjectsIdServicesMattermost function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdServicesMattermost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdServicesMattermost(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdServicesMattermost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdServicesMattermost - errors', () => {
      it('should have a putV4ProjectsIdServicesMattermost function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdServicesMattermost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdServicesMattermost(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesMattermost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdServicesMattermost('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesMattermost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdServicesMattermost - errors', () => {
      it('should have a deleteV4ProjectsIdServicesMattermost function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdServicesMattermost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdServicesMattermost(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdServicesMattermost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdServicesMattermostSlashCommands - errors', () => {
      it('should have a getV4ProjectsIdServicesMattermostSlashCommands function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdServicesMattermostSlashCommands === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdServicesMattermostSlashCommands(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdServicesMattermostSlashCommands', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdServicesMattermostSlashCommands - errors', () => {
      it('should have a putV4ProjectsIdServicesMattermostSlashCommands function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdServicesMattermostSlashCommands === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdServicesMattermostSlashCommands(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesMattermostSlashCommands', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdServicesMattermostSlashCommands('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesMattermostSlashCommands', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdServicesMattermostSlashCommands - errors', () => {
      it('should have a deleteV4ProjectsIdServicesMattermostSlashCommands function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdServicesMattermostSlashCommands === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdServicesMattermostSlashCommands(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdServicesMattermostSlashCommands', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdServicesPipelinesEmail - errors', () => {
      it('should have a getV4ProjectsIdServicesPipelinesEmail function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdServicesPipelinesEmail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdServicesPipelinesEmail(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdServicesPipelinesEmail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdServicesPipelinesEmail - errors', () => {
      it('should have a putV4ProjectsIdServicesPipelinesEmail function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdServicesPipelinesEmail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdServicesPipelinesEmail(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesPipelinesEmail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdServicesPipelinesEmail('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesPipelinesEmail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdServicesPipelinesEmail - errors', () => {
      it('should have a deleteV4ProjectsIdServicesPipelinesEmail function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdServicesPipelinesEmail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdServicesPipelinesEmail(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdServicesPipelinesEmail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdServicesPivotaltracker - errors', () => {
      it('should have a getV4ProjectsIdServicesPivotaltracker function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdServicesPivotaltracker === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdServicesPivotaltracker(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdServicesPivotaltracker', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdServicesPivotaltracker - errors', () => {
      it('should have a putV4ProjectsIdServicesPivotaltracker function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdServicesPivotaltracker === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdServicesPivotaltracker(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesPivotaltracker', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdServicesPivotaltracker('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesPivotaltracker', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdServicesPivotaltracker - errors', () => {
      it('should have a deleteV4ProjectsIdServicesPivotaltracker function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdServicesPivotaltracker === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdServicesPivotaltracker(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdServicesPivotaltracker', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdServicesPushover - errors', () => {
      it('should have a getV4ProjectsIdServicesPushover function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdServicesPushover === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdServicesPushover(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdServicesPushover', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdServicesPushover - errors', () => {
      it('should have a putV4ProjectsIdServicesPushover function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdServicesPushover === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdServicesPushover(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesPushover', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdServicesPushover('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesPushover', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdServicesPushover - errors', () => {
      it('should have a deleteV4ProjectsIdServicesPushover function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdServicesPushover === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdServicesPushover(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdServicesPushover', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdServicesRedmine - errors', () => {
      it('should have a getV4ProjectsIdServicesRedmine function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdServicesRedmine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdServicesRedmine(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdServicesRedmine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdServicesRedmine - errors', () => {
      it('should have a putV4ProjectsIdServicesRedmine function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdServicesRedmine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdServicesRedmine(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesRedmine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdServicesRedmine('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesRedmine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdServicesRedmine - errors', () => {
      it('should have a deleteV4ProjectsIdServicesRedmine function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdServicesRedmine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdServicesRedmine(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdServicesRedmine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdServicesSlack - errors', () => {
      it('should have a getV4ProjectsIdServicesSlack function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdServicesSlack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdServicesSlack(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdServicesSlack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdServicesSlack - errors', () => {
      it('should have a putV4ProjectsIdServicesSlack function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdServicesSlack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdServicesSlack(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesSlack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdServicesSlack('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesSlack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdServicesSlack - errors', () => {
      it('should have a deleteV4ProjectsIdServicesSlack function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdServicesSlack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdServicesSlack(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdServicesSlack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdServicesSlackSlashCommands - errors', () => {
      it('should have a getV4ProjectsIdServicesSlackSlashCommands function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdServicesSlackSlashCommands === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdServicesSlackSlashCommands(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdServicesSlackSlashCommands', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdServicesSlackSlashCommands - errors', () => {
      it('should have a putV4ProjectsIdServicesSlackSlashCommands function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdServicesSlackSlashCommands === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdServicesSlackSlashCommands(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesSlackSlashCommands', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdServicesSlackSlashCommands('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesSlackSlashCommands', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdServicesSlackSlashCommands - errors', () => {
      it('should have a deleteV4ProjectsIdServicesSlackSlashCommands function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdServicesSlackSlashCommands === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdServicesSlackSlashCommands(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdServicesSlackSlashCommands', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdServicesTeamcity - errors', () => {
      it('should have a getV4ProjectsIdServicesTeamcity function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdServicesTeamcity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdServicesTeamcity(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdServicesTeamcity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdServicesTeamcity - errors', () => {
      it('should have a putV4ProjectsIdServicesTeamcity function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdServicesTeamcity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdServicesTeamcity(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesTeamcity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdServicesTeamcity('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesTeamcity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdServicesTeamcity - errors', () => {
      it('should have a deleteV4ProjectsIdServicesTeamcity function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdServicesTeamcity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdServicesTeamcity(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdServicesTeamcity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdShare - errors', () => {
      it('should have a postV4ProjectsIdShare function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdShare === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdShare(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdShare', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdShare('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdShare', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdShareGroupId - errors', () => {
      it('should have a deleteV4ProjectsIdShareGroupId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdShareGroupId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdShareGroupId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdShareGroupId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.deleteV4ProjectsIdShareGroupId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdShareGroupId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdSnippets - errors', () => {
      it('should have a getV4ProjectsIdSnippets function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdSnippets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdSnippets(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdSnippets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdSnippets - errors', () => {
      it('should have a postV4ProjectsIdSnippets function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdSnippets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdSnippets(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdSnippets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdSnippets('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdSnippets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdSnippetsSnippetId - errors', () => {
      it('should have a getV4ProjectsIdSnippetsSnippetId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdSnippetsSnippetId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdSnippetsSnippetId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdSnippetsSnippetId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snippetId', (done) => {
        try {
          a.getV4ProjectsIdSnippetsSnippetId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'snippetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdSnippetsSnippetId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdSnippetsSnippetId - errors', () => {
      it('should have a putV4ProjectsIdSnippetsSnippetId function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdSnippetsSnippetId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdSnippetsSnippetId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdSnippetsSnippetId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snippetId', (done) => {
        try {
          a.putV4ProjectsIdSnippetsSnippetId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'snippetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdSnippetsSnippetId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdSnippetsSnippetId - errors', () => {
      it('should have a deleteV4ProjectsIdSnippetsSnippetId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdSnippetsSnippetId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdSnippetsSnippetId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdSnippetsSnippetId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snippetId', (done) => {
        try {
          a.deleteV4ProjectsIdSnippetsSnippetId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'snippetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdSnippetsSnippetId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdSnippetsSnippetIdAwardEmoji - errors', () => {
      it('should have a getV4ProjectsIdSnippetsSnippetIdAwardEmoji function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdSnippetsSnippetIdAwardEmoji === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdSnippetsSnippetIdAwardEmoji(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdSnippetsSnippetIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snippetId', (done) => {
        try {
          a.getV4ProjectsIdSnippetsSnippetIdAwardEmoji('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'snippetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdSnippetsSnippetIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdSnippetsSnippetIdAwardEmoji - errors', () => {
      it('should have a postV4ProjectsIdSnippetsSnippetIdAwardEmoji function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdSnippetsSnippetIdAwardEmoji === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdSnippetsSnippetIdAwardEmoji(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdSnippetsSnippetIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snippetId', (done) => {
        try {
          a.postV4ProjectsIdSnippetsSnippetIdAwardEmoji('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'snippetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdSnippetsSnippetIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdSnippetsSnippetIdAwardEmoji('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdSnippetsSnippetIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdSnippetsSnippetIdAwardEmojiAwardId - errors', () => {
      it('should have a getV4ProjectsIdSnippetsSnippetIdAwardEmojiAwardId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdSnippetsSnippetIdAwardEmojiAwardId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing awardId', (done) => {
        try {
          a.getV4ProjectsIdSnippetsSnippetIdAwardEmojiAwardId(null, null, null, (data, error) => {
            try {
              const displayE = 'awardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdSnippetsSnippetIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdSnippetsSnippetIdAwardEmojiAwardId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdSnippetsSnippetIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snippetId', (done) => {
        try {
          a.getV4ProjectsIdSnippetsSnippetIdAwardEmojiAwardId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'snippetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdSnippetsSnippetIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdSnippetsSnippetIdAwardEmojiAwardId - errors', () => {
      it('should have a deleteV4ProjectsIdSnippetsSnippetIdAwardEmojiAwardId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdSnippetsSnippetIdAwardEmojiAwardId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing awardId', (done) => {
        try {
          a.deleteV4ProjectsIdSnippetsSnippetIdAwardEmojiAwardId(null, null, null, (data, error) => {
            try {
              const displayE = 'awardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdSnippetsSnippetIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdSnippetsSnippetIdAwardEmojiAwardId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdSnippetsSnippetIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snippetId', (done) => {
        try {
          a.deleteV4ProjectsIdSnippetsSnippetIdAwardEmojiAwardId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'snippetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdSnippetsSnippetIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmoji - errors', () => {
      it('should have a getV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmoji function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmoji === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmoji(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snippetId', (done) => {
        try {
          a.getV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmoji('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'snippetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing noteId', (done) => {
        try {
          a.getV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmoji('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'noteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmoji - errors', () => {
      it('should have a postV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmoji function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmoji === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmoji(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snippetId', (done) => {
        try {
          a.postV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmoji('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'snippetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing noteId', (done) => {
        try {
          a.postV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmoji('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'noteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmoji('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmojiAwardId - errors', () => {
      it('should have a getV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmojiAwardId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmojiAwardId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing awardId', (done) => {
        try {
          a.getV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmojiAwardId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'awardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmojiAwardId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snippetId', (done) => {
        try {
          a.getV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmojiAwardId('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'snippetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing noteId', (done) => {
        try {
          a.getV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmojiAwardId('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'noteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmojiAwardId - errors', () => {
      it('should have a deleteV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmojiAwardId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmojiAwardId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing awardId', (done) => {
        try {
          a.deleteV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmojiAwardId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'awardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmojiAwardId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snippetId', (done) => {
        try {
          a.deleteV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmojiAwardId('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'snippetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing noteId', (done) => {
        try {
          a.deleteV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmojiAwardId('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'noteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdSnippetsSnippetIdRaw - errors', () => {
      it('should have a getV4ProjectsIdSnippetsSnippetIdRaw function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdSnippetsSnippetIdRaw === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdSnippetsSnippetIdRaw(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdSnippetsSnippetIdRaw', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snippetId', (done) => {
        try {
          a.getV4ProjectsIdSnippetsSnippetIdRaw('fakeparam', null, (data, error) => {
            try {
              const displayE = 'snippetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdSnippetsSnippetIdRaw', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdStar - errors', () => {
      it('should have a postV4ProjectsIdStar function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdStar === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdStar(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdStar', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdStatusesSha - errors', () => {
      it('should have a postV4ProjectsIdStatusesSha function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdStatusesSha === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdStatusesSha(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdStatusesSha', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sha', (done) => {
        try {
          a.postV4ProjectsIdStatusesSha('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sha is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdStatusesSha', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdStatusesSha('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdStatusesSha', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdTriggers - errors', () => {
      it('should have a getV4ProjectsIdTriggers function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdTriggers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdTriggers(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdTriggers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdTriggers - errors', () => {
      it('should have a postV4ProjectsIdTriggers function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdTriggers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdTriggers(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdTriggers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdUnarchive - errors', () => {
      it('should have a postV4ProjectsIdUnarchive function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdUnarchive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdUnarchive(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdUnarchive', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdUploads - errors', () => {
      it('should have a postV4ProjectsIdUploads function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdUploads === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdUploads(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdUploads', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdUsers - errors', () => {
      it('should have a getV4ProjectsIdUsers function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdUsers(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdVariables - errors', () => {
      it('should have a getV4ProjectsIdVariables function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdVariables === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdVariables(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdVariables', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdVariables - errors', () => {
      it('should have a postV4ProjectsIdVariables function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdVariables === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdVariables(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdVariables', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdVariables('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdVariables', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdVariablesKey - errors', () => {
      it('should have a getV4ProjectsIdVariablesKey function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdVariablesKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdVariablesKey(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdVariablesKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.getV4ProjectsIdVariablesKey('fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdVariablesKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdVariablesKey - errors', () => {
      it('should have a putV4ProjectsIdVariablesKey function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdVariablesKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdVariablesKey(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdVariablesKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.putV4ProjectsIdVariablesKey('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdVariablesKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdVariablesKey - errors', () => {
      it('should have a deleteV4ProjectsIdVariablesKey function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdVariablesKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdVariablesKey(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdVariablesKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.deleteV4ProjectsIdVariablesKey('fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdVariablesKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdForks - errors', () => {
      it('should have a getV4ProjectsIdForks function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdForks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdForks('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdForks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdUnstar - errors', () => {
      it('should have a postV4ProjectsIdUnstar function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdUnstar === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdUnstar(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdUnstar', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdLanguages - errors', () => {
      it('should have a getV4ProjectsIdLanguages function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdLanguages === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdLanguages(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdLanguages', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdPushRule - errors', () => {
      it('should have a getV4ProjectsIdPushRule function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdPushRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdPushRule(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdPushRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdPushRule - errors', () => {
      it('should have a putV4ProjectsIdPushRule function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdPushRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdPushRule(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdPushRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdPushRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdPushRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4PorjectsIdPushRule - errors', () => {
      it('should have a postV4PorjectsIdPushRule function', (done) => {
        try {
          assert.equal(true, typeof a.postV4PorjectsIdPushRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4PorjectsIdPushRule(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4PorjectsIdPushRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4PorjectsIdPushRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4PorjectsIdPushRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#delelteV4ProjectsIdPushRule - errors', () => {
      it('should have a delelteV4ProjectsIdPushRule function', (done) => {
        try {
          assert.equal(true, typeof a.delelteV4ProjectsIdPushRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.delelteV4ProjectsIdPushRule(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-delelteV4ProjectsIdPushRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdExport - errors', () => {
      it('should have a getV4ProjectsIdExport function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdExport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdExport(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdExport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdExport - errors', () => {
      it('should have a postV4ProjectsIdExport function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdExport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdExport(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdExport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdExport('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdExport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdExportDownload - errors', () => {
      it('should have a getV4ProjectsIdExportDownload function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdExportDownload === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdExportDownload(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdExportDownload', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdImport - errors', () => {
      it('should have a postV4ProjectsIdImport function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdImport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdImport - errors', () => {
      it('should have a getV4ProjectsIdImport function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdImport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdImport(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdImport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdBadges - errors', () => {
      it('should have a getV4ProjectsIdBadges function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdBadges === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdBadges(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdBadges', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdBadges - errors', () => {
      it('should have a postV4ProjectsIdBadges function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdBadges === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdBadges(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdBadges', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdBadgesBadgeId - errors', () => {
      it('should have a getV4ProjectsIdBadgesBadgeId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdBadgesBadgeId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdBadgesBadgeId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdBadgesBadgeId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing badgeId', (done) => {
        try {
          a.getV4ProjectsIdBadgesBadgeId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'badgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdBadgesBadgeId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdBadgesBadgeId - errors', () => {
      it('should have a postV4ProjectsIdBadgesBadgeId function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdBadgesBadgeId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdBadgesBadgeId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdBadgesBadgeId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing badgeId', (done) => {
        try {
          a.postV4ProjectsIdBadgesBadgeId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'badgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdBadgesBadgeId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdBadgesBadgeId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdBadgesBadgeId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdBadgesBadgeId - errors', () => {
      it('should have a deleteV4ProjectsIdBadgesBadgeId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdBadgesBadgeId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdBadgesBadgeId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdBadgesBadgeId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing badgeId', (done) => {
        try {
          a.deleteV4ProjectsIdBadgesBadgeId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'badgeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdBadgesBadgeId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdBadgesRender - errors', () => {
      it('should have a getV4ProjectsIdBadgesRender function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdBadgesRender === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdBadgesRender(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdBadgesRender', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing imageUrl', (done) => {
        try {
          a.getV4ProjectsIdBadgesRender('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'imageUrl is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdBadgesRender', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkUrl', (done) => {
        try {
          a.getV4ProjectsIdBadgesRender('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'linkUrl is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdBadgesRender', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdRegistryRepositories - errors', () => {
      it('should have a getV4ProjectsIdRegistryRepositories function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdRegistryRepositories === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdRegistryRepositories(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRegistryRepositories', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdRegistryRepositoriesRepositoryId - errors', () => {
      it('should have a deleteV4ProjectsIdRegistryRepositoriesRepositoryId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdRegistryRepositoriesRepositoryId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdRegistryRepositoriesRepositoryId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdRegistryRepositoriesRepositoryId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.deleteV4ProjectsIdRegistryRepositoriesRepositoryId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdRegistryRepositoriesRepositoryId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdRegistryRepositoriesRepositoryIdTags - errors', () => {
      it('should have a getV4ProjectsIdRegistryRepositoriesRepositoryIdTags function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdRegistryRepositoriesRepositoryIdTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdRegistryRepositoriesRepositoryIdTags(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRegistryRepositoriesRepositoryIdTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.getV4ProjectsIdRegistryRepositoriesRepositoryIdTags('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRegistryRepositoriesRepositoryIdTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdRegistryRepositoriesRepositoryIdTags - errors', () => {
      it('should have a deleteV4ProjectsIdRegistryRepositoriesRepositoryIdTags function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdRegistryRepositoriesRepositoryIdTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdRegistryRepositoriesRepositoryIdTags(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdRegistryRepositoriesRepositoryIdTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.deleteV4ProjectsIdRegistryRepositoriesRepositoryIdTags('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdRegistryRepositoriesRepositoryIdTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdRegistryRepositoriesRepositoryIdTagsTagName - errors', () => {
      it('should have a getV4ProjectsIdRegistryRepositoriesRepositoryIdTagsTagName function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdRegistryRepositoriesRepositoryIdTagsTagName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tagName', (done) => {
        try {
          a.getV4ProjectsIdRegistryRepositoriesRepositoryIdTagsTagName(null, null, null, (data, error) => {
            try {
              const displayE = 'tagName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRegistryRepositoriesRepositoryIdTagsTagName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdRegistryRepositoriesRepositoryIdTagsTagName('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRegistryRepositoriesRepositoryIdTagsTagName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.getV4ProjectsIdRegistryRepositoriesRepositoryIdTagsTagName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRegistryRepositoriesRepositoryIdTagsTagName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdRegistryRepositoriesRepositoryIdTagsTagName - errors', () => {
      it('should have a deleteV4ProjectsIdRegistryRepositoriesRepositoryIdTagsTagName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdRegistryRepositoriesRepositoryIdTagsTagName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tagName', (done) => {
        try {
          a.deleteV4ProjectsIdRegistryRepositoriesRepositoryIdTagsTagName(null, null, null, (data, error) => {
            try {
              const displayE = 'tagName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdRegistryRepositoriesRepositoryIdTagsTagName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdRegistryRepositoriesRepositoryIdTagsTagName('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdRegistryRepositoriesRepositoryIdTagsTagName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositoryId', (done) => {
        try {
          a.deleteV4ProjectsIdRegistryRepositoriesRepositoryIdTagsTagName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdRegistryRepositoriesRepositoryIdTagsTagName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdCustomAttributes - errors', () => {
      it('should have a getV4ProjectsIdCustomAttributes function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdCustomAttributes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdCustomAttributes(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdCustomAttributes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdCustomAttributesKey - errors', () => {
      it('should have a getV4ProjectsIdCustomAttributesKey function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdCustomAttributesKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdCustomAttributesKey(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdCustomAttributesKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.getV4ProjectsIdCustomAttributesKey('fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdCustomAttributesKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdCustomAttributesKey - errors', () => {
      it('should have a putV4ProjectsIdCustomAttributesKey function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdCustomAttributesKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdCustomAttributesKey(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdCustomAttributesKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.putV4ProjectsIdCustomAttributesKey('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdCustomAttributesKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdCustomAttributesKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdCustomAttributesKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdCustomAttributesKey - errors', () => {
      it('should have a deleteV4ProjectsIdCustomAttributesKey function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdCustomAttributesKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdCustomAttributesKey(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdCustomAttributesKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.deleteV4ProjectsIdCustomAttributesKey('fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdCustomAttributesKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdJobs - errors', () => {
      it('should have a getV4ProjectsIdJobs function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdJobs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdJobs(null, 'fakeparam', (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdJobs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdPipelinesPipelineIdJobs - errors', () => {
      it('should have a getV4ProjectsIdPipelinesPipelineIdJobs function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdPipelinesPipelineIdJobs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdPipelinesPipelineIdJobs(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdPipelinesPipelineIdJobs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pipelineId', (done) => {
        try {
          a.getV4ProjectsIdPipelinesPipelineIdJobs('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pipelineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdPipelinesPipelineIdJobs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdJobsJobId - errors', () => {
      it('should have a getV4ProjectsIdJobsJobId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdJobsJobId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdJobsJobId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdJobsJobId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.getV4ProjectsIdJobsJobId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdJobsJobId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdJobsJobIdTrace - errors', () => {
      it('should have a getV4ProjectsIdJobsJobIdTrace function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdJobsJobIdTrace === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdJobsJobIdTrace(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdJobsJobIdTrace', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.getV4ProjectsIdJobsJobIdTrace('fakeparam', null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdJobsJobIdTrace', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdJobsJobIdRetry - errors', () => {
      it('should have a postV4ProjectsIdJobsJobIdRetry function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdJobsJobIdRetry === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdJobsJobIdRetry(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdJobsJobIdRetry', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.postV4ProjectsIdJobsJobIdRetry('fakeparam', null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdJobsJobIdRetry', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdJobsJobIdErase - errors', () => {
      it('should have a postV4ProjectsIdJobsJobIdErase function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdJobsJobIdErase === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdJobsJobIdErase(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdJobsJobIdErase', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.postV4ProjectsIdJobsJobIdErase('fakeparam', null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdJobsJobIdErase', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdJobsJobIdCancel - errors', () => {
      it('should have a postV4ProjectsIdJobsJobIdCancel function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdJobsJobIdCancel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdJobsJobIdCancel(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdJobsJobIdCancel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.postV4ProjectsIdJobsJobIdCancel('fakeparam', null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdJobsJobIdCancel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdJobsJobIdPlay - errors', () => {
      it('should have a postV4ProjectsIdJobsJobIdPlay function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdJobsJobIdPlay === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdJobsJobIdPlay(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdJobsJobIdPlay', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.postV4ProjectsIdJobsJobIdPlay('fakeparam', null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdJobsJobIdPlay', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdPagesDomains - errors', () => {
      it('should have a getV4ProjectsIdPagesDomains function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdPagesDomains === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdPagesDomains(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdPagesDomains', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdPagesDomains - errors', () => {
      it('should have a postV4ProjectsIdPagesDomains function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdPagesDomains === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdPagesDomains(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdPagesDomains', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdPagesDomains('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdPagesDomains', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdPagesDomainsDomain - errors', () => {
      it('should have a getV4ProjectsIdPagesDomainsDomain function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdPagesDomainsDomain === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdPagesDomainsDomain(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdPagesDomainsDomain', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domain', (done) => {
        try {
          a.getV4ProjectsIdPagesDomainsDomain('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domain is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdPagesDomainsDomain', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdPagesDomainsDomain - errors', () => {
      it('should have a putV4ProjectsIdPagesDomainsDomain function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdPagesDomainsDomain === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdPagesDomainsDomain(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdPagesDomainsDomain', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domain', (done) => {
        try {
          a.putV4ProjectsIdPagesDomainsDomain('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'domain is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdPagesDomainsDomain', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdPagesDomainsDomain('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdPagesDomainsDomain', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdPagesDomainsDomain - errors', () => {
      it('should have a deleteV4ProjectsIdPagesDomainsDomain function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdPagesDomainsDomain === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdPagesDomainsDomain(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdPagesDomainsDomain', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domain', (done) => {
        try {
          a.deleteV4ProjectsIdPagesDomainsDomain('fakeparam', null, (data, error) => {
            try {
              const displayE = 'domain is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdPagesDomainsDomain', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdPipelineSchedules - errors', () => {
      it('should have a getV4ProjectsIdPipelineSchedules function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdPipelineSchedules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdPipelineSchedules('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdPipelineSchedules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsPipelineSchedules - errors', () => {
      it('should have a postV4ProjectsPipelineSchedules function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsPipelineSchedules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsPipelineSchedules(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsPipelineSchedules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdPipelineSchedulesPipelineScheduleId - errors', () => {
      it('should have a getV4ProjectsIdPipelineSchedulesPipelineScheduleId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdPipelineSchedulesPipelineScheduleId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdPipelineSchedulesPipelineScheduleId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdPipelineSchedulesPipelineScheduleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pipelineScheduleId', (done) => {
        try {
          a.getV4ProjectsIdPipelineSchedulesPipelineScheduleId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'pipelineScheduleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdPipelineSchedulesPipelineScheduleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsPipelineSchedulesPipelineScheduleId - errors', () => {
      it('should have a putV4ProjectsPipelineSchedulesPipelineScheduleId function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsPipelineSchedulesPipelineScheduleId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsPipelineSchedulesPipelineScheduleId(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsPipelineSchedulesPipelineScheduleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pipelineScheduleId', (done) => {
        try {
          a.putV4ProjectsPipelineSchedulesPipelineScheduleId('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'pipelineScheduleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsPipelineSchedulesPipelineScheduleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdPipelineSchedulesPipelineScheduleId - errors', () => {
      it('should have a deleteV4ProjectsIdPipelineSchedulesPipelineScheduleId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdPipelineSchedulesPipelineScheduleId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdPipelineSchedulesPipelineScheduleId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdPipelineSchedulesPipelineScheduleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pipelineScheduleId', (done) => {
        try {
          a.deleteV4ProjectsIdPipelineSchedulesPipelineScheduleId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'pipelineScheduleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdPipelineSchedulesPipelineScheduleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdPipelineSchedulesPipelineScheduleIdTakeOwnership - errors', () => {
      it('should have a postV4ProjectsIdPipelineSchedulesPipelineScheduleIdTakeOwnership function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdPipelineSchedulesPipelineScheduleIdTakeOwnership === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdPipelineSchedulesPipelineScheduleIdTakeOwnership(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdPipelineSchedulesPipelineScheduleIdTakeOwnership', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pipelineScheduleId', (done) => {
        try {
          a.postV4ProjectsIdPipelineSchedulesPipelineScheduleIdTakeOwnership('fakeparam', null, (data, error) => {
            try {
              const displayE = 'pipelineScheduleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdPipelineSchedulesPipelineScheduleIdTakeOwnership', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariables - errors', () => {
      it('should have a postV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariables function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariables === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariables(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariables', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pipelineScheduleId', (done) => {
        try {
          a.postV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariables('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pipelineScheduleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariables', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariablesKey - errors', () => {
      it('should have a putV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariablesKey function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariablesKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.putV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariablesKey(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariablesKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariablesKey('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariablesKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pipelineScheduleId', (done) => {
        try {
          a.putV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariablesKey('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pipelineScheduleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariablesKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariablesKey - errors', () => {
      it('should have a deleteV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariablesKey function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariablesKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariablesKey(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariablesKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pipelineScheduleId', (done) => {
        try {
          a.deleteV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariablesKey('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pipelineScheduleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariablesKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.deleteV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariablesKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdPipelineSchedulesPipelineScheduleIdVariablesKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdClusters - errors', () => {
      it('should have a getV4ProjectsIdClusters function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdClusters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdClusters(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdClusters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdClustersClusterId - errors', () => {
      it('should have a getV4ProjectsIdClustersClusterId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdClustersClusterId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdClustersClusterId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdClustersClusterId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clusterId', (done) => {
        try {
          a.getV4ProjectsIdClustersClusterId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'clusterId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdClustersClusterId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdClustesClusterId - errors', () => {
      it('should have a putV4ProjectsIdClustesClusterId function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdClustesClusterId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdClustesClusterId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdClustesClusterId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clusterId', (done) => {
        try {
          a.putV4ProjectsIdClustesClusterId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'clusterId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdClustesClusterId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdClustesClusterId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdClustesClusterId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdClustersClusterId - errors', () => {
      it('should have a deleteV4ProjectsIdClustersClusterId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdClustersClusterId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdClustersClusterId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdClustersClusterId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clusterId', (done) => {
        try {
          a.deleteV4ProjectsIdClustersClusterId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'clusterId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdClustersClusterId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdClustersUser - errors', () => {
      it('should have a postV4ProjectsIdClustersUser function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdClustersUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdClustersUser(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdClustersUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdClustersUser('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdClustersUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdProtectedBranches - errors', () => {
      it('should have a getV4ProjectsIdProtectedBranches function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdProtectedBranches === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdProtectedBranches(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdProtectedBranches', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProtectsIdProtectedBranches - errors', () => {
      it('should have a postV4ProtectsIdProtectedBranches function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProtectsIdProtectedBranches === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postV4ProtectsIdProtectedBranches(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProtectsIdProtectedBranches', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProtectsIdProtectedBranches('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProtectsIdProtectedBranches', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdProtectedBranchesName - errors', () => {
      it('should have a getV4ProjectsIdProtectedBranchesName function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdProtectedBranchesName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdProtectedBranchesName(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdProtectedBranchesName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getV4ProjectsIdProtectedBranchesName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdProtectedBranchesName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdProtectedBranchesName - errors', () => {
      it('should have a deleteV4ProjectsIdProtectedBranchesName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdProtectedBranchesName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdProtectedBranchesName(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdProtectedBranchesName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteV4ProjectsIdProtectedBranchesName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdProtectedBranchesName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdProtectedTags - errors', () => {
      it('should have a getV4ProjectsIdProtectedTags function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdProtectedTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdProtectedTags(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdProtectedTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdProtectedTags - errors', () => {
      it('should have a postV4ProjectsIdProtectedTags function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdProtectedTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdProtectedTags(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdProtectedTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postV4ProjectsIdProtectedTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdProtectedTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdProtectedTagsName - errors', () => {
      it('should have a getV4ProjectsIdProtectedTagsName function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdProtectedTagsName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getV4ProjectsIdProtectedTagsName(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdProtectedTagsName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdProtectedTagsName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdProtectedTagsName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdProtectedTagsName - errors', () => {
      it('should have a deleteV4ProjectsIdProtectedTagsName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdProtectedTagsName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdProtectedTagsName(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdProtectedTagsName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteV4ProjectsIdProtectedTagsName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdProtectedTagsName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdReleases - errors', () => {
      it('should have a getV4ProjectsIdReleases function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdReleases === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdReleases(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdReleases', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdReleases - errors', () => {
      it('should have a postV4ProjectsIdReleases function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdReleases === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdReleases(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdReleases', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdReleases('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdReleases', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdReleases - errors', () => {
      it('should have a deleteV4ProjectsIdReleases function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdReleases === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdReleases(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdReleases', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdReleasesTagName - errors', () => {
      it('should have a getV4ProjectsIdReleasesTagName function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdReleasesTagName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdReleasesTagName(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdReleasesTagName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tagName', (done) => {
        try {
          a.getV4ProjectsIdReleasesTagName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'tagName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdReleasesTagName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdReleasesTagName - errors', () => {
      it('should have a putV4ProjectsIdReleasesTagName function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdReleasesTagName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdReleasesTagName(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdReleasesTagName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tagName', (done) => {
        try {
          a.putV4ProjectsIdReleasesTagName('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'tagName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdReleasesTagName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdReleasesTagName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdReleasesTagName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdRepositorySubmodulesSubmodule - errors', () => {
      it('should have a putV4ProjectsIdRepositorySubmodulesSubmodule function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdRepositorySubmodulesSubmodule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdRepositorySubmodulesSubmodule(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdRepositorySubmodulesSubmodule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing submodule', (done) => {
        try {
          a.putV4ProjectsIdRepositorySubmodulesSubmodule('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'submodule is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdRepositorySubmodulesSubmodule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdRepositorySubmodulesSubmodule('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdRepositorySubmodulesSubmodule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdIssuesIssueiidResourceLabelEvents - errors', () => {
      it('should have a getV4ProjectsIdIssuesIssueiidResourceLabelEvents function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdIssuesIssueiidResourceLabelEvents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdIssuesIssueiidResourceLabelEvents(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdIssuesIssueiidResourceLabelEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIid', (done) => {
        try {
          a.getV4ProjectsIdIssuesIssueiidResourceLabelEvents('fakeparam', null, (data, error) => {
            try {
              const displayE = 'issueIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdIssuesIssueiidResourceLabelEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdIssuesIssueiidResourceLabelEventsId - errors', () => {
      it('should have a getV4ProjectsIdIssuesIssueiidResourceLabelEventsId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdIssuesIssueiidResourceLabelEventsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceLabelEventsId', (done) => {
        try {
          a.getV4ProjectsIdIssuesIssueiidResourceLabelEventsId(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceLabelEventsId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdIssuesIssueiidResourceLabelEventsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIid', (done) => {
        try {
          a.getV4ProjectsIdIssuesIssueiidResourceLabelEventsId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'issueIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdIssuesIssueiidResourceLabelEventsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdIssuesIssueiidResourceLabelEventsId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdIssuesIssueiidResourceLabelEventsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdMergeRequestiidResourceLabelEvents - errors', () => {
      it('should have a getV4ProjectsIdMergeRequestiidResourceLabelEvents function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdMergeRequestiidResourceLabelEvents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestiidResourceLabelEvents(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestiidResourceLabelEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestiidResourceLabelEvents('fakeparam', null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestiidResourceLabelEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdSearch - errors', () => {
      it('should have a getV4ProjectsIdSearch function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdSearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdSearch(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scope', (done) => {
        try {
          a.getV4ProjectsIdSearch('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'scope is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing search', (done) => {
        try {
          a.getV4ProjectsIdSearch('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'search is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdWikis - errors', () => {
      it('should have a getV4ProjectsIdWikis function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdWikis === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdWikis(null, 'fakeparam', (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdWikis', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdWikis - errors', () => {
      it('should have a postV4ProjectsIdWikis function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdWikis === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdWikis(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdWikis', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdWikis('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdWikis', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdWikisSlug - errors', () => {
      it('should have a getV4ProjectsIdWikisSlug function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdWikisSlug === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdWikisSlug(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdWikisSlug', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing slug', (done) => {
        try {
          a.getV4ProjectsIdWikisSlug('fakeparam', null, (data, error) => {
            try {
              const displayE = 'slug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdWikisSlug', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdWikisSlug - errors', () => {
      it('should have a putV4ProjectsIdWikisSlug function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdWikisSlug === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdWikisSlug(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdWikisSlug', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing slug', (done) => {
        try {
          a.putV4ProjectsIdWikisSlug('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'slug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdWikisSlug', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdWikisSlug('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdWikisSlug', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdWikis - errors', () => {
      it('should have a deleteV4ProjectsIdWikis function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdWikis === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdWikis(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdWikis', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing slug', (done) => {
        try {
          a.deleteV4ProjectsIdWikis('fakeparam', null, (data, error) => {
            try {
              const displayE = 'slug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdWikis', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdWikisAttachments - errors', () => {
      it('should have a postV4ProjectsIdWikisAttachments function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdWikisAttachments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdWikisAttachments('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdWikisAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsEnvironmentsEnvironmentIdStop - errors', () => {
      it('should have a postV4ProjectsEnvironmentsEnvironmentIdStop function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsEnvironmentsEnvironmentIdStop === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsEnvironmentsEnvironmentIdStop(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsEnvironmentsEnvironmentIdStop', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing environmentId', (done) => {
        try {
          a.postV4ProjectsEnvironmentsEnvironmentIdStop('fakeparam', null, (data, error) => {
            try {
              const displayE = 'environmentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsEnvironmentsEnvironmentIdStop', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdRepositoryFiles - errors', () => {
      it('should have a getV4ProjectsIdRepositoryFiles function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdRepositoryFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdRepositoryFiles(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRepositoryFiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filePath', (done) => {
        try {
          a.getV4ProjectsIdRepositoryFiles('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'filePath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRepositoryFiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ref', (done) => {
        try {
          a.getV4ProjectsIdRepositoryFiles('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ref is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRepositoryFiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdRepositoryFiles - errors', () => {
      it('should have a putV4ProjectsIdRepositoryFiles function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdRepositoryFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdRepositoryFiles(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdRepositoryFiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filePath', (done) => {
        try {
          a.putV4ProjectsIdRepositoryFiles('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'filePath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdRepositoryFiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdRepositoryFiles('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdRepositoryFiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdRepositoryFiles - errors', () => {
      it('should have a postV4ProjectsIdRepositoryFiles function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdRepositoryFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdRepositoryFiles(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdRepositoryFiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filePath', (done) => {
        try {
          a.postV4ProjectsIdRepositoryFiles('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'filePath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdRepositoryFiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdRepositoryFiles('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdRepositoryFiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdRepositoryFiles - errors', () => {
      it('should have a deleteV4ProjectsIdRepositoryFiles function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdRepositoryFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdRepositoryFiles(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdRepositoryFiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filePath', (done) => {
        try {
          a.deleteV4ProjectsIdRepositoryFiles('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'filePath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdRepositoryFiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.deleteV4ProjectsIdRepositoryFiles('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdRepositoryFiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsRepositoryFilesRaw - errors', () => {
      it('should have a getV4ProjectsRepositoryFilesRaw function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsRepositoryFilesRaw === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ref', (done) => {
        try {
          a.getV4ProjectsRepositoryFilesRaw(null, null, null, (data, error) => {
            try {
              const displayE = 'ref is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsRepositoryFilesRaw', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsRepositoryFilesRaw('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsRepositoryFilesRaw', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filePath', (done) => {
        try {
          a.getV4ProjectsRepositoryFilesRaw('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'filePath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsRepositoryFilesRaw', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdJobssJobIdArtifacts - errors', () => {
      it('should have a getV4ProjectsIdJobssJobIdArtifacts function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdJobssJobIdArtifacts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdJobssJobIdArtifacts(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdJobssJobIdArtifacts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.getV4ProjectsIdJobssJobIdArtifacts('fakeparam', null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdJobssJobIdArtifacts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdJobsJobIdArtifacts - errors', () => {
      it('should have a deleteV4ProjectsIdJobsJobIdArtifacts function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdJobsJobIdArtifacts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdJobsJobIdArtifacts(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdJobsJobIdArtifacts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.deleteV4ProjectsIdJobsJobIdArtifacts('fakeparam', null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdJobsJobIdArtifacts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdJobsJobIdArtifactsKeep - errors', () => {
      it('should have a postV4ProjectsIdJobsJobIdArtifactsKeep function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdJobsJobIdArtifactsKeep === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdJobsJobIdArtifactsKeep(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdJobsJobIdArtifactsKeep', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.postV4ProjectsIdJobsJobIdArtifactsKeep('fakeparam', null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdJobsJobIdArtifactsKeep', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdJobsArtifactsRefNameDownload - errors', () => {
      it('should have a getV4ProjectsIdJobsArtifactsRefNameDownload function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdJobsArtifactsRefNameDownload === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdJobsArtifactsRefNameDownload(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdJobsArtifactsRefNameDownload', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refName', (done) => {
        try {
          a.getV4ProjectsIdJobsArtifactsRefNameDownload('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'refName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdJobsArtifactsRefNameDownload', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing job', (done) => {
        try {
          a.getV4ProjectsIdJobsArtifactsRefNameDownload('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'job is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdJobsArtifactsRefNameDownload', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdJobsJobIdArtifactsArtifcactPath - errors', () => {
      it('should have a getV4ProjectsIdJobsJobIdArtifactsArtifcactPath function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdJobsJobIdArtifactsArtifcactPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdJobsJobIdArtifactsArtifcactPath(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdJobsJobIdArtifactsArtifcactPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.getV4ProjectsIdJobsJobIdArtifactsArtifcactPath('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdJobsJobIdArtifactsArtifcactPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing artifactPath', (done) => {
        try {
          a.getV4ProjectsIdJobsJobIdArtifactsArtifcactPath('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'artifactPath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdJobsJobIdArtifactsArtifcactPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdJobsArtifactsRefNameRaw - errors', () => {
      it('should have a getV4ProjectsIdJobsArtifactsRefNameRaw function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdJobsArtifactsRefNameRaw === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdJobsArtifactsRefNameRaw(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdJobsArtifactsRefNameRaw', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refName', (done) => {
        try {
          a.getV4ProjectsIdJobsArtifactsRefNameRaw('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'refName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdJobsArtifactsRefNameRaw', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing job', (done) => {
        try {
          a.getV4ProjectsIdJobsArtifactsRefNameRaw('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'job is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdJobsArtifactsRefNameRaw', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing artifactPath', (done) => {
        try {
          a.getV4ProjectsIdJobsArtifactsRefNameRaw('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'artifactPath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdJobsArtifactsRefNameRaw', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdRepositoryCommitsShaRefs - errors', () => {
      it('should have a getV4ProjectsIdRepositoryCommitsShaRefs function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdRepositoryCommitsShaRefs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdRepositoryCommitsShaRefs(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRepositoryCommitsShaRefs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sha', (done) => {
        try {
          a.getV4ProjectsIdRepositoryCommitsShaRefs('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sha is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdRepositoryCommitsShaRefs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdTriggersTriggerId - errors', () => {
      it('should have a getV4ProjectsIdTriggersTriggerId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdTriggersTriggerId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdTriggersTriggerId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdTriggersTriggerId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing triggerId', (done) => {
        try {
          a.getV4ProjectsIdTriggersTriggerId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'triggerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdTriggersTriggerId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdTriggersTriggerId - errors', () => {
      it('should have a postV4ProjectsIdTriggersTriggerId function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdTriggersTriggerId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing triggerId', (done) => {
        try {
          a.postV4ProjectsIdTriggersTriggerId(null, null, null, (data, error) => {
            try {
              const displayE = 'triggerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdTriggersTriggerId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdTriggersTriggerId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdTriggersTriggerId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdTriggersTriggerId - errors', () => {
      it('should have a deleteV4ProjectsIdTriggersTriggerId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdTriggersTriggerId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdTriggersTriggerId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdTriggersTriggerId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing triggerId', (done) => {
        try {
          a.deleteV4ProjectsIdTriggersTriggerId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'triggerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdTriggersTriggerId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdTriggersTriggerIdOwnership - errors', () => {
      it('should have a postV4ProjectsIdTriggersTriggerIdOwnership function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdTriggersTriggerIdOwnership === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing triggerId', (done) => {
        try {
          a.postV4ProjectsIdTriggersTriggerIdOwnership(null, null, null, (data, error) => {
            try {
              const displayE = 'triggerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdTriggersTriggerIdOwnership', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdTriggersTriggerIdOwnership('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdTriggersTriggerIdOwnership', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdServicesHangoutschat - errors', () => {
      it('should have a getV4ProjectsIdServicesHangoutschat function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdServicesHangoutschat === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdServicesHangoutschat(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdServicesHangoutschat', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdServicesHangoutschat - errors', () => {
      it('should have a putV4ProjectsIdServicesHangoutschat function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdServicesHangoutschat === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdServicesHangoutschat(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesHangoutschat', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdServicesHangoutschat('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesHangoutschat', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdServicesHangoutschat - errors', () => {
      it('should have a deleteV4ProjectsIdServicesHangoutschat function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdServicesHangoutschat === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdServicesHangoutschat(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdServicesHangoutschat', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdServicesPackagist - errors', () => {
      it('should have a getV4ProjectsIdServicesPackagist function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdServicesPackagist === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdServicesPackagist(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdServicesPackagist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdServicesPackagist - errors', () => {
      it('should have a putV4ProjectsIdServicesPackagist function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdServicesPackagist === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdServicesPackagist(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesPackagist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdServicesPackagist('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesPackagist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdServicesPackagist - errors', () => {
      it('should have a deleteV4ProjectsIdServicesPackagist function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdServicesPackagist === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdServicesPackagist(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdServicesPackagist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdServicesMicrosoftTeams - errors', () => {
      it('should have a getV4ProjectsIdServicesMicrosoftTeams function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdServicesMicrosoftTeams === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdServicesMicrosoftTeams(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdServicesMicrosoftTeams', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdServicesMicrosoftTeams - errors', () => {
      it('should have a putV4ProjectsIdServicesMicrosoftTeams function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdServicesMicrosoftTeams === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdServicesMicrosoftTeams(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesMicrosoftTeams', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdServicesMicrosoftTeams('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesMicrosoftTeams', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdServicesMicrosoftTeams - errors', () => {
      it('should have a deleteV4ProjectsIdServicesMicrosoftTeams function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdServicesMicrosoftTeams === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdServicesMicrosoftTeams(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdServicesMicrosoftTeams', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdServicesJenkins - errors', () => {
      it('should have a getV4ProjectsIdServicesJenkins function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdServicesJenkins === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdServicesJenkins(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdServicesJenkins', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdServicesJenkins - errors', () => {
      it('should have a putV4ProjectsIdServicesJenkins function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdServicesJenkins === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdServicesJenkins(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesJenkins', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdServicesJenkins('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesJenkins', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdServicesJenkins - errors', () => {
      it('should have a deleteV4ProjectsIdServicesJenkins function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdServicesJenkins === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdServicesJenkins(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdServicesJenkins', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdServicesMockCi - errors', () => {
      it('should have a getV4ProjectsIdServicesMockCi function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdServicesMockCi === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdServicesMockCi(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdServicesMockCi', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdServicesMockCi - errors', () => {
      it('should have a putV4ProjectsIdServicesMockCi function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdServicesMockCi === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdServicesMockCi(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesMockCi', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdServicesMockCi('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesMockCi', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdServicesMockCi - errors', () => {
      it('should have a deleteV4ProjectsIdServicesMockCi function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdServicesMockCi === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdServicesMockCi(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdServicesMockCi', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdServicesYoutrack - errors', () => {
      it('should have a getV4ProjectsIdServicesYoutrack function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdServicesYoutrack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdServicesYoutrack(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdServicesYoutrack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdServicesYoutrack - errors', () => {
      it('should have a putV4ProjectsIdServicesYoutrack function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdServicesYoutrack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdServicesYoutrack(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesYoutrack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdServicesYoutrack('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdServicesYoutrack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdServicesYoutrack - errors', () => {
      it('should have a deleteV4ProjectsIdServicesYoutrack function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdServicesYoutrack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdServicesYoutrack(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdServicesYoutrack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdSnippetIdDiscussions - errors', () => {
      it('should have a getV4ProjectsIdSnippetIdDiscussions function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdSnippetIdDiscussions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snippetId', (done) => {
        try {
          a.getV4ProjectsIdSnippetIdDiscussions(null, null, (data, error) => {
            try {
              const displayE = 'snippetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdSnippetIdDiscussions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdSnippetIdDiscussions('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdSnippetIdDiscussions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdSnippetIdDiscussions - errors', () => {
      it('should have a postV4ProjectsIdSnippetIdDiscussions function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdSnippetIdDiscussions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bodyQuery', (done) => {
        try {
          a.postV4ProjectsIdSnippetIdDiscussions(null, null, null, null, (data, error) => {
            try {
              const displayE = 'bodyQuery is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdSnippetIdDiscussions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdSnippetIdDiscussions('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdSnippetIdDiscussions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snippetId', (done) => {
        try {
          a.postV4ProjectsIdSnippetIdDiscussions('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'snippetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdSnippetIdDiscussions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdSnippetIdDiscussions('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdSnippetIdDiscussions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdSnippetIdDiscussionId - errors', () => {
      it('should have a getV4ProjectsIdSnippetIdDiscussionId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdSnippetIdDiscussionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdSnippetIdDiscussionId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdSnippetIdDiscussionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snippetId', (done) => {
        try {
          a.getV4ProjectsIdSnippetIdDiscussionId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'snippetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdSnippetIdDiscussionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing discussionId', (done) => {
        try {
          a.getV4ProjectsIdSnippetIdDiscussionId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'discussionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdSnippetIdDiscussionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdSnippetIdDiscussionIdNotes - errors', () => {
      it('should have a postV4ProjectsIdSnippetIdDiscussionIdNotes function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdSnippetIdDiscussionIdNotes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bodyQuery', (done) => {
        try {
          a.postV4ProjectsIdSnippetIdDiscussionIdNotes(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'bodyQuery is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdSnippetIdDiscussionIdNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdSnippetIdDiscussionIdNotes('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdSnippetIdDiscussionIdNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snippetId', (done) => {
        try {
          a.postV4ProjectsIdSnippetIdDiscussionIdNotes('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'snippetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdSnippetIdDiscussionIdNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing discussionId', (done) => {
        try {
          a.postV4ProjectsIdSnippetIdDiscussionIdNotes('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'discussionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdSnippetIdDiscussionIdNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdSnippetIdDiscussionIdNotes('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdSnippetIdDiscussionIdNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdSnippetIdDiscussionIdNoteId - errors', () => {
      it('should have a putV4ProjectsIdSnippetIdDiscussionIdNoteId function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdSnippetIdDiscussionIdNoteId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdSnippetIdDiscussionIdNoteId(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdSnippetIdDiscussionIdNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snippetId', (done) => {
        try {
          a.putV4ProjectsIdSnippetIdDiscussionIdNoteId('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'snippetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdSnippetIdDiscussionIdNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing discussionId', (done) => {
        try {
          a.putV4ProjectsIdSnippetIdDiscussionIdNoteId('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'discussionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdSnippetIdDiscussionIdNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing noteId', (done) => {
        try {
          a.putV4ProjectsIdSnippetIdDiscussionIdNoteId('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'noteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdSnippetIdDiscussionIdNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bodyQuery', (done) => {
        try {
          a.putV4ProjectsIdSnippetIdDiscussionIdNoteId('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'bodyQuery is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdSnippetIdDiscussionIdNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdSnippetIdDiscussionIdNoteId('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdSnippetIdDiscussionIdNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdSnippetIdDiscussionIdNoteId - errors', () => {
      it('should have a deleteV4ProjectsIdSnippetIdDiscussionIdNoteId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdSnippetIdDiscussionIdNoteId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdSnippetIdDiscussionIdNoteId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdSnippetIdDiscussionIdNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snippetId', (done) => {
        try {
          a.deleteV4ProjectsIdSnippetIdDiscussionIdNoteId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'snippetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdSnippetIdDiscussionIdNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing discussionId', (done) => {
        try {
          a.deleteV4ProjectsIdSnippetIdDiscussionIdNoteId('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'discussionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdSnippetIdDiscussionIdNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing noteId', (done) => {
        try {
          a.deleteV4ProjectsIdSnippetIdDiscussionIdNoteId('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'noteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdSnippetIdDiscussionIdNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdIssuesIidDiscussions - errors', () => {
      it('should have a getV4ProjectsIdIssuesIidDiscussions function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdIssuesIidDiscussions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdIssuesIidDiscussions(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdIssuesIidDiscussions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIid', (done) => {
        try {
          a.getV4ProjectsIdIssuesIidDiscussions('fakeparam', null, (data, error) => {
            try {
              const displayE = 'issueIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdIssuesIidDiscussions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdIssuesIidDiscussions - errors', () => {
      it('should have a postV4ProjectsIdIssuesIidDiscussions function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdIssuesIidDiscussions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bodyQuery', (done) => {
        try {
          a.postV4ProjectsIdIssuesIidDiscussions(null, null, null, null, (data, error) => {
            try {
              const displayE = 'bodyQuery is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIidDiscussions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdIssuesIidDiscussions('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIidDiscussions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIid', (done) => {
        try {
          a.postV4ProjectsIdIssuesIidDiscussions('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'issueIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIidDiscussions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdIssuesIidDiscussions('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIidDiscussions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdIssuesIidDiscussionsId - errors', () => {
      it('should have a getV4ProjectsIdIssuesIidDiscussionsId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdIssuesIidDiscussionsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdIssuesIidDiscussionsId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdIssuesIidDiscussionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIid', (done) => {
        try {
          a.getV4ProjectsIdIssuesIidDiscussionsId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'issueIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdIssuesIidDiscussionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing discussionId', (done) => {
        try {
          a.getV4ProjectsIdIssuesIidDiscussionsId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'discussionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdIssuesIidDiscussionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdIssuesIidDiscussionsIdNotes - errors', () => {
      it('should have a postV4ProjectsIdIssuesIidDiscussionsIdNotes function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdIssuesIidDiscussionsIdNotes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bodyQuery', (done) => {
        try {
          a.postV4ProjectsIdIssuesIidDiscussionsIdNotes(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'bodyQuery is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIidDiscussionsIdNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdIssuesIidDiscussionsIdNotes('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIidDiscussionsIdNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIid', (done) => {
        try {
          a.postV4ProjectsIdIssuesIidDiscussionsIdNotes('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'issueIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIidDiscussionsIdNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing discussionId', (done) => {
        try {
          a.postV4ProjectsIdIssuesIidDiscussionsIdNotes('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'discussionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIidDiscussionsIdNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdIssuesIidDiscussionsIdNotes('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIidDiscussionsIdNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdIssuesIidDiscussionsIdNotesId - errors', () => {
      it('should have a putV4ProjectsIdIssuesIidDiscussionsIdNotesId function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdIssuesIidDiscussionsIdNotesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bodyQuery', (done) => {
        try {
          a.putV4ProjectsIdIssuesIidDiscussionsIdNotesId(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'bodyQuery is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdIssuesIidDiscussionsIdNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdIssuesIidDiscussionsIdNotesId('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdIssuesIidDiscussionsIdNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIid', (done) => {
        try {
          a.putV4ProjectsIdIssuesIidDiscussionsIdNotesId('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'issueIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdIssuesIidDiscussionsIdNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing discussionId', (done) => {
        try {
          a.putV4ProjectsIdIssuesIidDiscussionsIdNotesId('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'discussionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdIssuesIidDiscussionsIdNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing noteId', (done) => {
        try {
          a.putV4ProjectsIdIssuesIidDiscussionsIdNotesId('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'noteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdIssuesIidDiscussionsIdNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdIssuesIidDiscussionsIdNotesId('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdIssuesIidDiscussionsIdNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdIssuesIidDiscussionsIdNotesId - errors', () => {
      it('should have a deleteV4ProjectsIdIssuesIidDiscussionsIdNotesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdIssuesIidDiscussionsIdNotesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdIssuesIidDiscussionsIdNotesId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdIssuesIidDiscussionsIdNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIid', (done) => {
        try {
          a.deleteV4ProjectsIdIssuesIidDiscussionsIdNotesId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'issueIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdIssuesIidDiscussionsIdNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing discussionId', (done) => {
        try {
          a.deleteV4ProjectsIdIssuesIidDiscussionsIdNotesId('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'discussionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdIssuesIidDiscussionsIdNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing noteId', (done) => {
        try {
          a.deleteV4ProjectsIdIssuesIidDiscussionsIdNotesId('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'noteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdIssuesIidDiscussionsIdNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdMergeRequestsIidDiscussions - errors', () => {
      it('should have a getV4ProjectsIdMergeRequestsIidDiscussions function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdMergeRequestsIidDiscussions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsIidDiscussions(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsIidDiscussions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsIidDiscussions('fakeparam', null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsIidDiscussions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdMergeRequestsIidDiscussionsId - errors', () => {
      it('should have a getV4ProjectsIdMergeRequestsIidDiscussionsId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdMergeRequestsIidDiscussionsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsIidDiscussionsId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsIidDiscussionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsIidDiscussionsId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsIidDiscussionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing discussionId', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsIidDiscussionsId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'discussionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsIidDiscussionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdMergeRequestsIidDiscussionsId - errors', () => {
      it('should have a putV4ProjectsIdMergeRequestsIidDiscussionsId function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdMergeRequestsIidDiscussionsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resolved', (done) => {
        try {
          a.putV4ProjectsIdMergeRequestsIidDiscussionsId(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'resolved is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdMergeRequestsIidDiscussionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdMergeRequestsIidDiscussionsId('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdMergeRequestsIidDiscussionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.putV4ProjectsIdMergeRequestsIidDiscussionsId('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdMergeRequestsIidDiscussionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing discussionId', (done) => {
        try {
          a.putV4ProjectsIdMergeRequestsIidDiscussionsId('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'discussionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdMergeRequestsIidDiscussionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId - errors', () => {
      it('should have a putV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bodyQuery', (done) => {
        try {
          a.putV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'bodyQuery is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resolved', (done) => {
        try {
          a.putV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'resolved is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.putV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing discussionId', (done) => {
        try {
          a.putV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'discussionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing noteId', (done) => {
        try {
          a.putV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'noteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId - errors', () => {
      it('should have a deleteV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.deleteV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing discussionId', (done) => {
        try {
          a.deleteV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'discussionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing noteId', (done) => {
        try {
          a.deleteV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'noteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdMergeRequestsIidDiscussionsIdNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdCommitsIdDiscussions - errors', () => {
      it('should have a getV4ProjectsIdCommitsIdDiscussions function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdCommitsIdDiscussions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdCommitsIdDiscussions(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdCommitsIdDiscussions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commitId', (done) => {
        try {
          a.getV4ProjectsIdCommitsIdDiscussions('fakeparam', null, (data, error) => {
            try {
              const displayE = 'commitId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdCommitsIdDiscussions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdCommitsIdDiscussions - errors', () => {
      it('should have a postV4ProjectsIdCommitsIdDiscussions function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdCommitsIdDiscussions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bodyQuery', (done) => {
        try {
          a.postV4ProjectsIdCommitsIdDiscussions(null, null, null, null, (data, error) => {
            try {
              const displayE = 'bodyQuery is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdCommitsIdDiscussions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdCommitsIdDiscussions('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdCommitsIdDiscussions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commitId', (done) => {
        try {
          a.postV4ProjectsIdCommitsIdDiscussions('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'commitId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdCommitsIdDiscussions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdCommitsIdDiscussions('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdCommitsIdDiscussions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdCommitsIdDiscussionsId - errors', () => {
      it('should have a getV4ProjectsIdCommitsIdDiscussionsId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdCommitsIdDiscussionsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdCommitsIdDiscussionsId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdCommitsIdDiscussionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commitId', (done) => {
        try {
          a.getV4ProjectsIdCommitsIdDiscussionsId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'commitId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdCommitsIdDiscussionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing discussionId', (done) => {
        try {
          a.getV4ProjectsIdCommitsIdDiscussionsId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'discussionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdCommitsIdDiscussionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdCommitsIdDiscussionsIdNotes - errors', () => {
      it('should have a postV4ProjectsIdCommitsIdDiscussionsIdNotes function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdCommitsIdDiscussionsIdNotes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bodyQuery', (done) => {
        try {
          a.postV4ProjectsIdCommitsIdDiscussionsIdNotes(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'bodyQuery is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdCommitsIdDiscussionsIdNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdCommitsIdDiscussionsIdNotes('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdCommitsIdDiscussionsIdNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commitId', (done) => {
        try {
          a.postV4ProjectsIdCommitsIdDiscussionsIdNotes('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'commitId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdCommitsIdDiscussionsIdNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing discussionId', (done) => {
        try {
          a.postV4ProjectsIdCommitsIdDiscussionsIdNotes('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'discussionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdCommitsIdDiscussionsIdNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdCommitsIdDiscussionsIdNotes('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdCommitsIdDiscussionsIdNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdCommitsIdDiscussionsIdNotesId - errors', () => {
      it('should have a putV4ProjectsIdCommitsIdDiscussionsIdNotesId function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdCommitsIdDiscussionsIdNotesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdCommitsIdDiscussionsIdNotesId(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdCommitsIdDiscussionsIdNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commitId', (done) => {
        try {
          a.putV4ProjectsIdCommitsIdDiscussionsIdNotesId('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'commitId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdCommitsIdDiscussionsIdNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing discussionId', (done) => {
        try {
          a.putV4ProjectsIdCommitsIdDiscussionsIdNotesId('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'discussionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdCommitsIdDiscussionsIdNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing noteId', (done) => {
        try {
          a.putV4ProjectsIdCommitsIdDiscussionsIdNotesId('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'noteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdCommitsIdDiscussionsIdNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bodyQuery', (done) => {
        try {
          a.putV4ProjectsIdCommitsIdDiscussionsIdNotesId('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'bodyQuery is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdCommitsIdDiscussionsIdNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdCommitsIdDiscussionsIdNotesId('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdCommitsIdDiscussionsIdNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdCommitsIdDiscussionsIdNotesId - errors', () => {
      it('should have a deleteV4ProjectsIdCommitsIdDiscussionsIdNotesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdCommitsIdDiscussionsIdNotesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdCommitsIdDiscussionsIdNotesId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdCommitsIdDiscussionsIdNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commitId', (done) => {
        try {
          a.deleteV4ProjectsIdCommitsIdDiscussionsIdNotesId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'commitId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdCommitsIdDiscussionsIdNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing discussionId', (done) => {
        try {
          a.deleteV4ProjectsIdCommitsIdDiscussionsIdNotesId('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'discussionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdCommitsIdDiscussionsIdNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing noteId', (done) => {
        try {
          a.deleteV4ProjectsIdCommitsIdDiscussionsIdNotesId('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'noteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdCommitsIdDiscussionsIdNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdSnippetsIdNotes - errors', () => {
      it('should have a getV4ProjectsIdSnippetsIdNotes function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdSnippetsIdNotes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdSnippetsIdNotes(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdSnippetsIdNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snippetId', (done) => {
        try {
          a.getV4ProjectsIdSnippetsIdNotes('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'snippetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdSnippetsIdNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdSnippetsIdNotes - errors', () => {
      it('should have a postV4ProjectsIdSnippetsIdNotes function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdSnippetsIdNotes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdSnippetsIdNotes(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdSnippetsIdNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snippetId', (done) => {
        try {
          a.postV4ProjectsIdSnippetsIdNotes('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'snippetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdSnippetsIdNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bodyQuery', (done) => {
        try {
          a.postV4ProjectsIdSnippetsIdNotes('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'bodyQuery is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdSnippetsIdNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdSnippetsIdNotes('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdSnippetsIdNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdSnippetsIdNoteId - errors', () => {
      it('should have a getV4ProjectsIdSnippetsIdNoteId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdSnippetsIdNoteId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdSnippetsIdNoteId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdSnippetsIdNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing noteId', (done) => {
        try {
          a.getV4ProjectsIdSnippetsIdNoteId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'noteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdSnippetsIdNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snippetId', (done) => {
        try {
          a.getV4ProjectsIdSnippetsIdNoteId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'snippetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdSnippetsIdNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdSnippetsIdNoteId - errors', () => {
      it('should have a putV4ProjectsIdSnippetsIdNoteId function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdSnippetsIdNoteId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdSnippetsIdNoteId(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdSnippetsIdNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing noteId', (done) => {
        try {
          a.putV4ProjectsIdSnippetsIdNoteId('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'noteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdSnippetsIdNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snippetId', (done) => {
        try {
          a.putV4ProjectsIdSnippetsIdNoteId('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'snippetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdSnippetsIdNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bodyQuery', (done) => {
        try {
          a.putV4ProjectsIdSnippetsIdNoteId('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'bodyQuery is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdSnippetsIdNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdSnippetsIdNoteId('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdSnippetsIdNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdSnippetsIdNoteId - errors', () => {
      it('should have a deleteV4ProjectsIdSnippetsIdNoteId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdSnippetsIdNoteId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdSnippetsIdNoteId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdSnippetsIdNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing noteId', (done) => {
        try {
          a.deleteV4ProjectsIdSnippetsIdNoteId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'noteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdSnippetsIdNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snippetId', (done) => {
        try {
          a.deleteV4ProjectsIdSnippetsIdNoteId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'snippetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdSnippetsIdNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdMergeRequestsIidNotes - errors', () => {
      it('should have a getV4ProjectsIdMergeRequestsIidNotes function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdMergeRequestsIidNotes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsIidNotes(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsIidNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsIidNotes('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsIidNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdMergeRequestsIidNotes - errors', () => {
      it('should have a postV4ProjectsIdMergeRequestsIidNotes function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdMergeRequestsIidNotes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestsIidNotes(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestsIidNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestsIidNotes('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestsIidNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bodyQuery', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestsIidNotes('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'bodyQuery is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestsIidNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestsIidNotes('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestsIidNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdIssuesIdSubscribe - errors', () => {
      it('should have a postV4ProjectsIdIssuesIdSubscribe function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdIssuesIdSubscribe === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdIssuesIdSubscribe(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIdSubscribe', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIid', (done) => {
        try {
          a.postV4ProjectsIdIssuesIdSubscribe('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'issueIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIdSubscribe', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdIssuesIdSubscribe('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIdSubscribe', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdIssuesIdUnsubscribe - errors', () => {
      it('should have a postV4ProjectsIdIssuesIdUnsubscribe function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdIssuesIdUnsubscribe === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdIssuesIdUnsubscribe(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIdUnsubscribe', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIid', (done) => {
        try {
          a.postV4ProjectsIdIssuesIdUnsubscribe('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'issueIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIdUnsubscribe', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdIssuesIdUnsubscribe('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIdUnsubscribe', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdIssuesIssueIdMove - errors', () => {
      it('should have a postV4ProjectsIdIssuesIssueIdMove function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdIssuesIssueIdMove === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdIssuesIssueIdMove(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIssueIdMove', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIid', (done) => {
        try {
          a.postV4ProjectsIdIssuesIssueIdMove('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'issueIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIssueIdMove', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdIssuesIidNotes - errors', () => {
      it('should have a getV4ProjectsIdIssuesIidNotes function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdIssuesIidNotes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdIssuesIidNotes(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdIssuesIidNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIid', (done) => {
        try {
          a.getV4ProjectsIdIssuesIidNotes('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'issueIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdIssuesIidNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdIssuesIidNotes - errors', () => {
      it('should have a postV4ProjectsIdIssuesIidNotes function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdIssuesIidNotes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdIssuesIidNotes(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIidNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIid', (done) => {
        try {
          a.postV4ProjectsIdIssuesIidNotes('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'issueIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIidNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bodyQuery', (done) => {
        try {
          a.postV4ProjectsIdIssuesIidNotes('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'bodyQuery is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIidNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdIssuesIidNotes('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIidNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmoji - errors', () => {
      it('should have a getV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmoji function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmoji === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmoji(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing noteId', (done) => {
        try {
          a.getV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmoji('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'noteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIid', (done) => {
        try {
          a.getV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmoji('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'issueIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmoji - errors', () => {
      it('should have a postV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmoji function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmoji === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmoji(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing noteId', (done) => {
        try {
          a.postV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmoji('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'noteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIid', (done) => {
        try {
          a.postV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmoji('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'issueIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmoji('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmojiAwardId - errors', () => {
      it('should have a getV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmojiAwardId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmojiAwardId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing awardId', (done) => {
        try {
          a.getV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmojiAwardId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'awardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmojiAwardId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing noteId', (done) => {
        try {
          a.getV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmojiAwardId('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'noteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIid', (done) => {
        try {
          a.getV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmojiAwardId('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'issueIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmojiAwardId - errors', () => {
      it('should have a deleteV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmojiAwardId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmojiAwardId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing awardId', (done) => {
        try {
          a.deleteV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmojiAwardId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'awardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmojiAwardId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing noteId', (done) => {
        try {
          a.deleteV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmojiAwardId('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'noteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIid', (done) => {
        try {
          a.deleteV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmojiAwardId('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'issueIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdIssuesIssueIdNotesNoteIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdIssuesIidNotesId - errors', () => {
      it('should have a getV4ProjectsIdIssuesIidNotesId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdIssuesIidNotesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdIssuesIidNotesId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdIssuesIidNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIid', (done) => {
        try {
          a.getV4ProjectsIdIssuesIidNotesId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'issueIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdIssuesIidNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing noteId', (done) => {
        try {
          a.getV4ProjectsIdIssuesIidNotesId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'noteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdIssuesIidNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdIssuesIidNotesId - errors', () => {
      it('should have a putV4ProjectsIdIssuesIidNotesId function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdIssuesIidNotesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdIssuesIidNotesId(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdIssuesIidNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bodyQuery', (done) => {
        try {
          a.putV4ProjectsIdIssuesIidNotesId('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'bodyQuery is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdIssuesIidNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIid', (done) => {
        try {
          a.putV4ProjectsIdIssuesIidNotesId('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'issueIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdIssuesIidNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing noteId', (done) => {
        try {
          a.putV4ProjectsIdIssuesIidNotesId('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'noteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdIssuesIidNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdIssuesIidNotesId('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdIssuesIidNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdIssuesIidNotesId - errors', () => {
      it('should have a deleteV4ProjectsIdIssuesIidNotesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdIssuesIidNotesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdIssuesIidNotesId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdIssuesIidNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIid', (done) => {
        try {
          a.deleteV4ProjectsIdIssuesIidNotesId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'issueIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdIssuesIidNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing noteId', (done) => {
        try {
          a.deleteV4ProjectsIdIssuesIidNotesId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'noteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdIssuesIidNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdIssuesIssueId - errors', () => {
      it('should have a getV4ProjectsIdIssuesIssueId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdIssuesIssueId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdIssuesIssueId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdIssuesIssueId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIid', (done) => {
        try {
          a.getV4ProjectsIdIssuesIssueId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'issueIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdIssuesIssueId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdIssuesIssueId - errors', () => {
      it('should have a putV4ProjectsIdIssuesIssueId function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdIssuesIssueId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdIssuesIssueId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdIssuesIssueId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIid', (done) => {
        try {
          a.putV4ProjectsIdIssuesIssueId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'issueIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdIssuesIssueId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdIssuesIssueId - errors', () => {
      it('should have a deleteV4ProjectsIdIssuesIssueId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdIssuesIssueId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdIssuesIssueId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdIssuesIssueId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIid', (done) => {
        try {
          a.deleteV4ProjectsIdIssuesIssueId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'issueIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdIssuesIssueId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdIssuesIssueIdAddSpentTime - errors', () => {
      it('should have a postV4ProjectsIdIssuesIssueIdAddSpentTime function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdIssuesIssueIdAddSpentTime === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdIssuesIssueIdAddSpentTime(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIssueIdAddSpentTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIid', (done) => {
        try {
          a.postV4ProjectsIdIssuesIssueIdAddSpentTime('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'issueIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIssueIdAddSpentTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdIssuesIssueIdAddSpentTime('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIssueIdAddSpentTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdIssuesIssueIdAwardEmoji - errors', () => {
      it('should have a getV4ProjectsIdIssuesIssueIdAwardEmoji function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdIssuesIssueIdAwardEmoji === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdIssuesIssueIdAwardEmoji(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdIssuesIssueIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIid', (done) => {
        try {
          a.getV4ProjectsIdIssuesIssueIdAwardEmoji('fakeparam', null, 'fakeparam', 'fakeparam', (data, error) => {
            try {
              const displayE = 'issueIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdIssuesIssueIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdIssuesIssueIdAwardEmoji - errors', () => {
      it('should have a postV4ProjectsIdIssuesIssueIdAwardEmoji function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdIssuesIssueIdAwardEmoji === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdIssuesIssueIdAwardEmoji(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIssueIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIid', (done) => {
        try {
          a.postV4ProjectsIdIssuesIssueIdAwardEmoji('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'issueIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIssueIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdIssuesIssueIdAwardEmoji('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIssueIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdIssuesIssueIdAwardEmojiAwardId - errors', () => {
      it('should have a getV4ProjectsIdIssuesIssueIdAwardEmojiAwardId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdIssuesIssueIdAwardEmojiAwardId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdIssuesIssueIdAwardEmojiAwardId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdIssuesIssueIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIid', (done) => {
        try {
          a.getV4ProjectsIdIssuesIssueIdAwardEmojiAwardId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'issueIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdIssuesIssueIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing awardId', (done) => {
        try {
          a.getV4ProjectsIdIssuesIssueIdAwardEmojiAwardId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'awardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdIssuesIssueIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdIssuesIssueIdAwardEmojiAwardId - errors', () => {
      it('should have a deleteV4ProjectsIdIssuesIssueIdAwardEmojiAwardId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdIssuesIssueIdAwardEmojiAwardId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdIssuesIssueIdAwardEmojiAwardId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdIssuesIssueIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIid', (done) => {
        try {
          a.deleteV4ProjectsIdIssuesIssueIdAwardEmojiAwardId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'issueIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdIssuesIssueIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing awardId', (done) => {
        try {
          a.deleteV4ProjectsIdIssuesIssueIdAwardEmojiAwardId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'awardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdIssuesIssueIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdIssuesIssueIdResetSpentTime - errors', () => {
      it('should have a postV4ProjectsIdIssuesIssueIdResetSpentTime function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdIssuesIssueIdResetSpentTime === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdIssuesIssueIdResetSpentTime(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIssueIdResetSpentTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIid', (done) => {
        try {
          a.postV4ProjectsIdIssuesIssueIdResetSpentTime('fakeparam', null, (data, error) => {
            try {
              const displayE = 'issueIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIssueIdResetSpentTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdIssuesIssueIdResetTimeEstimate - errors', () => {
      it('should have a postV4ProjectsIdIssuesIssueIdResetTimeEstimate function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdIssuesIssueIdResetTimeEstimate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdIssuesIssueIdResetTimeEstimate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIssueIdResetTimeEstimate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIid', (done) => {
        try {
          a.postV4ProjectsIdIssuesIssueIdResetTimeEstimate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'issueIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIssueIdResetTimeEstimate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdIssuesIssueIdTimeEstimate - errors', () => {
      it('should have a postV4ProjectsIdIssuesIssueIdTimeEstimate function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdIssuesIssueIdTimeEstimate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdIssuesIssueIdTimeEstimate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIssueIdTimeEstimate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIid', (done) => {
        try {
          a.postV4ProjectsIdIssuesIssueIdTimeEstimate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'issueIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIssueIdTimeEstimate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdIssuesIssueIdTimeEstimate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIssueIdTimeEstimate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdIssuesIssueIdTimeStats - errors', () => {
      it('should have a getV4ProjectsIdIssuesIssueIdTimeStats function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdIssuesIssueIdTimeStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdIssuesIssueIdTimeStats(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdIssuesIssueIdTimeStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIid', (done) => {
        try {
          a.getV4ProjectsIdIssuesIssueIdTimeStats('fakeparam', null, (data, error) => {
            try {
              const displayE = 'issueIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdIssuesIssueIdTimeStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdIssuesIssueIdTodo - errors', () => {
      it('should have a postV4ProjectsIdIssuesIssueIdTodo function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdIssuesIssueIdTodo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdIssuesIssueIdTodo(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIssueIdTodo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIid', (done) => {
        try {
          a.postV4ProjectsIdIssuesIssueIdTodo('fakeparam', null, (data, error) => {
            try {
              const displayE = 'issueIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdIssuesIssueIdTodo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdLabelsIdSubscribe - errors', () => {
      it('should have a postV4ProjectsIdLabelsIdSubscribe function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdLabelsIdSubscribe === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdLabelsIdSubscribe(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdLabelsIdSubscribe', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labelId', (done) => {
        try {
          a.postV4ProjectsIdLabelsIdSubscribe('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'labelId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdLabelsIdSubscribe', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdLabelsIdSubscribe('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdLabelsIdSubscribe', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdLabelsIdUnsubscribe - errors', () => {
      it('should have a postV4ProjectsIdLabelsIdUnsubscribe function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdLabelsIdUnsubscribe === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdLabelsIdUnsubscribe(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdLabelsIdUnsubscribe', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labelId', (done) => {
        try {
          a.postV4ProjectsIdLabelsIdUnsubscribe('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'labelId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdLabelsIdUnsubscribe', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdLabelsIdUnsubscribe('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdLabelsIdUnsubscribe', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdMergeRequestsMergeRequestId - errors', () => {
      it('should have a getV4ProjectsIdMergeRequestsMergeRequestId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdMergeRequestsMergeRequestId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsMergeRequestId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsMergeRequestId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsMergeRequestId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsMergeRequestId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdMergeRequestsMergeRequestId - errors', () => {
      it('should have a putV4ProjectsIdMergeRequestsMergeRequestId function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdMergeRequestsMergeRequestId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdMergeRequestsMergeRequestId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdMergeRequestsMergeRequestId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.putV4ProjectsIdMergeRequestsMergeRequestId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdMergeRequestsMergeRequestId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdMergeRequestsMergeRequestId - errors', () => {
      it('should have a deleteV4ProjectsIdMergeRequestsMergeRequestId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdMergeRequestsMergeRequestId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdMergeRequestsMergeRequestId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdMergeRequestsMergeRequestId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.deleteV4ProjectsIdMergeRequestsMergeRequestId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdMergeRequestsMergeRequestId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdMergeRequestsMergeRequestIdAddSpentTime - errors', () => {
      it('should have a postV4ProjectsIdMergeRequestsMergeRequestIdAddSpentTime function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdMergeRequestsMergeRequestIdAddSpentTime === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestsMergeRequestIdAddSpentTime(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestsMergeRequestIdAddSpentTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestsMergeRequestIdAddSpentTime('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestsMergeRequestIdAddSpentTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestsMergeRequestIdAddSpentTime('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestsMergeRequestIdAddSpentTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdMergeRequestsMergeRequestIdAwardEmoji - errors', () => {
      it('should have a getV4ProjectsIdMergeRequestsMergeRequestIdAwardEmoji function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdMergeRequestsMergeRequestIdAwardEmoji === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsMergeRequestIdAwardEmoji(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsMergeRequestIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsMergeRequestIdAwardEmoji('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsMergeRequestIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdMergeRequestsMergeRequestIdAwardEmoji - errors', () => {
      it('should have a postV4ProjectsIdMergeRequestsMergeRequestIdAwardEmoji function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdMergeRequestsMergeRequestIdAwardEmoji === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestsMergeRequestIdAwardEmoji(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestsMergeRequestIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestsMergeRequestIdAwardEmoji('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestsMergeRequestIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestsMergeRequestIdAwardEmoji('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestsMergeRequestIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdMergeRequestsMergeRequestIdAwardEmojiAwardId - errors', () => {
      it('should have a getV4ProjectsIdMergeRequestsMergeRequestIdAwardEmojiAwardId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdMergeRequestsMergeRequestIdAwardEmojiAwardId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsMergeRequestIdAwardEmojiAwardId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsMergeRequestIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsMergeRequestIdAwardEmojiAwardId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsMergeRequestIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing awardId', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsMergeRequestIdAwardEmojiAwardId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'awardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsMergeRequestIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdMergeRequestsMergeRequestIdAwardEmojiAwardId - errors', () => {
      it('should have a deleteV4ProjectsIdMergeRequestsMergeRequestIdAwardEmojiAwardId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdMergeRequestsMergeRequestIdAwardEmojiAwardId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdMergeRequestsMergeRequestIdAwardEmojiAwardId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdMergeRequestsMergeRequestIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.deleteV4ProjectsIdMergeRequestsMergeRequestIdAwardEmojiAwardId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdMergeRequestsMergeRequestIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing awardId', (done) => {
        try {
          a.deleteV4ProjectsIdMergeRequestsMergeRequestIdAwardEmojiAwardId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'awardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdMergeRequestsMergeRequestIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdMergeRequestsMergeRequestIdCancelMergeWhenBuildSucceeds - errors', () => {
      it('should have a postV4ProjectsIdMergeRequestsMergeRequestIdCancelMergeWhenBuildSucceeds function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdMergeRequestsMergeRequestIdCancelMergeWhenBuildSucceeds === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestsMergeRequestIdCancelMergeWhenBuildSucceeds(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestsMergeRequestIdCancelMergeWhenBuildSucceeds', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestsMergeRequestIdCancelMergeWhenBuildSucceeds('fakeparam', null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestsMergeRequestIdCancelMergeWhenBuildSucceeds', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdMergeRequestsMergeRequestIdChanges - errors', () => {
      it('should have a getV4ProjectsIdMergeRequestsMergeRequestIdChanges function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdMergeRequestsMergeRequestIdChanges === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsMergeRequestIdChanges(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsMergeRequestIdChanges', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsMergeRequestIdChanges('fakeparam', null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsMergeRequestIdChanges', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdMergeRequestsMergeRequestIdClosesIssues - errors', () => {
      it('should have a getV4ProjectsIdMergeRequestsMergeRequestIdClosesIssues function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdMergeRequestsMergeRequestIdClosesIssues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsMergeRequestIdClosesIssues(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsMergeRequestIdClosesIssues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsMergeRequestIdClosesIssues('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsMergeRequestIdClosesIssues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdMergeRequestsMergeRequestIdComments - errors', () => {
      it('should have a getV4ProjectsIdMergeRequestsMergeRequestIdComments function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdMergeRequestsMergeRequestIdComments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsMergeRequestIdComments(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsMergeRequestIdComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsMergeRequestIdComments('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsMergeRequestIdComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdMergeRequestsMergeRequestIdComments - errors', () => {
      it('should have a postV4ProjectsIdMergeRequestsMergeRequestIdComments function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdMergeRequestsMergeRequestIdComments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestsMergeRequestIdComments(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestsMergeRequestIdComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestsMergeRequestIdComments('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestsMergeRequestIdComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestsMergeRequestIdComments('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestsMergeRequestIdComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdMergeRequestsMergeRequestIdCommits - errors', () => {
      it('should have a getV4ProjectsIdMergeRequestsMergeRequestIdCommits function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdMergeRequestsMergeRequestIdCommits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsMergeRequestIdCommits(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsMergeRequestIdCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsMergeRequestIdCommits('fakeparam', null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsMergeRequestIdCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdMergeRequestsMergeRequestIdMerge - errors', () => {
      it('should have a putV4ProjectsIdMergeRequestsMergeRequestIdMerge function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdMergeRequestsMergeRequestIdMerge === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdMergeRequestsMergeRequestIdMerge(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdMergeRequestsMergeRequestIdMerge', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.putV4ProjectsIdMergeRequestsMergeRequestIdMerge('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdMergeRequestsMergeRequestIdMerge', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmoji - errors', () => {
      it('should have a getV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmoji function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmoji === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmoji('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmoji('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing noteId', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmoji('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'noteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmoji - errors', () => {
      it('should have a postV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmoji function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmoji === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmoji(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmoji('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing noteId', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmoji('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'noteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmoji('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmoji', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmojiAwardId - errors', () => {
      it('should have a getV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmojiAwardId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmojiAwardId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmojiAwardId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmojiAwardId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing noteId', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmojiAwardId('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'noteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing awardId', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmojiAwardId('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'awardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmojiAwardId - errors', () => {
      it('should have a deleteV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmojiAwardId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmojiAwardId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmojiAwardId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.deleteV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmojiAwardId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing noteId', (done) => {
        try {
          a.deleteV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmojiAwardId('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'noteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing awardId', (done) => {
        try {
          a.deleteV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmojiAwardId('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'awardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmojiAwardId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdMergeRequestsMergeRequestIdResetSpentTime - errors', () => {
      it('should have a postV4ProjectsIdMergeRequestsMergeRequestIdResetSpentTime function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdMergeRequestsMergeRequestIdResetSpentTime === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestsMergeRequestIdResetSpentTime(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestsMergeRequestIdResetSpentTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestsMergeRequestIdResetSpentTime('fakeparam', null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestsMergeRequestIdResetSpentTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdMergeRequestsMergeRequestIdResetTimeEstimate - errors', () => {
      it('should have a postV4ProjectsIdMergeRequestsMergeRequestIdResetTimeEstimate function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdMergeRequestsMergeRequestIdResetTimeEstimate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestsMergeRequestIdResetTimeEstimate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestsMergeRequestIdResetTimeEstimate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestsMergeRequestIdResetTimeEstimate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestsMergeRequestIdResetTimeEstimate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdMergeRequestsMergeRequestIdTimeEstimate - errors', () => {
      it('should have a postV4ProjectsIdMergeRequestsMergeRequestIdTimeEstimate function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdMergeRequestsMergeRequestIdTimeEstimate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestsMergeRequestIdTimeEstimate(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestsMergeRequestIdTimeEstimate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestsMergeRequestIdTimeEstimate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestsMergeRequestIdTimeEstimate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestsMergeRequestIdTimeEstimate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestsMergeRequestIdTimeEstimate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdMergeRequestsMergeRequestIdTimeStats - errors', () => {
      it('should have a getV4ProjectsIdMergeRequestsMergeRequestIdTimeStats function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdMergeRequestsMergeRequestIdTimeStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsMergeRequestIdTimeStats(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsMergeRequestIdTimeStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsMergeRequestIdTimeStats('fakeparam', null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsMergeRequestIdTimeStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdMergeRequestsMergeRequestIdTodo - errors', () => {
      it('should have a postV4ProjectsIdMergeRequestsMergeRequestIdTodo function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdMergeRequestsMergeRequestIdTodo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestsMergeRequestIdTodo(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestsMergeRequestIdTodo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestsMergeRequestIdTodo('fakeparam', null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestsMergeRequestIdTodo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdMergeRequestsMergeRequestIdVersions - errors', () => {
      it('should have a getV4ProjectsIdMergeRequestsMergeRequestIdVersions function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdMergeRequestsMergeRequestIdVersions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsMergeRequestIdVersions(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsMergeRequestIdVersions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsMergeRequestIdVersions('fakeparam', null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsMergeRequestIdVersions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdMergeRequestsMergeRequestIdVersionsVersionId - errors', () => {
      it('should have a getV4ProjectsIdMergeRequestsMergeRequestIdVersionsVersionId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdMergeRequestsMergeRequestIdVersionsVersionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsMergeRequestIdVersionsVersionId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsMergeRequestIdVersionsVersionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing versionId', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsMergeRequestIdVersionsVersionId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'versionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsMergeRequestIdVersionsVersionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsMergeRequestIdVersionsVersionId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsMergeRequestIdVersionsVersionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdMergeRequestsIidNotesId - errors', () => {
      it('should have a getV4ProjectsIdMergeRequestsIidNotesId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdMergeRequestsIidNotesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsIidNotesId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsIidNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsIidNotesId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsIidNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing noteId', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestsIidNotesId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'noteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestsIidNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4ProjectsIdMergeRequestsIidNotesId - errors', () => {
      it('should have a putV4ProjectsIdMergeRequestsIidNotesId function', (done) => {
        try {
          assert.equal(true, typeof a.putV4ProjectsIdMergeRequestsIidNotesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4ProjectsIdMergeRequestsIidNotesId(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdMergeRequestsIidNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.putV4ProjectsIdMergeRequestsIidNotesId('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdMergeRequestsIidNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing noteId', (done) => {
        try {
          a.putV4ProjectsIdMergeRequestsIidNotesId('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'noteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdMergeRequestsIidNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bodyQuery', (done) => {
        try {
          a.putV4ProjectsIdMergeRequestsIidNotesId('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'bodyQuery is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdMergeRequestsIidNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putV4ProjectsIdMergeRequestsIidNotesId('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4ProjectsIdMergeRequestsIidNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ProjectsIdMergeRequestsIidNotesId - errors', () => {
      it('should have a deleteV4ProjectsIdMergeRequestsIidNotesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ProjectsIdMergeRequestsIidNotesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4ProjectsIdMergeRequestsIidNotesId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdMergeRequestsIidNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing noteId', (done) => {
        try {
          a.deleteV4ProjectsIdMergeRequestsIidNotesId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'noteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdMergeRequestsIidNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.deleteV4ProjectsIdMergeRequestsIidNotesId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4ProjectsIdMergeRequestsIidNotesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdMergeRequestIidDiscussionsIdNotes - errors', () => {
      it('should have a postV4ProjectsIdMergeRequestIidDiscussionsIdNotes function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdMergeRequestIidDiscussionsIdNotes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bodyQuery', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestIidDiscussionsIdNotes(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'bodyQuery is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestIidDiscussionsIdNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestIidDiscussionsIdNotes('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestIidDiscussionsIdNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestIidDiscussionsIdNotes('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestIidDiscussionsIdNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing discussionId', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestIidDiscussionsIdNotes('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'discussionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestIidDiscussionsIdNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestIidDiscussionsIdNotes('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestIidDiscussionsIdNotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ProjectsIdMergeRequestiidResourceLabelEventId - errors', () => {
      it('should have a getV4ProjectsIdMergeRequestiidResourceLabelEventId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ProjectsIdMergeRequestiidResourceLabelEventId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestiidResourceLabelEventId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestiidResourceLabelEventId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestiidResourceLabelEventId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestiidResourceLabelEventId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceLabelEventId', (done) => {
        try {
          a.getV4ProjectsIdMergeRequestiidResourceLabelEventId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'resourceLabelEventId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4ProjectsIdMergeRequestiidResourceLabelEventId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdMergeRequestsIidSubscribe - errors', () => {
      it('should have a postV4ProjectsIdMergeRequestsIidSubscribe function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdMergeRequestsIidSubscribe === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestsIidSubscribe(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestsIidSubscribe', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestsIidSubscribe('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestsIidSubscribe', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestsIidSubscribe('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestsIidSubscribe', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ProjectsIdMergeRequestsIidUnsubscribe - errors', () => {
      it('should have a postV4ProjectsIdMergeRequestsIidUnsubscribe function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ProjectsIdMergeRequestsIidUnsubscribe === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestsIidUnsubscribe(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestsIidUnsubscribe', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mergeRequestIid', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestsIidUnsubscribe('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'mergeRequestIid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestsIidUnsubscribe', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ProjectsIdMergeRequestsIidUnsubscribe('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ProjectsIdMergeRequestsIidUnsubscribe', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4Runners - errors', () => {
      it('should have a getV4Runners function', (done) => {
        try {
          assert.equal(true, typeof a.getV4Runners === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4RunnersAll - errors', () => {
      it('should have a getV4RunnersAll function', (done) => {
        try {
          assert.equal(true, typeof a.getV4RunnersAll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4RunnersId - errors', () => {
      it('should have a getV4RunnersId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4RunnersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4RunnersId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4RunnersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4RunnersId - errors', () => {
      it('should have a putV4RunnersId function', (done) => {
        try {
          assert.equal(true, typeof a.putV4RunnersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4RunnersId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4RunnersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4RunnersId - errors', () => {
      it('should have a deleteV4RunnersId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4RunnersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4RunnersId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4RunnersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4Session - errors', () => {
      it('should have a postV4Session function', (done) => {
        try {
          assert.equal(true, typeof a.postV4Session === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4Session(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4Session', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4SidekiqCompoundMetrics - errors', () => {
      it('should have a getV4SidekiqCompoundMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.getV4SidekiqCompoundMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4SidekiqJobStats - errors', () => {
      it('should have a getV4SidekiqJobStats function', (done) => {
        try {
          assert.equal(true, typeof a.getV4SidekiqJobStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4SidekiqProcessMetrics - errors', () => {
      it('should have a getV4SidekiqProcessMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.getV4SidekiqProcessMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4SidekiqQueueMetrics - errors', () => {
      it('should have a getV4SidekiqQueueMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.getV4SidekiqQueueMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4Snippets - errors', () => {
      it('should have a getV4Snippets function', (done) => {
        try {
          assert.equal(true, typeof a.getV4Snippets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4Snippets - errors', () => {
      it('should have a postV4Snippets function', (done) => {
        try {
          assert.equal(true, typeof a.postV4Snippets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4Snippets(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4Snippets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4SnippetsPublic - errors', () => {
      it('should have a getV4SnippetsPublic function', (done) => {
        try {
          assert.equal(true, typeof a.getV4SnippetsPublic === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4SnippetsId - errors', () => {
      it('should have a getV4SnippetsId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4SnippetsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4SnippetsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4SnippetsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4SnippetsId - errors', () => {
      it('should have a putV4SnippetsId function', (done) => {
        try {
          assert.equal(true, typeof a.putV4SnippetsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4SnippetsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4SnippetsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4SnippetsId - errors', () => {
      it('should have a deleteV4SnippetsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4SnippetsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4SnippetsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4SnippetsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4SnippetsIdRaw - errors', () => {
      it('should have a getV4SnippetsIdRaw function', (done) => {
        try {
          assert.equal(true, typeof a.getV4SnippetsIdRaw === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4SnippetsIdRaw(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4SnippetsIdRaw', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4TemplatesDockerfiles - errors', () => {
      it('should have a getV4TemplatesDockerfiles function', (done) => {
        try {
          assert.equal(true, typeof a.getV4TemplatesDockerfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4TemplatesGitignores - errors', () => {
      it('should have a getV4TemplatesGitignores function', (done) => {
        try {
          assert.equal(true, typeof a.getV4TemplatesGitignores === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4TemplatesGitlabCiYmls - errors', () => {
      it('should have a getV4TemplatesGitlabCiYmls function', (done) => {
        try {
          assert.equal(true, typeof a.getV4TemplatesGitlabCiYmls === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4TemplatesLicenses - errors', () => {
      it('should have a getV4TemplatesLicenses function', (done) => {
        try {
          assert.equal(true, typeof a.getV4TemplatesLicenses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4TemplatesDockerfilesKey - errors', () => {
      it('should have a getV4TemplatesDockerfilesKey function', (done) => {
        try {
          assert.equal(true, typeof a.getV4TemplatesDockerfilesKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.getV4TemplatesDockerfilesKey(null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4TemplatesDockerfilesKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4TemplatesGitignoresKey - errors', () => {
      it('should have a getV4TemplatesGitignoresKey function', (done) => {
        try {
          assert.equal(true, typeof a.getV4TemplatesGitignoresKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.getV4TemplatesGitignoresKey(null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4TemplatesGitignoresKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4TemplatesGitlabCiYmlsKey - errors', () => {
      it('should have a getV4TemplatesGitlabCiYmlsKey function', (done) => {
        try {
          assert.equal(true, typeof a.getV4TemplatesGitlabCiYmlsKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.getV4TemplatesGitlabCiYmlsKey(null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4TemplatesGitlabCiYmlsKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4TemplatesLicensesKey - errors', () => {
      it('should have a getV4TemplatesLicensesKey function', (done) => {
        try {
          assert.equal(true, typeof a.getV4TemplatesLicensesKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.getV4TemplatesLicensesKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4TemplatesLicensesKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4Todos - errors', () => {
      it('should have a getV4Todos function', (done) => {
        try {
          assert.equal(true, typeof a.getV4Todos === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4TodosMarkAsDone - errors', () => {
      it('should have a postV4TodosMarkAsDone function', (done) => {
        try {
          assert.equal(true, typeof a.postV4TodosMarkAsDone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4TodosId - errors', () => {
      it('should have a deleteV4TodosId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4TodosId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4TodosId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4TodosId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4Users - errors', () => {
      it('should have a getV4Users function', (done) => {
        try {
          assert.equal(true, typeof a.getV4Users === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4Users - errors', () => {
      it('should have a postV4Users function', (done) => {
        try {
          assert.equal(true, typeof a.postV4Users === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4Users(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4Users', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4UsersId - errors', () => {
      it('should have a getV4UsersId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4UsersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4UsersId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4UsersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4UsersId - errors', () => {
      it('should have a putV4UsersId function', (done) => {
        try {
          assert.equal(true, typeof a.putV4UsersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4UsersId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4UsersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4UsersId - errors', () => {
      it('should have a deleteV4UsersId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4UsersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4UsersId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4UsersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4UsersIdBlock - errors', () => {
      it('should have a putV4UsersIdBlock function', (done) => {
        try {
          assert.equal(true, typeof a.putV4UsersIdBlock === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4UsersIdBlock(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4UsersIdBlock', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4UsersIdEmails - errors', () => {
      it('should have a getV4UsersIdEmails function', (done) => {
        try {
          assert.equal(true, typeof a.getV4UsersIdEmails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4UsersIdEmails(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4UsersIdEmails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4UsersIdEmails - errors', () => {
      it('should have a postV4UsersIdEmails function', (done) => {
        try {
          assert.equal(true, typeof a.postV4UsersIdEmails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4UsersIdEmails(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4UsersIdEmails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4UsersIdEmails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4UsersIdEmails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4UsersIdEmailsEmailId - errors', () => {
      it('should have a deleteV4UsersIdEmailsEmailId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4UsersIdEmailsEmailId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4UsersIdEmailsEmailId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4UsersIdEmailsEmailId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing emailId', (done) => {
        try {
          a.deleteV4UsersIdEmailsEmailId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'emailId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4UsersIdEmailsEmailId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4UsersIdEvents - errors', () => {
      it('should have a getV4UsersIdEvents function', (done) => {
        try {
          assert.equal(true, typeof a.getV4UsersIdEvents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4UsersIdEvents(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4UsersIdEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4UsersIdKeys - errors', () => {
      it('should have a getV4UsersIdKeys function', (done) => {
        try {
          assert.equal(true, typeof a.getV4UsersIdKeys === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4UsersIdKeys(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4UsersIdKeys', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4UsersIdKeys - errors', () => {
      it('should have a postV4UsersIdKeys function', (done) => {
        try {
          assert.equal(true, typeof a.postV4UsersIdKeys === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postV4UsersIdKeys(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4UsersIdKeys', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4UsersIdKeys('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4UsersIdKeys', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4UsersIdKeysKeyId - errors', () => {
      it('should have a deleteV4UsersIdKeysKeyId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4UsersIdKeysKeyId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4UsersIdKeysKeyId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4UsersIdKeysKeyId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keyId', (done) => {
        try {
          a.deleteV4UsersIdKeysKeyId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'keyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4UsersIdKeysKeyId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4UsersIdUnblock - errors', () => {
      it('should have a putV4UsersIdUnblock function', (done) => {
        try {
          assert.equal(true, typeof a.putV4UsersIdUnblock === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4UsersIdUnblock(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4UsersIdUnblock', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4UsersIdProjects - errors', () => {
      it('should have a getV4UsersIdProjects function', (done) => {
        try {
          assert.equal(true, typeof a.getV4UsersIdProjects === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4UsersIdProjects(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4UsersIdProjects', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4UsersIdStatus - errors', () => {
      it('should have a getV4UsersIdStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getV4UsersIdStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4UsersIdStatus(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4UsersIdStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4Version - errors', () => {
      it('should have a getV4Version function', (done) => {
        try {
          assert.equal(true, typeof a.getV4Version === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4Applications - errors', () => {
      it('should have a getV4Applications function', (done) => {
        try {
          assert.equal(true, typeof a.getV4Applications === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4Applications - errors', () => {
      it('should have a postV4Applications function', (done) => {
        try {
          assert.equal(true, typeof a.postV4Applications === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4Application - errors', () => {
      it('should have a deleteV4Application function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4Application === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4Application(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4Application', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4PagesDomains - errors', () => {
      it('should have a getV4PagesDomains function', (done) => {
        try {
          assert.equal(true, typeof a.getV4PagesDomains === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4Avatar - errors', () => {
      it('should have a getV4Avatar function', (done) => {
        try {
          assert.equal(true, typeof a.getV4Avatar === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing email', (done) => {
        try {
          a.getV4Avatar(null, null, (data, error) => {
            try {
              const displayE = 'email is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4Avatar', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4BroadcastMessage - errors', () => {
      it('should have a getV4BroadcastMessage function', (done) => {
        try {
          assert.equal(true, typeof a.getV4BroadcastMessage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4BroadcastMessage - errors', () => {
      it('should have a postV4BroadcastMessage function', (done) => {
        try {
          assert.equal(true, typeof a.postV4BroadcastMessage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4BroadcastMessage(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4BroadcastMessage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4BroadcastMessageId - errors', () => {
      it('should have a getV4BroadcastMessageId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4BroadcastMessageId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4BroadcastMessageId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4BroadcastMessageId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4BroadcastMessageId - errors', () => {
      it('should have a putV4BroadcastMessageId function', (done) => {
        try {
          assert.equal(true, typeof a.putV4BroadcastMessageId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4BroadcastMessageId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4BroadcastMessageId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4BroadcastMessageId - errors', () => {
      it('should have a deleteV4BroadcastMessageId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4BroadcastMessageId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteV4BroadcastMessageId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4BroadcastMessageId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4Events - errors', () => {
      it('should have a getV4Events function', (done) => {
        try {
          assert.equal(true, typeof a.getV4Events === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4Features - errors', () => {
      it('should have a getV4Features function', (done) => {
        try {
          assert.equal(true, typeof a.getV4Features === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4FeaturesName - errors', () => {
      it('should have a postV4FeaturesName function', (done) => {
        try {
          assert.equal(true, typeof a.postV4FeaturesName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postV4FeaturesName(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4FeaturesName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4FeaturesName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4FeaturesName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4FeaturesName - errors', () => {
      it('should have a deleteV4FeaturesName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4FeaturesName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteV4FeaturesName(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-deleteV4FeaturesName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4ImportGithub - errors', () => {
      it('should have a postV4ImportGithub function', (done) => {
        try {
          assert.equal(true, typeof a.postV4ImportGithub === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4ImportGithub(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4ImportGithub', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4Markdown - errors', () => {
      it('should have a postV4Markdown function', (done) => {
        try {
          assert.equal(true, typeof a.postV4Markdown === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4Markdown(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4Markdown', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4MergeRequests - errors', () => {
      it('should have a getV4MergeRequests function', (done) => {
        try {
          assert.equal(true, typeof a.getV4MergeRequests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4Search - errors', () => {
      it('should have a getV4Search function', (done) => {
        try {
          assert.equal(true, typeof a.getV4Search === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scope', (done) => {
        try {
          a.getV4Search(null, null, (data, error) => {
            try {
              const displayE = 'scope is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4Search', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing search', (done) => {
        try {
          a.getV4Search('fakeparam', null, (data, error) => {
            try {
              const displayE = 'search is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4Search', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4SuggestionIdApply - errors', () => {
      it('should have a putV4SuggestionIdApply function', (done) => {
        try {
          assert.equal(true, typeof a.putV4SuggestionIdApply === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4SuggestionIdApply(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-putV4SuggestionIdApply', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4Lint - errors', () => {
      it('should have a postV4Lint function', (done) => {
        try {
          assert.equal(true, typeof a.postV4Lint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postV4Lint(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-postV4Lint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4SubGroups - errors', () => {
      it('should have a getV4SubGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getV4SubGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getV4SubGroups(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-gitlab-adapter-getV4SubGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
